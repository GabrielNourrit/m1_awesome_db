--@ Documents/M1/S1/ABD/projet/insert.sql

insert into Client values ('jules@jules.com', 'noel', 'jules', 'ici','actif' ,'0000',0);
insert into Client values ('mickael@mickael.com', 'koc', 'mickael', 'la','actif', '0000',0);
insert into Client values ('seb@seb.com', 'lempereur', 'seb', 'vers la','actif' ,'0000',0);

insert into FichierImage values (fichierImage_id.nextval, '1', '/tmp/', 'i1', '100x100', 'actif', sysdate, 1, 0, 'jules@jules.com');
insert into FichierImage values (fichierImage_id.nextval, '2', '/tmp/', 'i2', '100x100', 'actif', sysdate, 1, 0, 'mickael@mickael.com');
insert into FichierImage values (fichierImage_id.nextval, '3', '/tmp/', 'i3', '100x100', 'actif', sysdate, 1, 0, 'seb@seb.com');
insert into FichierImage values (fichierImage_id.nextval, '4', '/tmp/', 'i4', '100x100', 'actif', sysdate, 1, 0, 'seb@seb.com');

insert into Photo values (photo_id.nextval, 'noir', 1, 'jules@jules.com','actif');
insert into Photo values (photo_id.nextval, 'negatif', 2, 'mickael@mickael.com','actif');
insert into Photo values (photo_id.nextval, 'sepia', 3, 'seb@seb.com','actif');
insert into Photo values (photo_id.nextval, 'sepia', 4, 'seb@seb.com','actif');

insert into AdresseLivraison values (adresseLivraison_id.nextval, '1','rue des patates', 'patateland', '38000', 'noel', 'jules');
insert into AdresseLivraison values (adresseLivraison_id.nextval, '2','rue des patates', 'patateland', '38000', 'noel', 'jules');
insert into AdresseLivraison values (adresseLivraison_id.nextval, '3','rue des patates', 'patateland', '38000', 'noel', 'jules');

insert into Commande values (commande_id.nextval, sysdate, 'Adresse', '', 58.25, 58.25, 'Paye', 'jules@jules.com', 1);
insert into Commande values (commande_id.nextval, sysdate, 'Adresse', '', 98.25, 58.25, 'EnCours', 'jules@jules.com', 2);
insert into Commande values (commande_id.nextval, sysdate, 'Adresse', '', 78.25, 58.25, 'Envoye', 'jules@jules.com', 3);
insert into Commande values (commande_id.nextval, sysdate, 'Adresse', '', 78.25, 58.25, 'Paye', 'seb@seb.com', 3);

insert into Article values (article_id.nextval, 2, 2);
insert into Article values (article_id.nextval, 2, 1);
insert into Article values (article_id.nextval, 2, 1);
insert into Article values (article_id.nextval, 2, 1);
insert into Article values (article_id.nextval, 2, 1);
insert into Article values (article_id.nextval, 2, 1);
insert into Article values (article_id.nextval, 2, 1);
insert into Article values (article_id.nextval, 2, 1);
insert into Article values (article_id.nextval, 2, 1);
insert into Article values (article_id.nextval, 2, 1);
insert into Article values (article_id.nextval, 2, 1);
insert into Article values (article_id.nextval, 2, 1);
insert into Article values (article_id.nextval, 2, 1);
insert into Article values (article_id.nextval, 2, 1);
insert into Article values (article_id.nextval, 2, 1);
insert into Article values (article_id.nextval, 2, 4);


insert into Impression (id_impression, PU, estPromo) values (impression_id.nextval, 58.55, 0);
insert into Impression (id_impression, PU, estPromo) values (impression_id.nextval, 25.55, 0);
insert into Impression (id_impression, PU, estPromo) values (impression_id.nextval, 528.55, 0);
insert into Impression (id_impression, PU, estPromo) values (impression_id.nextval, 47.55, 0);
insert into Impression (id_impression, PU, estPromo) values (impression_id.nextval, 56.55, 0);
insert into Impression (id_impression, PU, estPromo) values (impression_id.nextval, 88.55, 0);
insert into Impression (id_impression, PU, estPromo) values (impression_id.nextval, 68.55, 0);
insert into Impression (id_impression, PU, estPromo) values (impression_id.nextval, 98.55, 0);
insert into Impression (id_impression, PU, estPromo) values (impression_id.nextval, 548.55, 0);
insert into Impression (id_impression, PU, estPromo) values (impression_id.nextval, 78.55, 0);
insert into Impression (id_impression, PU, estPromo) values (impression_id.nextval, 97.55, 0);
insert into Impression (id_impression, PU, estPromo) values (impression_id.nextval, 78.55, 0);
insert into Impression (id_impression, PU, estPromo) values (impression_id.nextval, 69.55, 0);
insert into Impression (id_impression, PU, estPromo) values (impression_id.nextval, 41.55, 0);
insert into Impression (id_impression, PU, estPromo) values (impression_id.nextval, 54.55, 0);
insert into Impression (id_impression, PU, estPromo) values (impression_id.nextval, 54.55, 0);

insert into Tirage values (1, '80g', 'A4');
insert into Tirage values (2, '120g', 'A2');
insert into Tirage values (3, '200g', 'A3');

insert into Album values (4, 't1', 'infos1', 'A4', '80g', 1);
insert into Album values (5, 't2', 'infos2', 'A3', '120g', 2);
insert into Album values (6, 't3', 'infos3', 'A2', '200g', 3);


insert into Agenda values (7, 'S52', 'grand');
insert into Agenda values (8, 'S52', 'petit');
insert into Agenda values (9, 'J365', 'moyen');

insert into Cadre values (10, 'grand', 'm1', 'haut');
insert into Cadre values (11, 'petit', 'm2', 'bas');
insert into Cadre values (12, 'moyen', 'm3', 'milieu');

insert into Calendrier values (13, 'Bureau');
insert into Calendrier values (14, 'Bureau');
insert into Calendrier values (15, 'Mural');
insert into Calendrier values (16, 'Mural');

insert into Lk_Album_Photo values (4, 1, 'yy', 1, 1);
insert into Lk_Album_Photo values (5, 2, 'rr', 1, 1);
insert into Lk_Album_Photo values (6, 3, 'ee', 1, 1);

insert into Lk_Tirage_Photo values (1, 1, 2);
insert into Lk_Tirage_Photo values (2, 2, 4);
insert into Lk_Tirage_Photo values (3, 3, 8);

insert into Lk_Agenda_Photo values (7, 1, 1);
insert into Lk_Agenda_Photo values (8, 2, 2);
insert into Lk_Agenda_Photo values (9, 3, 1);

insert into Lk_Cadre_Photo values (10, 1, 4);
insert into Lk_Cadre_Photo values (11, 2, 8);
insert into Lk_Cadre_Photo values (12, 3, 9);

insert into Lk_Calendrier_Photo values (13, 1, 1, 1);
insert into Lk_Calendrier_Photo values (14, 2, 1, 3);
insert into Lk_Calendrier_Photo values (15, 3, 2, 5);
insert into Lk_Calendrier_Photo values (16, 4, 2, 5);

insert into InventaireAgenda values ('grand','S52', 12, 5);
insert into InventaireAgenda values ('petit','S52', 8, 5);
insert into InventaireAgenda values ('grand','J365', 40, 5);
insert into InventaireAgenda values ('petit','J365', 35, 5);

insert into InventaireCalendrier values ('Bureau', 12, 5);
insert into InventaireCalendrier values ('Mural', 8, 5);

insert into InventairePapier values ('80g','A4', 12, 5);
insert into InventairePapier values ('80g','A3', 15, 5);
insert into InventairePapier values ('120g','A4', 17, 5);
insert into InventairePapier values ('120g','A3', 20, 5);
insert into InventairePapier values ('120g','A2', 8, 5);
insert into InventairePapier values ('200g','A3', 40, 5);

insert into InventaireCadre values ('grand','m1', 12, 5);
insert into InventaireCadre values ('petit','m1', 17, 5);
insert into InventaireCadre values ('petit','m2', 8, 5);
insert into InventaireCadre values ('grand','m2', 10, 5);
insert into InventaireCadre values ('moyen','m3', 40, 5);

insert into CodePromo values ('4B2A', 10, 0, 0, sysdate,2);
insert into CodePromo values ('5B2A', 20, 1, 0, sysdate, null);
insert into CodePromo values ('6B2A', 20, 0, 0, sysdate,3);

insert into Lk_CodePromo_Commande values ('4B2A', 1);
insert into Lk_CodePromo_Commande values ('5B2A', 1);
insert into Lk_CodePromo_Commande values ('5B2A', 2);
insert into Lk_CodePromo_Commande values ('6B2A', 3);

insert into Lk_AdresseLivraison_Client values (1, 'jules@jules.com');
insert into Lk_AdresseLivraison_Client values (1, 'mickael@mickael.com');
insert into Lk_AdresseLivraison_Client values (1, 'seb@seb.com');
