--@ Documents/M1/S1/ABD/projet/triggers.sql

CREATE OR REPLACE TRIGGER c8 BEFORE INSERT ON Lk_CodePromo_Commande
FOR EACH ROW
BEGIN
UPDATE CodePromo SET nombre_utilisation = nombre_utilisation+1 WHERE CodePromo.id_commande = :NEW.id_commande;
END;
/


CREATE OR REPLACE TRIGGER c11 
BEFORE UPDATE of etat ON Client
FOR EACH ROW
BEGIN
	IF (:NEW.etat LIKE 'supprime') THEN
	   UPDATE FichierImage SET etat = 'inactif' WHERE :NEW.mail_client = FichierImage.mail_client;
	END IF;
END;
/

/* exemple : update FichierImage set etat = 'problemeJuridique', est_actif=0 where id_fic=1;*/
create or replace trigger C12
before update on fichierimage
for each row
begin
if (:NEW.etat = 'problemeJuridique')
then
update commande set statut='Annule' where statut like 'EnCours' and id_commande in (
select distinct id_commande from
/*on recupere les fichiers à problèmes qui ont été utilisé par une photo*/
(select id_photo from photo where id_fic =:new.id_fic ) natural join
/*on cree id_photo,id_article,id_impression*/
((SELECT id_photo,id_album as id_article FROM lk_album_photo ) natural full outer join
(SELECT id_photo,id_calendrier as id_article FROM lk_calendrier_photo) natural full outer join
(SELECT id_photo,id_tirage as id_article FROM lk_tirage_photo) natural full outer join
(SELECT id_photo,id_agenda as id_article FROM lk_agenda_photo) natural full outer join
(SELECT id_photo,id_cadre as id_article FROM lk_cadre_photo) natural join 
/*on associe les infos qu'ils nous manquent*/
article) natural join commande);
end if;
end;
/

-- Trigger 1
create or replace trigger codePromoNbUtilisation
before insert or update on Lk_CodePromo_Commande
for each row
declare
	nbUtilisation int;
	pub int;
begin
	select nombre_utilisation into nbUtilisation from codepromo where code_promo = :new.code_promo;
	select est_public into pub from codepromo where code_promo = :new.code_promo;
	if (pub = 0 and nbUtilisation = 1) then
		raise_application_error(-20105,'Un code promo prive ne peut etre utilise qu une seule fois');
	else 
		update codepromo set nombre_utilisation = nbUtilisation + 1 where code_promo = :new.code_promo;
	end if;
	
end;
/

-- Trigger 2
create or replace trigger codePromoDateUtilisation
before insert or update on Lk_CodePromo_Commande
for each row
declare
	date_fin date;
begin
	select date_fin_code into date_fin from CodePromo where code_promo = :new.code_promo;
	if (date_fin < sysdate) then
		raise_application_error(-20105,'Le code promo est fini');
	end if;
end;
/


-- Trigger Bonus
CREATE OR REPLACE TRIGGER cb 
BEFORE UPDATE of etat ON FichierImage
FOR EACH ROW
BEGIN
	IF (:NEW.etat LIKE 'problemeJuridique') THEN
	   UPDATE Photo SET etat = 'supprime' WHERE id_fic=:new.id_fic;
	END IF;
END;
/