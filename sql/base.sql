--@ Documents/M1/S1/ABD/projet/base.sql

DROP TABLE InventaireAgenda;
DROP TABLE InventaireCalendrier;
DROP TABLE InventairePapier;
DROP TABLE InventaireCadre;
DROP TABLE Lk_CodePromo_Commande;
DROP TABLE Lk_AdresseLivraison_Client;
drop table Lk_Album_Photo;
drop table Lk_Tirage_Photo;
drop table Lk_Agenda_Photo;
drop table Lk_Cadre_Photo;
drop table Lk_Calendrier_Photo;
drop table Calendrier;
drop table Cadre;
drop table Agenda;
drop table Album;
drop table Tirage;
drop table Impression;
drop table Photo;
drop table FichierImage;
DROP TABLE CodePromo;
drop table Article;
drop table Commande;
drop table Client;
DROP TABLE AdresseLivraison;


DROP SEQUENCE impression_id;
DROP SEQUENCE tirage_id;
DROP SEQUENCE album_id;
DROP SEQUENCE agenda_id;
DROP SEQUENCE cadre_id;
DROP SEQUENCE calendrier_id;
DROP SEQUENCE fichierImage_id;
DROP SEQUENCE photo_id;
DROP SEQUENCE adresseLivraison_id;
DROP SEQUENCE commande_id;
DROP SEQUENCE article_id;

CREATE SEQUENCE article_id START WITH 0 MINVALUE 0;
CREATE SEQUENCE impression_id START WITH 0 MINVALUE 0;
CREATE SEQUENCE tirage_id START WITH 0 MINVALUE 0;
CREATE SEQUENCE album_id START WITH 0 MINVALUE 0;
CREATE SEQUENCE agenda_id START WITH 0 MINVALUE 0;
CREATE SEQUENCE cadre_id START WITH 0 MINVALUE 0;
CREATE SEQUENCE calendrier_id START WITH 0 MINVALUE 0;
CREATE SEQUENCE fichierImage_id START WITH 0 MINVALUE 0;
CREATE SEQUENCE photo_id START WITH 0 MINVALUE 0;
CREATE SEQUENCE adresseLivraison_id START WITH 0 MINVALUE 0;
CREATE SEQUENCE commande_id START WITH 0 MINVALUE 0;


create table Client (
	mail_client varchar2(255) primary key,
	nom varchar2(255),
	prenom varchar2(255),
	adresse varchar2(255),
    etat varchar(10),
	password varchar2(255) not null,
	est_connecte int
);

create table FichierImage (
	id_fic int primary key,
	libelle varchar2(255),
	chemin varchar2(255),
	infos_vue varchar2(255),
	resolution varchar2(255),
	etat varchar2(255),
	date_ajout date not null,
	est_actif int not null,
	est_partage int not null,
	mail_client varchar2(255),
	constraint cEstActif check (est_actif between 0 and 1),
	constraint cEstPartage check (est_partage between 0 and 1),
	foreign key(mail_client) references Client(mail_client)
);

CREATE TABLE AdresseLivraison (
	id_adresse_livraison int PRIMARY KEY,
	numero varchar2(255) not null,
	rue varchar2(255) not null,
	ville varchar2(255) not null,
	code_postal varchar2(255), 
	nom varchar2(255) not null, 
	prenom varchar2(255) not null
);

create table Commande (
	id_commande int primary key,
	date_commande date,
	mode_livraison varchar2(255),
	code_point_relais varchar2(255),
	prix_tt number(*,2),
	prix_tt_remise number(*,2),
	statut varchar2(255),
	mail_client varchar2(255),
	id_adresse_livraison int,
	foreign key(mail_client) references Client(mail_client),
	foreign key(id_adresse_livraison) references AdresseLivraison(id_adresse_livraison)
);

create table Photo (
	id_photo int primary key,
	retouches varchar2(255),
	id_fic int,
	mail_client varchar2(255),
        etat varchar2(255),
	foreign key(id_fic) references FichierImage(id_fic),
	foreign key(mail_client) references Client(mail_client)
);

create table Article (
	id_article int primary key,
	quantite int,
	id_commande int,
	foreign key (id_commande) references Commande (id_commande)
);

create table Impression (
	id_impression int primary key,
	pu number(*,2) not null,
	estPromo int,
	prix_promo number(*,2),
	--id_article int,
	foreign key (id_impression) references Article (id_article),
	constraint cEstPromo check (estPromo between 0 and 1)
);

create table Tirage (
	id_tirage int primary key,
	qualite varchar2(255) not null,
	format varchar2(255) not null,
	foreign key(id_tirage) references Impression(id_impression)
);

create table Album (
	id_album int primary key,
	titre varchar2(255) not null,
	infos_MiseEnPage varchar2(255) not null,
	format varchar2(255) not null,
	qualite varchar2(255) not null,
	id_photo int,
	foreign key(id_album) references Impression(id_impression),
	foreign key(id_photo) references Photo(id_photo)
);

create table Agenda (
	id_agenda int primary key,
	modele varchar2(255),
	type varchar2(255),
	foreign key(id_agenda) references Impression(id_impression)
	--constraint cType check (type='s52' or type='j365')
);

create table Cadre (
	id_cadre int primary key,
	taille varchar2(255), --A changer dans MLD dossier
	modele varchar2(255),
	mise_en_page varchar2(255),
	foreign key(id_cadre) references Impression(id_impression)
);

create table Calendrier(
	id_calendrier int primary key,
	modele varchar2(255),
	foreign key(id_calendrier) references Impression(id_impression)
);

create table Lk_Album_Photo (
	id_album int,
	id_photo int,
	description varchar2(255),
	ordre int,
	page int,
	primary key(id_album,id_photo),
	foreign key(id_album) references Album(id_album),
	foreign key(id_photo) references Photo(id_photo)
);

create table Lk_Tirage_Photo (
	id_tirage int,
	id_photo int,
	quantite int,
	primary key(id_tirage,id_photo),
	foreign key(id_tirage) references Tirage(id_tirage),
	foreign key(id_photo) references Photo(id_photo)
);

create table Lk_Agenda_Photo (
	id_agenda int,
	id_photo int,
	no_page int,
	primary key(id_agenda,id_photo),
	foreign key(id_agenda) references Agenda(id_agenda),
	foreign key(id_photo) references Photo(id_photo)
);

create table Lk_Cadre_Photo (
	id_cadre int,
	id_photo int,
	page int,
	primary key(id_cadre,id_photo),
	foreign key(id_cadre) references Cadre(id_cadre),
	foreign key(id_photo) references Photo(id_photo)
);

create table Lk_Calendrier_Photo (
	id_calendrier int,
	id_photo int,
	ordre int,
	page int,
	primary key(id_calendrier,id_photo),
	foreign key(id_calendrier) references Calendrier(id_calendrier),
	foreign key(id_photo) references Photo(id_photo)
);

CREATE TABLE InventaireAgenda (
	type varchar2(255),
	modele varchar2(255),
	prix number(*,2) not null,
	stock number not null,
	PRIMARY KEY (type, modele)
);

CREATE TABLE InventaireCalendrier (
	modele varchar2(255),
	prix number(*,2) not null,
	stock number not null,
	PRIMARY KEY (modele)
);

CREATE TABLE InventairePapier (
	qualite varchar2(255),
	format varchar2(255),
	prix number(*,2)  not null,
	stock number not null,
	PRIMARY KEY (qualite, format)
);

CREATE TABLE InventaireCadre(
	taille varchar2(255),
	modele varchar2(255),
	prix number(*,2) not null,
	stock number not null,
	PRIMARY KEY (taille, modele)
);

CREATE TABLE CodePromo (
	code_promo varchar2(255) PRIMARY KEY,
	remise number not null,
	nombre_utilisation number not null,
	est_public INT not null,
	date_fin_code DATE,
	id_commande int,
	foreign key(id_commande) references Commande(id_commande),
	CONSTRAINT chk_est_public CHECK (est_public BETWEEN 0 AND 1)
);

CREATE TABLE Lk_CodePromo_Commande (
	code_promo varchar2(255) NOT NULL,
	id_commande INT NOT NULL,
	PRIMARY KEY (code_promo, id_commande),
	CONSTRAINT FK_Code_Promo FOREIGN KEY (code_promo) REFERENCES CodePromo(code_promo),
	CONSTRAINT FK_Id_Commande FOREIGN KEY (id_commande) REFERENCES Commande(id_commande)
);

CREATE TABLE Lk_AdresseLivraison_Client (
	id_adresse_livraison INT NOT NULL,
	mail_client varchar2(255) NOT NULL,
	PRIMARY KEY (id_adresse_livraison, mail_client),
	CONSTRAINT FK_Adresse_Livraison FOREIGN KEY (id_adresse_livraison) REFERENCES AdresseLivraison(id_adresse_livraison),
	CONSTRAINT FK_Mail_Client FOREIGN KEY (mail_client) REFERENCES Client(mail_client)
);
