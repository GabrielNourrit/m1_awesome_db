package type;

public enum etatFichierImage implements myEnum{
	actif,
	supprimeParUtilisateur,
	supprimeParAdministration,
	supprimePourDroitAuteur,
	problemeJuridique
}
