package util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import models.Agenda;
import models.Album;
import models.Article;
import models.Cadre;
import models.Calendrier;
import models.Client;
import models.CodePromo;
import models.Commande;
import models.FichierImage;
import models.Tirage;
import services.ConnectionJDBC;
import services.TirageService;
import type.statutCommande;

public class Contrainte {
	
	/**
	 * Permet de vérifier la contrainte : C2 : Calcul du prix total en fonction du code promo du client.
	 * @param prix le prix avant promo
	 * @param reduction pourcentage de réduction
	 * @return le prix en promo
	 */
	public static Number calculPrixPromo(Number prix, Number reduction) {
		Number n = prix.doubleValue()*(1.0 - reduction.doubleValue() / 100.0);
		n = (int)(n.doubleValue()*100)/100.;
		System.out.println(n);
		return n;
	}
	
	/**
	 * Permet de v�rifier la contrainte : C4 : Calcule du prix total d'un article en fonction de sa quantité.
	 * et C5 : Calcule du prix d'un article en fonction de l'inventaire (qualité, format)
	 * @param c la commande ou on va calculer le prix
	 * @return le prix total de la commande
	 * @throws SQLException
	 */
	public static Commande calculPrixCommande (Commande c, Connection conn) throws SQLException {
		double prix = 0;
		List<Article> la = c.getArticle();
		System.out.println("2"+la);
		for (Article a : la) {
			if (a instanceof Album) prix+=a.getQuantite().doubleValue()*getPrixInventairePapier(((Album) a).getQualite(), ((Album) a).getFormat(), conn);
			else if (a instanceof Tirage) prix+=a.getQuantite().doubleValue()*getPrixInventairePapier(((Tirage) a).getQualite(), ((Tirage) a).getFormat(), conn);
			else if (a instanceof Cadre) prix+=a.getQuantite().doubleValue()*getPrixInventaireCadre(((Cadre) a).getModele(), ((Cadre) a).getTaille(), conn);
			else if (a instanceof Agenda) prix+=a.getQuantite().doubleValue()*getPrixInventaireAgenda(((Agenda) a).getModele().toString(), ((Agenda) a).getType(), conn);
			else if (a instanceof Calendrier) prix+=a.getQuantite().doubleValue()*getPrixInventaireCalendrier(((Calendrier) a).getModele().toString(), conn);
		}
		c.setPrix_tt(prix);
		c.setPrix_tt_remise(prix);
		return c;
	}
	
	
	public static double calculPrixImpression(Article a, Connection... c) {
		int connectionLevelIsolation = Connection.TRANSACTION_SERIALIZABLE;
		Connection connection;
		boolean b = false;
		if(c.length != 0) {
			connection = c[0];
			b = true;
		}
		else connection = ConnectionJDBC.getConnection();
		double prix = 0;
		try {
			if (a instanceof Album) prix+=a.getQuantite().doubleValue()*getPrixInventairePapier(((Album) a).getQualite(), ((Album) a).getFormat(), connection);
			else if (a instanceof Tirage) prix+=a.getQuantite().doubleValue()*getPrixInventairePapier(((Tirage) a).getQualite(), ((Tirage) a).getFormat(), connection);
			else if (a instanceof Cadre) prix+=a.getQuantite().doubleValue()*getPrixInventaireCadre(((Cadre) a).getModele(), ((Cadre) a).getTaille(),connection);
			else if (a instanceof Agenda) prix+=a.getQuantite().doubleValue()*getPrixInventaireAgenda(((Agenda) a).getModele().toString(), ((Agenda) a).getType(),connection);
			else if (a instanceof Calendrier) prix+=a.getQuantite().doubleValue()*getPrixInventaireCalendrier(((Calendrier) a).getModele().toString(),connection);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if (b) try {connection.close();} catch (SQLException e) {e.printStackTrace();}
		return prix;
	}
	
	
	public static double getPrixInventairePapier(String qualite, String format, Connection connection) throws SQLException {
		Number prix = null;
		Statement stmt = null;
		try {
			stmt = connection.createStatement();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		ResultSet rs = stmt.executeQuery("select prix from inventairepapier where qualite='"+qualite+"'and format='"+format+"'");
		if (rs.next()) prix = (Number) rs.getObject(1);
		rs.close();
		stmt.close();
		return prix.doubleValue();	
	}
	
	public static double getPrixInventaireCadre(String modele, String taille, Connection connection) throws SQLException {
		Number prix = null;
		Statement stmt = null;
		try {
			stmt = connection.createStatement();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		ResultSet rs = stmt.executeQuery("select prix from inventairecadre where modele='"+modele+"'and taille='"+taille+"'");
		if (rs.next()) prix = (Number) rs.getObject(1);
		rs.close();
		stmt.close();
		return prix.doubleValue();	
	}
	
	public static double getPrixInventaireAgenda(String modele, String type, Connection connection) throws SQLException {
		Number prix = null;
		Statement stmt = null;
		try {
			stmt = connection.createStatement();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		ResultSet rs = stmt.executeQuery("select prix from InventaireAgenda where modele='"+modele+"'and type='"+type+"'");
		if (rs.next()) prix = (Number) rs.getObject(1);
		rs.close();
		stmt.close();
		return prix.doubleValue();	
	}
	
	public static double getPrixInventaireCalendrier(String modele, Connection connection) throws SQLException {
		Number prix = null;
		Statement stmt = null;
		try {
			stmt = connection.createStatement();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		ResultSet rs = stmt.executeQuery("select prix from InventaireCalendrier where modele='"+modele+"'");
		if (rs.next()) prix = (Number) rs.getObject(1);
		rs.close();
		stmt.close();
		return prix.doubleValue();	
	}
	
	/**
	 * Met � jour le stock de l'inventaire papier
	 * @param qualite
	 * @param format
	 * @param quantite
	 * @return
	 * @throws SQLException
	 */
	public static boolean decrementerStockPapier(String qualite, String format, int quantite, Connection connection) throws SQLException {
		int quantiteStock = 0;
		Statement stmt = null;
		try {
			stmt = connection.createStatement();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		ResultSet rs = stmt.executeQuery("select stock from inventairepapier where qualite='"+qualite+"'and format='"+format+"'");
		if (rs.next()) quantiteStock = rs.getInt(1);
		System.out.println("q stock " + quantiteStock);
		System.out.println("q " + quantite);
		
		if (quantiteStock < quantite) return false;
		stmt.executeUpdate("update inventairepapier set stock="+ (quantiteStock-quantite) +" where qualite='"+qualite+"'and format='"+format+"'");
		rs.close();
		stmt.close();
		return true;
	}
	
	public static boolean decrementerStockCadre(String modele, String taille, int quantite, Connection connection) throws SQLException {
		int quantiteStock = 0;
		Statement stmt = null;
		try {
			stmt = connection.createStatement();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		ResultSet rs = stmt.executeQuery("select stock from inventairecadre where modele='"+modele+"'and taille='"+taille+"'");
		if (rs.next()) quantiteStock = rs.getInt(1);
		if (quantiteStock < quantite) return false;
		stmt.executeUpdate("update inventairecadre set stock="+ (quantiteStock-quantite) +" where modele='"+modele+"'and taille='"+taille+"'");
		rs.close();
		stmt.close();
		return true;
	}
	
	public static boolean decrementerStockAgenda(String modele, String type, int quantite, Connection connection) throws SQLException {
		int quantiteStock = 0;
		Statement stmt = null;
		try {
			stmt = connection.createStatement();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		ResultSet rs = stmt.executeQuery("select stock from InventaireAgenda where modele='"+modele+"'and type='"+type+"'");
		if (rs.next()) quantiteStock = rs.getInt(1);
		if (quantiteStock < quantite) return false;
		stmt.executeUpdate("update inventaireagenda set stock="+ (quantiteStock-quantite) + " where modele='"+modele+"'and type='"+type+"'");
		rs.close();
		stmt.close();
		return true;
	}
	
	public static boolean decrementerStockCalendrier(String modele, int quantite, Connection connection) throws SQLException {
		int quantiteStock = 0;
		Statement stmt = null;
		try {
			stmt = connection.createStatement();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		ResultSet rs = stmt.executeQuery("select stock from InventaireCalendrier where modele='"+modele+"'");
		if (rs.next()) quantiteStock = rs.getInt(1);
		if (quantiteStock < quantite) return false;
		stmt.executeUpdate("update inventairecalendrier set stock="+ (quantiteStock-quantite) +" where modele='"+modele+"'");
		rs.close();
		stmt.close();
		return true;
	}
	
	

	public static void incrementerStockPapier(String qualite, String format, int quantite) throws SQLException {
		int quantiteStock = 0;
		Connection connection = ConnectionJDBC.getConnection();
		Statement stmt = null;
		try {
			stmt = connection.createStatement();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		ResultSet rs = stmt.executeQuery("select stock from inventairepapier where qualite='"+qualite+"'and format='"+format+"'");
		if (rs.next()) quantiteStock = rs.getInt(1);
		stmt.executeUpdate("update inventairepapier set stock="+ (quantiteStock+quantite) +" where qualite='"+qualite+"'and format='"+format+"'");
		rs.close();
		stmt.close();
		connection.close();
	}
	
	public static void incrementerStockCadre(String modele, String taille, int quantite) throws SQLException {
		int quantiteStock = 0;
		Connection connection = ConnectionJDBC.getConnection();
		Statement stmt = null;
		try {
			stmt = connection.createStatement();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		ResultSet rs = stmt.executeQuery("select stock from inventairecadre where modele='"+modele+"'and taille='"+taille+"'");
		if (rs.next()) quantiteStock = rs.getInt(1);
		stmt.executeUpdate("update inventairecadre set stock="+ (quantiteStock+quantite) +" where modele='"+modele+"'and taille='"+taille+"'");
		rs.close();
		stmt.close();
		connection.close();
	}
	
	public static void incrementerStockAgenda(String modele, String type, int quantite) throws SQLException {
		int quantiteStock = 0;
		Connection connection = ConnectionJDBC.getConnection();
		Statement stmt = null;
		try {
			stmt = connection.createStatement();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		ResultSet rs = stmt.executeQuery("select stock from InventaireAgenda where modele='"+modele+"'and type='"+type+"'");
		if (rs.next()) quantiteStock = rs.getInt(1);
		stmt.executeUpdate("update inventaireagenda set stock="+ (quantiteStock+quantite) + " where modele='"+modele+"'and type='"+type+"'");
		rs.close();
		stmt.close();
		connection.close();
	}
	
	public static void incrementerStockCalendrier(String modele, int quantite) throws SQLException {
		int quantiteStock = 0;
		Connection connection = ConnectionJDBC.getConnection();
		Statement stmt = null;
		try {
			stmt = connection.createStatement();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		ResultSet rs = stmt.executeQuery("select stock from InventaireCalendrier where modele='"+modele+"'");
		if (rs.next()) quantiteStock = rs.getInt(1);
		stmt.executeUpdate("update inventairecalendrier set stock="+ (quantiteStock+quantite) +" where modele='"+modele+"'");
		rs.close();
		stmt.close();
		connection.close();
	}
	
	

	/**
	 * Permet de vérifier la contrainte : C10 : Décrémente les stocks si commande payée, 
	 * si le stock est égal 0 alors commande = en attente fournisseur
	 * 
	 * et C6 : Le stock doit être mis à jour à chaque commande d'un article.
	 * @param c la commande a verifier et mettre a jour
	 * @return la commande mise a jour
	 * @throws SQLException
	 */
	public static Commande verifEtMajEtatCommande(Commande c, Connection conn) throws SQLException {
		boolean ok = true;
		List<Article> a = c.getArticle();
		System.out.println(a);
		int taille = a.size();
		int i = 0;
		while (i < taille && ok) {
			System.out.print(a.get(i) instanceof Tirage);
			System.out.println(a.get(i));
			if (a.get(i) instanceof Album) {
				boolean b = decrementerStockPapier(((Album) a.get(i)).getQualite(), ((Album) a.get(i)).getFormat(), a.get(i).getQuantite().intValue(), conn);
				if (!b) ok = false;
			}
			else if (a.get(i) instanceof Tirage) {
				boolean b = decrementerStockPapier(((Tirage) a.get(i)).getQualite(), ((Tirage) a.get(i)).getFormat(), a.get(i).getQuantite().intValue(), conn);
				System.out.println("b ++++ " + b);
				if (!b) ok = false;
			}
			else if (a.get(i) instanceof Cadre) { 
				boolean b = decrementerStockCadre(((Cadre) a.get(i)).getModele(), ((Cadre) a.get(i)).getTaille(), a.get(i).getQuantite().intValue(), conn);
				if (!b) ok = false;
			}
			else if (a.get(i) instanceof Agenda) {
				boolean b = decrementerStockAgenda(((Agenda) a.get(i)).getModele().toString(), ((Agenda) a.get(i)).getType(), a.get(i).getQuantite().intValue(), conn);
				if (!b) ok = false;
			}
			else if (a.get(i) instanceof Calendrier) { 
				boolean b = decrementerStockCalendrier(((Calendrier) a.get(i)).getModele().toString(), a.get(i).getQuantite().intValue(), conn);
				if (!b) ok = false;
			}
			i++;
		}
		System.out.println(ok);
		if (ok) c.setStatut(statutCommande.Paye);
		else c.setStatut(statutCommande.EnAttenteFournisseur);
		
		return c;
	}
	
	/**
	 * Permet de vérifier la contrainte : Un  client  ne  peut  transformer un fichier  partagé  par 
	 * une  autre  personne en photo que  si lui même partage au moins un fichier
	 * @param c le client a verifier
	 * @return true si le client peut utiliser un fichier partage false sinon
	 */
	public static boolean peutUtiliserFichierPartage(Client c) {
		boolean partage = false;
		List<FichierImage> f = c.getFichiersImage();
		int taille = f.size();
		int i = 0;
		while (i < taille && !partage) {
			if (f.get(i).isEst_partage()) partage = true; 
			i++;
		}
		return partage;
	}
	
	
	/**
	 * Verifie si une commande est > 100€
	 * @param c
	 * @return
	 */
	public static boolean estEligibleCodePromo(Commande c) {
		if (c.getPrix_tt().doubleValue() > 100.0) {
			return true;
		}
		return false;
	}
	
	/**
	 * La methode genere le code promo
	 * @return
	 */
	public static String genererCodePromo()
	{
		String chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		String codePromo = "";
		for(int x=0;x<10;x++)
		{
			int i = (int)Math.floor(Math.random() * 62);
			codePromo += chars.charAt(i);
		}
		return codePromo;
	}
	
	/*public static boolean estUtilisableCodePromo(String codePromo) {
		Connection connection = ConnectionJDBC.getConnection();
		try {
			connection.setAutoCommit(false);
			connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		TirageService t = new TirageService();
		CodePromo cp = null;
		try {
			cp = (CodePromo) t.findById(CodePromo.class, codePromo, Connection.TRANSACTION_READ_COMMITTED);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if (cp != null) {
			if (cp.getEst_public().intValue() == 0 && cp.getNombre_utilisation().intValue() == 0) return true;
			else if (cp.getEst_public().intValue() == 1)
		}
		return false;
	}*/
	
	
}
