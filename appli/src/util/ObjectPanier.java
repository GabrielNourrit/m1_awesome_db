package util;

import javafx.beans.property.SimpleStringProperty;
import services.IgnoreQueryProperty;

public class ObjectPanier {

	@IgnoreQueryProperty
	private SimpleStringProperty id_impression;
	@IgnoreQueryProperty
    private SimpleStringProperty type;
	@IgnoreQueryProperty
    private SimpleStringProperty PU;
	@IgnoreQueryProperty
    private SimpleStringProperty reduction;
	@IgnoreQueryProperty
    private SimpleStringProperty quantite;
	
	
	public String getId_impression() {
		return id_impression.get();
	}
	public void setId_impression(SimpleStringProperty id_impression) {
		this.id_impression = id_impression;
	}
	public String getType() {
		return type.get();
	}
	public void setType(SimpleStringProperty type) {
		this.type = type;
	}
	public String getPU() {
		return PU.get();
	}
	public void setPU(SimpleStringProperty pU) {
		PU = pU;
	}
	public String getReduction() {
		return reduction.get();
	}
	public void setReduction(SimpleStringProperty reduction) {
		this.reduction = reduction;
	}
	public String getQuantite() {
		return quantite.get();
	}
	public void setQuantite(SimpleStringProperty quantite) {
		this.quantite = quantite;
	}
	
}
