package client;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import models.AdresseLivraison;
import models.Agenda;
import models.Album;
import models.Article;
import models.Cadre;
import models.Calendrier;
import models.Client;
import models.CodePromo;
import models.Commande;
import models.FichierImage;
import models.Impression;
import models.LK;
import models.LkImpressionnable;
import models.Lk_Agenda_Photo;
import models.Lk_Album_Photo;
import models.Lk_Cadre_Photo;
import models.Lk_Calendrier_Photo;
import models.Lk_CodePromo_Commande;
import models.Lk_Tirage_Photo;
import models.Photo;
import models.Tirage;
import services.ConnectionJDBC;
import services.InsertService;
import services.TirageService;
import services.UpdateService;
import type.agendaModel;
import type.calendrierModel;
import type.clientEtat;
import type.etatFichierImage;
import type.modeLivraison;
import type.statutCommande;
import util.Contrainte;
import util.LectureClavier;
import util.ObjectPanier;

public class Controller implements Initializable {

	private List<FichierImage> listeFichierImage;

	@FXML
	private TextField codePRForm;
	
	@FXML
	private ListView<AdresseLivraison> listAdresseLivraison;
	
	@FXML
	private Label descrCommande;
	
	@FXML
	private HBox loaderConnexion;

	@FXML
	private ImageView loader;

	@FXML
	private TextField mailInscr;

	@FXML
	private TextField nomInscr;

	@FXML
	private TextField prenomInscr;

	@FXML
	private TextArea adresseInscr;
	
	@FXML
	private TextField codePromoForm;

	@FXML
	private TableView<Commande> mesCommandes;

	@FXML
	private TableColumn<Commande, String> date;

	@FXML
	private TableColumn<Commande, String> mode;

	@FXML
	private TableColumn<Commande, String> prix;

	@FXML
	private TableColumn<Commande, String> statut;

	@FXML
	private ListView<String> codeReduc;

	@FXML
	private Button retourNav;

	@FXML
	private Text alert;

	@FXML
	private Button btnModifInfo;

	@FXML
	private TextField oldPass;

	@FXML
	private PasswordField passwordInscr;

	@FXML
	private Button btnInscr;

	@FXML
	private Button btnCoAcc;
	@FXML
	private Button monCompte;

	@FXML
	private Button panier;

	@FXML
	private Button deconnexion;

	@FXML
	private Button btnCollection;

	@FXML
	private Button btnImpression;

	@FXML
	private TextField mailConn;

	@FXML
	private PasswordField passwordConn;

	@FXML
	private Text alertConnexion;

	@FXML
	private Button btnCoConn;

	@FXML
	private TableView<FichierImage> tabPictures;

	@FXML
	private Button payer;

	@FXML
	private TableView<ObjectPanier> tabPanier;

	@FXML
	private TableColumn<ObjectPanier, String> id_impr;

	@FXML
	private TableColumn<ObjectPanier, String> type;

	@FXML
	private TableColumn<ObjectPanier, String> prixu;

	@FXML
	private TableColumn<ObjectPanier, String> reduc;

	@FXML
	private TableColumn<ObjectPanier, String> quantite;


	@FXML
	private Button btnDel;

	@FXML
	private Button btnHome;

	@FXML
	private Button btnAddPicture;

	@FXML
	private TableColumn<FichierImage, String> libelleImg;

	@FXML
	private TableColumn<FichierImage, String> pathImg;

	@FXML
	private TableColumn<FichierImage, String> infosImg;

	@FXML
	private TableColumn<FichierImage, String> resImg;

	@FXML
	private TableColumn<FichierImage, String> dateImg;

	@FXML
	private TableColumn<FichierImage, String> partageImg;

	@FXML
	private TextField newlibelleImg;

	@FXML
	private TextField newPathImg;

	@FXML
	private TextField newInfosImg;

	@FXML
	private TextField newResolutionImg;

	@FXML
	private CheckBox newChkImg;

	@FXML
	private Text alertAddImg;

	@FXML
	private Button btnAddImg;

	@FXML
	private Button btnSharePicture;

	@FXML
	private Button btnDelPicture;

	@FXML
	private Button btnChoiceCadre;

	@FXML
	private Button btnChoiceCalendrier;

	@FXML
	private Button btnChoiceAlbum;

	@FXML
	private Button btnChoiceTirage;

	@FXML
	private Button btnChoiceAgenda;


	@FXML
	private ListView<Photo> selectedPhotos;

	@FXML
	private Button addPhotosElement;

	@FXML
	private ListView<Photo> myPhotos;

	@FXML
	private Button delPhotosElement;

	@FXML
	private Button newPhotos;

	@FXML
	private Button addPhotos;

	@FXML
	private ListView<FichierImage> ListPictures;

	@FXML
	private ListView<FichierImage> tabPicturesPublic;

	@FXML
	private Button newPhotosForm;

	@FXML
	private TextArea retouches;





	@FXML
	private ListView<Photo> viewPhotosForm;

	@FXML
	private Button gestionPhotos;

	@FXML
	private ComboBox<String> qualiteForm;

	@FXML
	private ComboBox<String> formatForm;

	@FXML
	private Text alertTirageForm;

	@FXML
	private Button btnAddPanier;


	@FXML
	private Text alertCalendrierForm;

	@FXML
	private TextArea cssForm;

	@FXML
	private ComboBox<String> modeleForm;

	@FXML
	private ComboBox<String> typeForm;

	@FXML
	private Text alertAgendaForm;

	@FXML
	private ComboBox<String> tailleForm;

	@FXML
	private Text alertCadreForm;

	
	private boolean testAgendaAddPanier(agendaModel modele) {
		for (Object lk : Main.getListeLienImpressionPhoto()) {
			if (lk instanceof Lk_Agenda_Photo) {
				if (((Lk_Agenda_Photo) lk).getNo_page().intValue() < 1) {
					alertCalendrierForm.setText("Erreur : veuillez choisir une page supérieur à 0");
					return false;
				}
				else if (modele.equals(agendaModel.S52) && ((Lk_Agenda_Photo) lk).getNo_page().intValue() > 52) {
					alertTirageForm.setText("Erreur : veuillez choisir une page entre 1 et 52 pour un modèle 52");
				}
				else if (modele.equals(agendaModel.J365) && ((Lk_Agenda_Photo) lk).getNo_page().intValue() > 365) {
					alertTirageForm.setText("Erreur : veuillez choisir une page entre 1 et 365 pour un modèle 365");
				}
			}
		}
		return true;
	}
	
	private boolean testCalendrierAddPanier() {
		if(viewPhotosForm.getItems().size()==0){
			alertCalendrierForm.setText("Erreur : Veuillez choisir au moins 1 photos !");
			return false;
		}

		if(Main.getListeLienImpressionPhoto().size()==0) {
			alertCalendrierForm.setText("Erreur : Vous n'avez fixe aucune page ni ordre de photo !");
			return false;
		}

		if(viewPhotosForm.getItems().size()==Main.getListeLienImpressionPhoto().size()) {
			for(Photo p : viewPhotosForm.getItems()) {
				if(!this.estUnEntier(Main.getLienCalendrierByPhoto(p).getOrdre()+"") || !this.estUnEntier(Main.getLienCalendrierByPhoto(p).getPage()+"")) {
					alertCalendrierForm.setText("Erreur : Veuillez fixer les pages et ordre de toutes les photos !");
					return false;
				}
			}
		} else {
			alertCalendrierForm.setText("Erreur : Veuillez fixer les pages et ordre de toutes les photos !");
			return false;
		}

		boolean c1 = modeleForm.selectionModelProperty().getValue().getSelectedItem()==null;
		if(c1) {
			alertCalendrierForm.setText("Erreur : Veuillez selectionner un modele !");
			return false;
		}
		
		for (Object lk : Main.getListeLienImpressionPhoto()) {
			if (lk instanceof Lk_Calendrier_Photo) {
				if (((Lk_Calendrier_Photo) lk).getPage().intValue() > 12 || ((Lk_Calendrier_Photo) lk).getPage().intValue() < 1) {
					alertCalendrierForm.setText("Erreur : veuillez choisir une page entre 1 et 12");
					return false;
				}
			}
		}
		
		return true;
	}
	@FXML
	void addPanier(ActionEvent event) {
		Article a;
		Impression i;
		int idart = 0;
		switch(Main.getAddPhotoNavOrigin()) {
		case "calendrier":
			if(!this.testCalendrierAddPanier()) return;

			//TODO : Inserer article + recuperer id insere
			a = new Article();
			a.setQuantite(1);
			a.setId_commande(Main.getCommandeEnCours().getId_commande());
			try {
				idart = (int) InsertService.insert(a, Connection.TRANSACTION_SERIALIZABLE);
			} catch (SQLException e9) {
				// TODO Auto-generated catch block
				e9.printStackTrace();
			}

			//tout est passé si on est là
			//on fait persister nos lk_dynamiques en lk_temporaire en base
			Calendrier c = new Calendrier();
			c.setQuantite(1);
			c.setId_calendrier(idart);
			c.setModele(modeleForm.selectionModelProperty().getValue().getSelectedItem());
			((ArrayList<Lk_Calendrier_Photo>)Main.getListeLienImpressionPhoto()).forEach(lk_cp->{
				lk_cp.setId_calendrier(c);
				LK.getInstance().ajouter_Lk_Calendrier_Photo(lk_cp);
			});
			//puis on vide le cache dynamique
			Main.setListeLienImpressionPhoto(new ArrayList<>());
			//TODO : Inserer Impression avec id insere plus tot
			//TODO : calcul du prix
			i = new Impression();
			i.setId_impression(idart);
			i.setPu(Contrainte.calculPrixImpression(c));
			i.setEstpromo(0);

			try {
				InsertService.insert(i, Connection.TRANSACTION_SERIALIZABLE);
				//TODO : Inserer Calendrier avec id insere
				InsertService.insert(c, Connection.TRANSACTION_SERIALIZABLE);
			} catch (SQLException e8) {
				// TODO Auto-generated catch block
				e8.printStackTrace();
			}

			Main.getCommandeEnCours().ajoutArticle(c);
			

			break;

		case "album":
			//TODO : Inserer article + recuperer id insere
			a = new Article();
			a.setQuantite(1);
			a.setId_commande(Main.getCommandeEnCours().getId_commande());
			try {
				idart = (int) InsertService.insert(a, Connection.TRANSACTION_READ_COMMITTED);
			} catch (SQLException e7) {
				// TODO Auto-generated catch block
				e7.printStackTrace();
			}

			//tout est passé si on est là
			//on fait persister nos lk_dynamiques en lk_temporaire en base
			Album al = new Album();
			al.setQuantite(1);
			al.setTitre("Mon super album");
			al.setId_album(idart);
			al.setQualite(this.qualiteForm.selectionModelProperty().getValue().getSelectedItem());
			al.setId_photo(this.photoTitreForm.selectionModelProperty().getValue().getSelectedItem());
			al.setFormat(this.formatForm.selectionModelProperty().getValue().getSelectedItem());
			((ArrayList<Lk_Album_Photo>)Main.getListeLienImpressionPhoto()).forEach(lk_cp->{
				lk_cp.setId_album(al);
				LK.getInstance().ajouter_Lk_Album_Photo(lk_cp);
			});
			//puis on vide le cache dynamique
			Main.setListeLienImpressionPhoto(new ArrayList<>());
			//TODO : Inserer Impression avec id insere plus tot
			//TODO : calcul du prix
			i = new Impression();
			i.setId_impression(idart);
			i.setPu(Contrainte.calculPrixImpression(al));
			i.setEstpromo(0);

			try {
				InsertService.insert(i, Connection.TRANSACTION_READ_COMMITTED);
				//TODO : Inserer Calendrier avec id insere
				InsertService.insert(al, Connection.TRANSACTION_READ_COMMITTED);
			} catch (SQLException e6) {
				// TODO Auto-generated catch block
				e6.printStackTrace();
			}

			Main.getCommandeEnCours().ajoutArticle(al);

			break;
		case "tirage":
			//TODO : Inserer article + recuperer id insere
			a = new Article();
			a.setQuantite(1);
			a.setId_commande(Main.getCommandeEnCours().getId_commande());
			try {
				idart = (int) InsertService.insert(a, Connection.TRANSACTION_READ_COMMITTED);
			} catch (SQLException e5) {
				// TODO Auto-generated catch block
				e5.printStackTrace();
			}

			//tout est passé si on est là
			//on fait persister nos lk_dynamiques en lk_temporaire en base
			Tirage ti = new Tirage();
			ti.setQuantite(1);
			ti.setId_tirage(idart);
			ti.setQualite(this.qualiteForm.selectionModelProperty().getValue().getSelectedItem());
			ti.setFormat(this.formatForm.selectionModelProperty().getValue().getSelectedItem());
			((ArrayList<Lk_Tirage_Photo>)Main.getListeLienImpressionPhoto()).forEach(lk_cp->{
				lk_cp.setId_tirage(ti);
				LK.getInstance().ajouter_Lk_Tirage_Photo(lk_cp);
			});
			//puis on vide le cache dynamique
			Main.setListeLienImpressionPhoto(new ArrayList<>());
			//TODO : Inserer Impression avec id insere plus tot
			//TODO : calcul du prix
			i = new Impression();
			i.setId_impression(idart);
			i.setPu(Contrainte.calculPrixImpression(ti));
			i.setEstpromo(0);

			try {
				InsertService.insert(i, Connection.TRANSACTION_READ_COMMITTED);
				InsertService.insert(ti, Connection.TRANSACTION_READ_COMMITTED);
			} catch (SQLException e4) {
				// TODO Auto-generated catch block
				e4.printStackTrace();
			}

			//TODO : Inserer Calendrier avec id insere
			
			Main.getCommandeEnCours().ajoutArticle(ti);
			
			break;
		case "cadre":
			//TODO : Inserer article + recuperer id insere
			a = new Article();
			a.setQuantite(1);
			a.setId_commande(Main.getCommandeEnCours().getId_commande());
			try {
				idart = (int) InsertService.insert(a, Connection.TRANSACTION_READ_COMMITTED);
			} catch (SQLException e3) {
				// TODO Auto-generated catch block
				e3.printStackTrace();
			}

			//tout est passé si on est là
			//on fait persister nos lk_dynamiques en lk_temporaire en base
			Cadre ca = new Cadre();
			ca.setQuantite(1);
			ca.setId_cadre(idart);
			ca.setMise_en_page(this.cssForm.getText());
			ca.setModele(this.modeleForm.selectionModelProperty().getValue().getSelectedItem());
			ca.setTaille(this.tailleForm.selectionModelProperty().getValue().getSelectedItem());
			((ArrayList<Lk_Cadre_Photo>)Main.getListeLienImpressionPhoto()).forEach(lk_cp->{
				lk_cp.setId_cadre(ca);
				LK.getInstance().ajouter_Lk_Cadre_Photo(lk_cp);
			});
			//puis on vide le cache dynamique
			Main.setListeLienImpressionPhoto(new ArrayList<>());
			//TODO : Inserer Impression avec id insere plus tot
			//TODO : calcul du prix
			i = new Impression();
			i.setId_impression(idart);
			i.setPu(Contrainte.calculPrixImpression(ca));
			i.setEstpromo(0);

			try {
				InsertService.insert(i, Connection.TRANSACTION_READ_COMMITTED);
				//TODO : Inserer Calendrier avec id insere
				InsertService.insert(ca, Connection.TRANSACTION_READ_COMMITTED);
			} catch (SQLException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}

			Main.getCommandeEnCours().ajoutArticle(ca);
			
			break;
		case "agenda":
			//TODO : Inserer article + recuperer id insere
			a = new Article();
			a.setQuantite(1);
			a.setId_commande(Main.getCommandeEnCours().getId_commande());
			try {
				idart = (int) InsertService.insert(a, Connection.TRANSACTION_READ_COMMITTED);
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			//tout est passé si on est là
			//on fait persister nos lk_dynamiques en lk_temporaire en base
			Agenda ag = new Agenda();
			ag.setQuantite(1);
			ag.setId_agenda(idart);
			ag.setModele(this.modeleForm.selectionModelProperty().getValue().getSelectedItem());
			ag.setType(this.typeForm.selectionModelProperty().getValue().getSelectedItem());
			((ArrayList<Lk_Agenda_Photo>)Main.getListeLienImpressionPhoto()).forEach(lk_cp->{
				lk_cp.setId_agenda(ag);
				LK.getInstance().ajouter_Lk_Agenda_Photo(lk_cp);
			});
			//puis on vide le cache dynamique
			Main.setListeLienImpressionPhoto(new ArrayList<>());
			//TODO : Inserer Impression avec id insere plus tot
			//TODO : calcul du prix
			i = new Impression();
			i.setId_impression(idart);
			i.setPu(Contrainte.calculPrixImpression(ag));
			i.setEstpromo(0);

			try {
				InsertService.insert(i, Connection.TRANSACTION_READ_COMMITTED);
				//TODO : Inserer Calendrier avec id insere
				InsertService.insert(ag);
			} catch (SQLException e) {
				e.printStackTrace();
			}

			Main.getCommandeEnCours().ajoutArticle(ag);
			
			break;
		};

		this.changementDeFenetre("connexion.fxml", event);
	}

	@FXML
	void renderPhotos(ActionEvent event) {
		switch(Main.getAddPhotoNavOrigin()) {
		case "calendrier":
			//on reinit parametre
			List<Lk_Calendrier_Photo> lk_p = new ArrayList<>();
			Main.setListeLienImpressionPhoto(lk_p);
			break;
		};
		this.changementDeFenetre("addPhotos.fxml", event);
	}

	//-----------------------

	@FXML
	private TextField pageForm;

	@FXML
	private TextField ordreForm;

	@FXML
	private TextArea descrForm;

	@FXML
	private ComboBox<Photo> photoTitreForm;

	private Photo old_photo = new Photo();

	@FXML
	void majViewForm(MouseEvent event) {
		// La photo selectionné
		Photo photo = this.viewPhotosForm.getSelectionModel().getSelectedItem();

		List<Lk_Calendrier_Photo> lk_cp;
		List<Lk_Album_Photo> lk_alp;

		switch(Main.getAddPhotoNavOrigin()) {
		case "calendrier":
			//on reinit parametre

			if(photo!=null) {
				Lk_Calendrier_Photo lk = majViewFormAssist(Lk_Calendrier_Photo.class, photo, Calendrier.class);
				//On rempli le formulaire avec la table lien
				if(ordreForm!=null) this.ordreForm.setText("");
				if(pageForm!=null) this.pageForm.setText("");
				if (lk.getOrdre()!=null) this.ordreForm.setText(lk.getOrdre().toString());
				if (lk.getPage()!=null) this.pageForm.setText(lk.getPage().toString());
			}
			break;
		case "album":
			//on reinit parametre

			if(photo!=null) {
				Lk_Album_Photo lk = majViewFormAssist(Lk_Album_Photo.class, photo, Album.class);
				//On rempli le formulaire avec la table lien
				if(ordreForm!=null) this.ordreForm.setText("");
				if(pageForm!=null) this.pageForm.setText("");
				if (lk.getDescription()!=null) {
					this.descrForm.setText(lk.getDescription().toString());	
				}
				else this.descrForm.setText("");
				if (lk.getOrdre()!=null) this.ordreForm.setText(lk.getOrdre().toString());
				if (lk.getPage()!=null) this.pageForm.setText(lk.getPage().toString());
			}
			break;
		case "tirage":
			if(photo!=null) {
				Lk_Tirage_Photo lk = majViewFormAssist(Lk_Tirage_Photo.class, photo, Tirage.class);
			}
			break;
		case "cadre":
			//on reinit parametre

			if(photo!=null) {
				Lk_Cadre_Photo lk = majViewFormAssist(Lk_Cadre_Photo.class, photo, Cadre.class);
				//On rempli le formulaire avec la table lien
				if(pageForm!=null) this.pageForm.setText("");
				if (lk.getPage()!=null) this.pageForm.setText(lk.getPage().toString());
			}
			break;
		case "agenda":
			//on reinit parametre

			if(photo!=null) {
				Lk_Agenda_Photo lk = majViewFormAssist(Lk_Agenda_Photo.class, photo, Agenda.class);
				//On rempli le formulaire avec la table lien
				if(pageForm!=null) this.pageForm.setText("");
				if (lk.getNo_page()!=null) this.pageForm.setText(lk.getNo_page().toString());
			}
			break;
		};


	}

	private <T extends LkImpressionnable> T majViewFormAssist(Class<T> c, Photo photo, Class<?> c2) {

		// Recupere la liste des liens 
		List<T> listeLienPhotoCalendrier = (ArrayList<T>)Main.getListeLienImpressionPhoto();

		boolean exist = false;
		// Recupere le lien associé à la photo selectionné

		T lk = null;
		for(T p2: listeLienPhotoCalendrier) {

			if(photo.getId_photo() == p2.getId_photo().getId_photo()){
				lk = p2;
				exist = true;
			} 
		}


		if(!exist) {
			//Je crée le lien avec photo
			T l = null;
			try {
				l = c.newInstance();
				l.setId(c2.newInstance());
			} catch (Exception e2) {e2.printStackTrace();}
			l.setId_photo(photo);
			listeLienPhotoCalendrier.add(l);
			Main.setListeLienImpressionPhoto(listeLienPhotoCalendrier);
			lk = l;
		}

		return lk;

	}


	//--------------
	@FXML
	void delPanier(ActionEvent event) {
		Connection connection = ConnectionJDBC.getConnection();
		try {
			connection.setAutoCommit(false);
			connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		TirageService t = new TirageService();
		ObservableList<ObjectPanier> selectedRows = tabPanier.getSelectionModel().getSelectedItems();
		ArrayList<ObjectPanier> rows = new ArrayList<>(selectedRows);
		rows.forEach(row -> {
			String idtmp = row.getId_impression();
			int id = Integer.parseInt(idtmp);
			if (row.getType().equals("Agenda")) {
				Agenda ag = new Agenda();
				ag.setId_agenda(id);
				t.deleteObject(ag, connection);
			}
			else if (row.getType().equals("Album")) {
				Album al = new Album();
				al.setId_album(id);
				t.deleteObject(al, connection);
			}
			else if (row.getType().equals("Cadre")) {
				Cadre cad = new Cadre();
				cad.setId_cadre(id);
				t.deleteObject(cad, connection);
			}
			else if (row.getType().equals("Calendrier")) {
				Calendrier cal = new Calendrier();
				cal.setId_calendrier(id);
				t.deleteObject(cal, connection);
			}
			if (row.getType().equals("Tirage")) {
				Tirage ti = new Tirage();
				ti.setId_tirage(id);
				t.deleteObject(ti, connection);
			}
			Impression i = new Impression();
			i.setId_impression(id);
			t.deleteObject(i, connection);
			
			Article a = new Article();
			a.setId_article(id);
			t.deleteObject(a, connection);	
			
			tabPanier.getItems().remove(row);		
		});
		
		try {
			connection.commit();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}


	@FXML
	void payer(ActionEvent event) {

	}

	@FXML
	void modifInfo(ActionEvent event) {
		Client c = Main.getClientConnecte();
		if (c.getPassword().equals(this.oldPass.getText())) {
			c.setPassword(this.passwordInscr.getText());
			UpdateService.update(c, Connection.TRANSACTION_READ_COMMITTED);
			this.oldPass.setText("");
			this.passwordInscr.setText("");
			this.alert.setText("Votre mot de passse a été modifié");
		}
	}

	@FXML
	void retourNav(ActionEvent event) {

	}
	@FXML
	void newPhotosElement(ActionEvent event) {
		List<FichierImage> listeFichierSelectionne = this.ListPictures.getSelectionModel().getSelectedItems();
		List<FichierImage> listeFichierSelectionnePub = this.tabPicturesPublic.getSelectionModel().getSelectedItems();
		for(FichierImage i : listeFichierSelectionne) {
			Photo photo = new Photo();
			photo.setId_fic(i);
			//photo.setId_fic(i.getId_fic());
			photo.setMail_client(Main.getClientConnecte().getMail_client());
			photo.setRetouches(this.retouches.getText());
			photo.setEtat("actif");
			try {
				InsertService.insert(photo, Connection.TRANSACTION_READ_COMMITTED);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		for(FichierImage i : listeFichierSelectionnePub) {
			Photo photo = new Photo();
			photo.setId_fic(i);
			//photo.setId_fic(i.getId_fic());
			photo.setMail_client(Main.getClientConnecte().getMail_client());
			photo.setRetouches(this.retouches.getText());
			photo.setEtat("actif");
			try {
				InsertService.insert(photo, Connection.TRANSACTION_READ_COMMITTED);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		this.changementDeFenetre("addPhotos.fxml", event);
	}


	@FXML
	void renderAgenda(ActionEvent event) {
		Main.setAddPhotoNavOrigin("agenda");
		this.changementDeFenetre("agenda.fxml", event);
	}

	@FXML
	void renderAlbum(ActionEvent event) {
		Main.setAddPhotoNavOrigin("album");
		this.changementDeFenetre("album.fxml", event);
	}

	@FXML
	void renderCadre(ActionEvent event) {
		Main.setAddPhotoNavOrigin("cadre");
		this.changementDeFenetre("cadre.fxml", event);
	}

	@FXML
	void renderCalendrier(ActionEvent event) {
		Main.setAddPhotoNavOrigin("calendrier");
		this.changementDeFenetre("calendrier.fxml", event);
	}

	@FXML
	void renderTirage(ActionEvent event) {
		Main.setAddPhotoNavOrigin("tirage");
		this.changementDeFenetre("tirage.fxml", event);
	}


	@FXML
	void delPicture(ActionEvent event) {
		ObservableList<FichierImage> selectedRows = tabPictures.getSelectionModel().getSelectedItems();
		ArrayList<FichierImage> rows = new ArrayList<>(selectedRows);
		rows.forEach(row -> {
			row.setEtat(etatFichierImage.supprimeParUtilisateur.toString());
			row.setEst_actif(0);
			UpdateService.update(row, Connection.TRANSACTION_READ_COMMITTED);
			tabPictures.getItems().remove(row);
		});
	}



	@FXML
	void addPicture(ActionEvent event) {
		this.changementDeFenetre("addImageForm.fxml", event);
		//FichierImage f1 = new FichierImage("a","a","a","a","a",false);

		//tabPictures.getItems().addAll(f1);

		/*libelleImg.setCellValueFactory(new PropertyValueFactory<FichierImage, String>("Libelle"));
		pathImg.setCellValueFactory(new PropertyValueFactory<FichierImage, String>("Path"));
		infosImg.setCellValueFactory(new PropertyValueFactory<FichierImage, String>("Infos"));
		resImg.setCellValueFactory(new PropertyValueFactory<FichierImage, String>("Resolution"));
		dateImg.setCellValueFactory(new PropertyValueFactory<FichierImage, String>("DateAjout"));
		partageImg.setCellValueFactory(new PropertyValueFactory<FichierImage, String>("Partage"));*/

	}

	@FXML
	void addImg(ActionEvent event) {
		FichierImage fichier = new FichierImage();
		fichier.setLibelle(this.newlibelleImg.getText());
		fichier.setChemin(this.newPathImg.getText());
		fichier.setInfos_vue(this.newInfosImg.getText());
		fichier.setResolution(this.newResolutionImg.getText());
		fichier.setEst_partage(this.newChkImg.isSelected()?1:0);
		fichier.setEst_actif(1);
		fichier.setDate_ajout(new Timestamp(System.currentTimeMillis()));
		fichier.setEtat(etatFichierImage.actif.name());
		fichier.setMail_client(Main.getClientConnecte());
		try {
			InsertService.insert(fichier);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.changementDeFenetre("gestionImages.fxml", event);




	}

	@FXML
	void sharePicture(ActionEvent event) {
		ObservableList<FichierImage> selectedRows = tabPictures.getSelectionModel().getSelectedItems();
		ArrayList<FichierImage> rows = new ArrayList<>(selectedRows);
		rows.forEach(row -> {
			FichierImage f = listeFichierImage.get(listeFichierImage.indexOf(row));
			f.switchPartage();
			UpdateService.update(row, Connection.TRANSACTION_READ_COMMITTED);

		});

		//refresh de l'affichage
		ArrayList<FichierImage> save = new ArrayList<FichierImage>();
		save.addAll(tabPictures.getItems());
		tabPictures.getItems().removeAll(save);
		tabPictures.getItems().addAll(save);
		partageImg.setCellValueFactory(new PropertyValueFactory<FichierImage, String>("partageColumn"));

	}

	@FXML
	void clickCollectionByPct(MouseEvent event) {
		changementDeFenetre("gestionImages.fxml",event);
	}

	@FXML
	void gotoImpressionByPict(MouseEvent event) {
		changementDeFenetre("choiceImpression.fxml",event);
	}



	@FXML
	void gotoHome(ActionEvent event) {
		changementDeFenetre("accueil.fxml",event);
	}

	@FXML
	void gotoHome2(ActionEvent event) {
		Main.setPhotoSelectionne(new ArrayList<>());
		changementDeFenetre("connexion.fxml",event);
	}

	@FXML
	void deco(ActionEvent event) {
		Client c = Main.getClientConnecte();
		c.setEst_connecte(0);
		UpdateService.update(c, Connection.TRANSACTION_READ_COMMITTED);
		Main.setClientConnecte(null);
		changementDeFenetre("accueil.fxml",event);
	}





	@FXML
	void clickCollection(ActionEvent event) {
		changementDeFenetre("gestionImages.fxml",event);
	}

	@FXML
	void gotoImpression(ActionEvent event) {
		changementDeFenetre("choiceImpression.fxml",event);
	}


	@FXML
	void addPhotosElement(ActionEvent event) {

		ObservableList<Photo> selectedRows = myPhotos.getSelectionModel().getSelectedItems();
		ArrayList<Photo> rows = new ArrayList<>(selectedRows);

		rows.forEach(row -> {
			selectedPhotos.getItems().addAll(row);
			myPhotos.getItems().remove(row);
		});




	}
	@FXML
	void delPhotosElement(ActionEvent event) {
		ObservableList<Photo> selectedRows = selectedPhotos.getSelectionModel().getSelectedItems();
		ArrayList<Photo> rows = new ArrayList<>(selectedRows);

		rows.forEach(row -> {
			selectedPhotos.getItems().remove(row);
			myPhotos.getItems().add(row);
		});

	}


	@FXML
	void newPhotos(ActionEvent event) {
		changementDeFenetre("newPhotos.fxml",event);
	}

	@FXML
	void renderImpress(ActionEvent event) {
		//Recupere les photos selectionné
		List<Photo> listePhotoChoisi = this.selectedPhotos.getItems();
		Main.setPhotoSelectionne(listePhotoChoisi);
		Main.setListeLienImpressionPhoto(new ArrayList<>());

		if(Main.getAddPhotoNavOrigin().equals("tirage")) {
			changementDeFenetre("tirage.fxml",event);
		}else if(Main.getAddPhotoNavOrigin().equals("album")) {
			changementDeFenetre("album.fxml",event);
		}else if(Main.getAddPhotoNavOrigin().equals("cadre")) {
			changementDeFenetre("cadre.fxml",event);
		}else if(Main.getAddPhotoNavOrigin().equals("agenda")) {
			changementDeFenetre("agenda.fxml",event);
		}else if(Main.getAddPhotoNavOrigin().equals("calendrier")) {
			changementDeFenetre("calendrier.fxml",event);
		}
	}

	@FXML
	void connexion(ActionEvent event) {
		changementDeFenetre("connection.fxml",event);
	}





	private void changementDeFenetre(String name, Event event) {

		Parent blah = null;
		try {
			blah = FXMLLoader.load(getClass().getResource(name));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		Scene scene = new Scene(blah);
		Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		appStage.setScene(scene);
		appStage.show();
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		String a[] = arg0.toString().split("/");
		String b = a[a.length-1];



		switch(b){
		case "agenda.fxml":
			this.typeForm.getItems().addAll("grand","petit");
			this.modeleForm.getItems().addAll(agendaModel.S52.toString(), agendaModel.J365.toString());
			this.viewPhotosForm.getItems().addAll(Main.getPhotoSelectionne());
			this.pageForm.textProperty().addListener((obs, oldText, newText) -> {
				Photo p = viewPhotosForm.getSelectionModel().getSelectedItem();

				if(p!=null) {
					if(this.estUnEntier(this.pageForm.getText())) {
						Lk_Agenda_Photo lc = Main.getLienAgendaByPhoto(p); 
						lc.setNo_page(Integer.parseInt(newText));
						List<Lk_Agenda_Photo> lk2 = (List<Lk_Agenda_Photo>) Main.getListeLienImpressionPhoto();
						int index = lk2.indexOf(lc); 
						lk2.remove(lc);
						lk2.add(index,lc);
						Main.setListeLienImpressionPhoto(lk2);
					}
				}
			});
			break;
		case "album.fxml":
			//this.modeleForm.getItems().addAll(albumModel., calendrierModel.Mural.name());
			List<Photo> listePhoto = Main.getClientConnecte().loadPhoto();
			this.photoTitreForm.getItems().addAll(listePhoto);
			this.formatForm.getItems().addAll("A4", "A3");
			this.qualiteForm.getItems().addAll("80g", "120g");
			//for (Photo p : listePhoto) this.photoTitreForm.getItems().add(p.toString());
			this.pageForm.textProperty().addListener((obs, oldText, newText) -> {
				Photo p = viewPhotosForm.getSelectionModel().getSelectedItem();

				if(p!=null) {
					if(this.estUnEntier(this.pageForm.getText())) {
						Lk_Album_Photo lc = Main.getLienAlbumByPhoto(p); 
						lc.setPage(Integer.parseInt(newText));
						List<Lk_Album_Photo> lk2 = (List<Lk_Album_Photo>) Main.getListeLienImpressionPhoto();
						int index = lk2.indexOf(lc); 
						lk2.remove(lc);
						lk2.add(index,lc);
						Main.setListeLienImpressionPhoto(lk2);
					}
				}
			});

			this.ordreForm.textProperty().addListener((obs, oldText, newText) -> {
				Photo p = viewPhotosForm.getSelectionModel().getSelectedItem();
				if(p!=null) {
					if(this.estUnEntier(this.ordreForm.getText())) {
						Lk_Album_Photo lc = Main.getLienAlbumByPhoto(p); 
						lc.setOrdre(Integer.parseInt(newText));
						List<Lk_Album_Photo> lk2 = (List<Lk_Album_Photo>) Main.getListeLienImpressionPhoto();
						int index = lk2.indexOf(lc); 
						lk2.remove(lc);
						lk2.add(index,lc);
						Main.setListeLienImpressionPhoto(lk2);
					}
				}
			});

			this.descrForm.textProperty().addListener((obs, oldText, newText) -> {
				Photo p = viewPhotosForm.getSelectionModel().getSelectedItem();
				if(p!=null) {
					Lk_Album_Photo lc = Main.getLienAlbumByPhoto(p); 
					lc.setDescription(newText);
					List<Lk_Album_Photo> lk2 = (List<Lk_Album_Photo>) Main.getListeLienImpressionPhoto();
					int index = lk2.indexOf(lc); 
					lk2.remove(lc);
					lk2.add(index,lc);
					Main.setListeLienImpressionPhoto(lk2);
				}
			});

			this.viewPhotosForm.getItems().addAll(Main.getPhotoSelectionne());
			break;
		case "cadre.fxml":
			this.tailleForm.getItems().addAll("grand","petit");
			this.modeleForm.getItems().addAll("m1", "m2");
			this.viewPhotosForm.getItems().addAll(Main.getPhotoSelectionne());
			this.pageForm.textProperty().addListener((obs, oldText, newText) -> {
				Photo p = viewPhotosForm.getSelectionModel().getSelectedItem();

				if(p!=null) {
					if(this.estUnEntier(this.pageForm.getText())) {
						Lk_Cadre_Photo lc = Main.getLienCadreByPhoto(p); 
						lc.setPage(Integer.parseInt(newText));
						List<Lk_Cadre_Photo> lk2 = (List<Lk_Cadre_Photo>) Main.getListeLienImpressionPhoto();
						int index = lk2.indexOf(lc); 
						lk2.remove(lc);
						lk2.add(index,lc);
						Main.setListeLienImpressionPhoto(lk2);
					}
				}
			});
			break;
		case "calendrier.fxml":
			this.viewPhotosForm.getItems().addAll(Main.getPhotoSelectionne());
			this.modeleForm.getItems().addAll(calendrierModel.Bureau.name(), calendrierModel.Mural.name());
			List<Lk_Calendrier_Photo> lk = (List<Lk_Calendrier_Photo>) Main.getListeLienImpressionPhoto();
			this.pageForm.textProperty().addListener((obs, oldText, newText) -> {
				Photo p = viewPhotosForm.getSelectionModel().getSelectedItem();

				if(p!=null) {
					//on vérifie qu'on check pas la mise à zero de l'affichage
					if(this.estUnEntier(this.pageForm.getText())) {
						//on récupère le lk qui nous intérèsse
						Lk_Calendrier_Photo lc = Main.getLienCalendrierByPhoto(p); 
						//on update ses pages
						lc.setPage(Integer.parseInt(newText));
						//maj du lk
						List<Lk_Calendrier_Photo> lk2 = (List<Lk_Calendrier_Photo>) Main.getListeLienImpressionPhoto();
						//--on récupère son adresse 
						int index = lk2.indexOf(lc); 
						//--on remove l'ancien
						lk2.remove(lc);
						//--on recrée le nouveau à la même place qu'avant
						lk2.add(index,lc);
						//-- on save les modifs
						Main.setListeLienImpressionPhoto(lk2);
					}
				}
			});

			//idem avec ordreForm

			this.ordreForm.textProperty().addListener((obs, oldText, newText) -> {
				Photo p = viewPhotosForm.getSelectionModel().getSelectedItem();

				if(p!=null) {
					if(this.estUnEntier(this.ordreForm.getText())) {
						Lk_Calendrier_Photo lc = Main.getLienCalendrierByPhoto(p); 
						lc.setOrdre(Integer.parseInt(newText));
						List<Lk_Calendrier_Photo> lk2 = (List<Lk_Calendrier_Photo>) Main.getListeLienImpressionPhoto();
						int index = lk2.indexOf(lc); 
						lk2.remove(lc);
						lk2.add(index,lc);
						Main.setListeLienImpressionPhoto(lk2);
					}
				}
			});

			break;
		case "tirage.fxml":
			this.viewPhotosForm.getItems().addAll(Main.getPhotoSelectionne());
			this.formatForm.getItems().addAll("A4", "A3");
			this.qualiteForm.getItems().addAll("80g", "120g");
			break;
		case "connection.fxml":
			//this.loaderConnexion.setVisible(true);
			this.mailConn.setText("jules@jules.com");
			this.passwordConn.setText("0000");
			break;
		case "monCompte.fxml":
			mesCommandes.setRowFactory( tv -> {
			    TableRow<Commande> row = new TableRow<>();
			    row.setOnMouseClicked(event -> {
			        if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
			            Commande rowData = row.getItem();
			            Main.setCmdClick(rowData);
			            
			            new Thread(() -> {
			    			//On ajoute le loader
			    			Platform.runLater(()-> loaderConnexion.setVisible(true));

			    			Main.getCmdClick().loadArticle();

			    			Platform.runLater(()-> changementDeFenetre("descriptionCommande.fxml",event));
			    		}).start();
			            
			        }
			    });
			    return row ;
			});
			Client c = Main.getClientConnecte();
			this.mailInscr.setText(c.getMail_client());
			this.adresseInscr.setText(c.getAdresse());
			this.nomInscr.setText(c.getNom());
			this.prenomInscr.setText(c.getPrenom());
			List<CodePromo> cp = c.getCodePromoClient();
			for (CodePromo code : cp) this.codeReduc.getItems().add(code.getCode_promo());
			List<Commande> listCmd = c.getCommande();
			date.setCellValueFactory(new PropertyValueFactory<Commande, String>("date"));
			mode.setCellValueFactory(new PropertyValueFactory<Commande, String>("mode"));
			prix.setCellValueFactory(new PropertyValueFactory<Commande, String>("prix"));
			statut.setCellValueFactory(new PropertyValueFactory<Commande, String>("statutColumn"));
			ObservableList<Commande> data = FXCollections.observableArrayList(listCmd);
			mesCommandes.setItems(data);
			break;
		case "panier.fxml":
			List<ObjectPanier> l = Main.getCommandeEnCours().getObjectPanier();
			id_impr.setCellValueFactory(new PropertyValueFactory<ObjectPanier, String>("id_impression"));
			type.setCellValueFactory(new PropertyValueFactory<ObjectPanier, String>("type"));
			prixu.setCellValueFactory(new PropertyValueFactory<ObjectPanier, String>("PU"));
			reduc.setCellValueFactory(new PropertyValueFactory<ObjectPanier, String>("reduction"));
			quantite.setCellValueFactory(new PropertyValueFactory<ObjectPanier, String>("quantite"));
			ObservableList<ObjectPanier> data2 = FXCollections.observableArrayList(l);
			tabPanier.setItems(data2);
			break;
		case "newPhotos.fxml":
			List<FichierImage> listefichiers = Main.getClientConnecte().loadFichiers();
			this.ListPictures.getItems().addAll(listefichiers);
			List<FichierImage> listeFichierPublic = new TirageService().loadFichiersPublic();
			this.tabPicturesPublic.getItems().addAll(listeFichierPublic);
			break;
		case "addPhotos.fxml":
			myPhotos.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
			selectedPhotos.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
			this.myPhotos.getItems().addAll(Main.getClientConnecte().loadPhoto());
			break;
		case "gestionImages.fxml":
			listeFichierImage = Main.getClientConnecte().loadFichiers();
			libelleImg.setCellValueFactory(new PropertyValueFactory<FichierImage, String>("libelleColumn"));
			pathImg.setCellValueFactory(new PropertyValueFactory<FichierImage, String>("pathColumn"));
			infosImg.setCellValueFactory(new PropertyValueFactory<FichierImage, String>("infosColumn"));
			resImg.setCellValueFactory(new PropertyValueFactory<FichierImage, String>("resolutionColumn"));
			dateImg.setCellValueFactory(new PropertyValueFactory<FichierImage, String>("dateAjoutColumn"));
			partageImg.setCellValueFactory(new PropertyValueFactory<FichierImage, String>("partageColumn"));
			ObservableList<FichierImage> data3 = FXCollections.observableArrayList(listeFichierImage);
			tabPictures.setItems(data3);
			break;
		case "descriptionCommande.fxml":
			this.descrCommande.setWrapText(true);
			this.descrCommande.setText(Main.getCmdClick().toStringDetails());
			
			break;
		case "livraison.fxml":
			List<AdresseLivraison> listAdr = Main.getClientConnecte().getAllAdresse();
			listAdresseLivraison.getItems().addAll(listAdr);
			break;
		}

	}

	private boolean estUnEntier(String chaine) {
		try {
			Integer.parseInt(chaine);
		} catch (NumberFormatException e){
			return false;
		}

		return true;
	}


	/*----------------- DEBUT Connexion -----------------*/

	@FXML
	void gotoConn(ActionEvent event) {

		new Thread(() -> {
			//On ajoute le loader
			Platform.runLater(()-> loaderConnexion.setVisible(true));

			TirageService t = new TirageService();
			Client c = t.connect(mailConn.getText(), passwordConn.getText());
			if (c != null && c.getEst_connecte()==0) {
				c.setEst_connecte(1);
				UpdateService.update(c, Connection.TRANSACTION_READ_COMMITTED);
				Main.setClientConnecte(c);

				Commande tmp = new Commande();
				tmp.setMail_client(c);
				tmp.setStatut(statutCommande.EnCours);
				List<Commande> l = t.findByExample(tmp, Connection.TRANSACTION_READ_COMMITTED);
				if (l.size() > 0) {
					Main.setCommandeEnCours(l.get(0));
					Main.getCommandeEnCours().loadArticle();
				}
				else {
					Commande cm = new Commande();
					try {
						int id = (int) InsertService.insert(cm);
						cm.setId_commande(id);
					} catch (SQLException e) {
						e.printStackTrace();
					}
					cm.setMail_client(Main.getClientConnecte());
					Main.setCommandeEnCours(cm);					
				}
				Platform.runLater(()-> changementDeFenetre("connexion.fxml",event));
			}
			else {
				alertConnexion.setText("Echec de la connexion !");
				loaderConnexion.setVisible(false);
			}


		}).start();

	}

	/*----------------- FIN Connexion -----------------*/





	/*----------------- DEBUT Inscription -----------------*/

	@FXML
	void valideForm(ActionEvent event) {
		Client c = new Client();
		c.setMail_client(mailInscr.getText());
		c.setNom(nomInscr.getText());
		c.setPrenom(prenomInscr.getText());
		c.setAdresse(adresseInscr.getText());
		c.setPassword(passwordInscr.getText());
		c.setEtat(clientEtat.Actif);
		try {
			InsertService.insert(c);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Main.setClientConnecte(c);
		changementDeFenetre("connexion.fxml",event);
	}

	/*----------------- FIN Inscription -----------------*/



	/*----------------- DEBUT MonCompte -----------------*/

	@FXML
	void gotoMonCompte(ActionEvent event) {
		new Thread(() -> {
			//On ajoute le loader
			Platform.runLater(()-> loaderConnexion.setVisible(true));

			Main.getClientConnecte().loadCodePromo();
			Main.getClientConnecte().loadCommande();

			Platform.runLater(()-> changementDeFenetre("monCompte.fxml", event));
		}).start();

	}

	/*----------------- FIN MonCompte -----------------*/



	/*----------------- DEBUT Panier -----------------*/

	@FXML
	void gotoPanier(ActionEvent event) {
		new Thread(() -> {
			//On ajoute le loader
			Platform.runLater(()-> loaderConnexion.setVisible(true));

			Main.getCommandeEnCours().loadObjectPanier();

			Platform.runLater(()-> changementDeFenetre("panier.fxml",event));
		}).start();

	}

	/*----------------- FIN Panier -----------------*/
	
	@FXML
	void payerPanier(ActionEvent event) {
		changementDeFenetre("payer.fxml",event);
	}
	
	@FXML
	void payerCommande(ActionEvent event) {
		//changementDeFenetre("payer.fxml",event);
		boolean ok = true;
		Commande c = Main.getCommandeEnCours();
		Connection connection = ConnectionJDBC.getConnection();
		try {
			connection.setAutoCommit(false);
			connection.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
			c = Contrainte.verifEtMajEtatCommande(Main.getCommandeEnCours(), connection);
			connection.commit();
			connection.close();
			
			connection = ConnectionJDBC.getConnection();
			connection.setAutoCommit(false);
			connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
			c = Contrainte.calculPrixCommande(c, connection);
			connection.commit();
			connection.close();
			
			connection = ConnectionJDBC.getConnection();
			connection.setAutoCommit(false);
			connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
			
			c.setPrix_tt_remise(c.getPrix_tt());
			c.setDate_commande(new Timestamp(System.currentTimeMillis()));
		} catch (SQLException e) {
			e.printStackTrace();
			ok = false;
			System.out.println("Non serializable veuillez reessayer");
		}	
		if (ok) {
			if (!this.codePromoForm.getText().equals("")) { //On a un code promo rentré
				TirageService t = new TirageService();
				CodePromo cp = null;
				try {
					cp = (CodePromo) t.findById(CodePromo.class, codePromoForm.getText(), connection);

					if (cp != null) { //On a un codepromo valide
						try {
							c.setPrix_tt_remise(Contrainte.calculPrixPromo(c.getPrix_tt(), cp.getRemise()));
							connection = ConnectionJDBC.getConnection();
							connection.setAutoCommit(false);
							connection.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
							//UpdateService.update(cp, connection);
							Lk_CodePromo_Commande lk = new Lk_CodePromo_Commande(cp, Main.getCommandeEnCours());
							InsertService.insertMyLk(lk, connection);
							connection.commit();
							connection.close();
						} catch (SQLException e1) {
							//Erreur trigger
							System.out.println(e1.getMessage().split("\n")[0].split(": ")[1]);
							ok = false;
						}
					}
					else {
						System.out.println("Code promo invalide");
						ok = false;
					}
				} catch (Exception e2) {
					e2.printStackTrace();
				}
			}
		}

		if (ok) {
			try {
				boolean eligible = Contrainte.estEligibleCodePromo(c);
				if (eligible) {
					String code = Contrainte.genererCodePromo();
					CodePromo newcp = new CodePromo();		
					newcp.setCode_promo(code);
					newcp.setCommande_creatrice_du_codePromo(c);
					newcp.setEst_public(0);
					newcp.setNombre_utilisation(0);
					newcp.setRemise(5);
					newcp.setDate_fin_code(new Timestamp(System.currentTimeMillis()));
					System.out.println("Est eligible code promo : " + newcp);
					connection = ConnectionJDBC.getConnection();
					connection.setAutoCommit(false);
					connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
					InsertService.insert(newcp, connection);
					connection.commit();
					connection.close();
				}
				connection = ConnectionJDBC.getConnection();
				connection.setAutoCommit(false);
				connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
				UpdateService.update(c, connection);
				
				
				connection.commit();
				connection.close();
				gotoLivraison(event);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	
	/*----------------- DEBUT Livraison -----------------*/

	@FXML
	void gotoLivraison(ActionEvent event) {

		new Thread(() -> {
			//On ajoute le loader
			Platform.runLater(()-> loaderConnexion.setVisible(true));

			Main.getClientConnecte().loadAllAdresse();

			Platform.runLater(()-> changementDeFenetre("livraison.fxml",event));
		}).start();

	}
	
	
	@FXML
	void validerLivraison(ActionEvent event) {
		Commande c =  Main.getCommandeEnCours();
		if (!codePRForm.getText().equals("") && listAdresseLivraison.getSelectionModel().getSelectedItem()!=null) 
			System.out.println("Veuillez choisir soit code point relais soit adresse livraison");
		else if (codePRForm.getText().equals("") && listAdresseLivraison.getSelectionModel().getSelectedItem()==null)
			System.out.println("Veuillez choisir entre code point relais soit adresse livraison");
		else {
			if (!codePRForm.getText().equals("")) {
				c.setCode_point_relais(codePRForm.getText());
				c.setMode_livraison(modeLivraison.PointRelais);
			}
			else {
				c.setAdresse_livraison(listAdresseLivraison.getSelectionModel().getSelectedItem());
			}
			UpdateService.update(c, Connection.TRANSACTION_READ_COMMITTED);
			Commande cm = new Commande();
			try {
				int id = (int) InsertService.insert(cm);
				cm.setId_commande(id);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			cm.setMail_client(Main.getClientConnecte());
			Main.setCommandeEnCours(cm);
			changementDeFenetre("connexion.fxml", event);
		}
		
	}

	/*----------------- FIN Livraison -----------------*/
}

