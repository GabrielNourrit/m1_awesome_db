package client;

import java.io.FileWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import models.Agenda;
import models.Album;
import models.Cadre;
import models.Calendrier;
import models.Client;
import models.CodePromo;
import models.Commande;
import models.FichierImage;
import models.Impression;
import models.Tirage;
import services.ConnectionJDBC;
import services.InsertService;
import services.TirageService;
import services.UpdateService;
import type.statutCommande;
import util.Contrainte;
import util.LectureClavier;

public class InterfaceGestion {

	public static void main(String[] args) throws SQLException {
		int choix = 0;
		int choixGestion = 0;
		int choixStock = 0;
		int choixCsv = 0;

		while (choix != -1) {

			System.out.println("**********************************************");
			System.out.println("***** BIENVENUE DANS L'INTERFACE GESTION *****");
			System.out.println("**********************************************\n");

			System.out.println("QUE VOULEZ-VOUS FAIRE ?\n");
			System.out.println("(1) Generer code promo commercial");
			System.out.println("(2) Supprimer un client");
			System.out.println("(3) Mise à jour du stock");
			System.out.println("(4) Envoyer des commandes");
			System.out.println("(5) Voir les statistiques");
			System.out.println("(6) Supprimer un fichier pour problème juridique");
			System.out.println("(-1) Quitter");

			choix = LectureClavier.lireEntier("Choix : ");

			switch(choix) { 

			/*----------------- DEBUT Génération Code Promo -----------------*/

			case 1 :
				TirageService tpromo = new TirageService();
				int remise = 0;
				int Setpublic = -1;
				String dateStr;
				String month31 = "1 3 5 7 8 10 12";
				String month30 = "4 6 9 11";

				while (remise <= 0) {
					remise = LectureClavier.lireEntier("Pourcentage  de remise : ");					
				}
				
				while (Setpublic < 0 || Setpublic > 1) {
					Setpublic = LectureClavier.lireEntier("Code promo public ? (1 : Oui / 0:Non) : ");
				}

				System.out.println("Date de fin de code (YYYY-MM-DD) : ");
				dateStr = LectureClavier.lireChaine();
				String[] tabR = dateStr.split("-");

				while ( Integer.parseInt(tabR[0]) < 2018 && (
						(month31.contains(tabR[1]) && Integer.parseInt(tabR[2])  < 1 && Integer.parseInt(tabR[2]) > 31) ||
						(month30.contains(tabR[1]) &&  Integer.parseInt(tabR[2]) < 1 && Integer.parseInt(tabR[2]) > 30) ||
						(Integer.parseInt(tabR[0]) % 4 == 0 && (Integer.parseInt(tabR[0]) % 100 != 0 && Integer.parseInt(tabR[0]) % 400 == 0) &&
						Integer.parseInt(tabR[1])    == 2   && Integer.parseInt(tabR[2])     < 1     && Integer.parseInt(tabR[2]) > 29) ||
						(Integer.parseInt(tabR[0]) % 4 != 0 || Integer.parseInt(tabR[0]) % 100 == 0  &&
						Integer.parseInt(tabR[1])    == 2   && Integer.parseInt(tabR[2])     < 1     && Integer.parseInt(tabR[2]) > 28 ) ) ) {

					System.out.println("Date Incorrecte ");
					System.out.println("Date de fin de code (YYYY-MM-DD) : ");
					dateStr = LectureClavier.lireChaine();
					tabR = dateStr.split("-");
				}

				dateStr += " 08:00:00";
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date datetmp = null;

				try {
					datetmp = format.parse(dateStr);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				/*-*/

				java.sql.Date sDate = new java.sql.Date(datetmp.getTime());
				
				CodePromo cp = new CodePromo();
				cp.setRemise(remise);
				cp.setDate_fin_code(new Timestamp(sDate.getTime()));
				cp.setEst_public(Setpublic);
				cp.setNombre_utilisation(0);
				cp.setCode_promo(Contrainte.genererCodePromo());
				
				InsertService.insert(cp, Connection.TRANSACTION_READ_COMMITTED);

				break;


				/*----------------- FIN  Génération Code Promo  -----------------*/



				/*----------------- DEBUT Suppression Client -----------------*/

			case 2 :

				TirageService tCli = new TirageService();
				List<Client> resultClient = tCli.getAll(Client.class);
				
				if(resultClient.size() > 0) {
					//Affichage de tous les clients
					for(Client c : resultClient) {
						System.out.println(c);
					}
					System.out.println("Quel client voulez-vous supprimer (veuillez saisir le mail du client) ? ");
					String mail_client = LectureClavier.lireChaine();
					//Suppression client
					Client c = new Client();
					c.setMail_client(mail_client);
					tCli.deleteObject(c, Connection.TRANSACTION_READ_COMMITTED);
				} else {
					System.out.println("Aucun client trouvé");
				}
				break;

				/*----------------- FIN Suppression Client -----------------*/



				/*----------------- DEBUT Mise à Jour des Stocks -----------------*/

			case 3 :


				while (choixStock != -1) {

					System.out.println("*************************************************");
					System.out.println("Quel stock voulez mettre à jour ?");
					System.out.println("(1) Inventaire Agenda");
					System.out.println("(2) Inventaire Calendrier");
					System.out.println("(3) Inventaire Papier");
					System.out.println("(4) Inventaire Cadre");
					choixStock = LectureClavier.lireEntier("Choix de l'inventaire : ");
					//Gestion des inventaires

					switch (choixStock) {

					// Inventaire Agenda
					case 1 :

						while (choixGestion != -1) {
							System.out.println("*************************************************");
							System.out.println("Comment manipuler le stock Agenda ? ");
							System.out.println("(1) Incrémenter le stock");
							System.out.println("(2) Décrémenter le stock");
							choixGestion = LectureClavier.lireEntier("Manipulation choisie : ");							

							String modele = null;
							String type = null;
							int quantite = 0;

							switch (choixGestion) {

							case 1 :
								System.out.println("Quel modèle incrémenter ?");
								modele = LectureClavier.lireChaine();
								System.out.println("Quel type incrémenter ?");
								type = LectureClavier.lireChaine();
								quantite = LectureClavier.lireEntier("Quantité à ajouter ? : ");	

								Contrainte.incrementerStockAgenda(modele, type, quantite);
								break;

							case 2 :
								System.out.println("Quel modèle décrémenter ?");
								modele = LectureClavier.lireChaine();
								System.out.println("Quel type décrémenter ?");
								type = LectureClavier.lireChaine();
								quantite = LectureClavier.lireEntier("Quantité à retirer ? : ");	
								Connection connection = ConnectionJDBC.getConnection();
								try {
									connection.setAutoCommit(false);
									connection.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
								} catch (SQLException e) {
									e.printStackTrace();
								}
								Contrainte.decrementerStockAgenda(modele, type, quantite, connection);
								connection.commit();
								connection.close();
								break;

							case -1 :
								// Sortie de gestion Calendrier
								break;
								
							default :
								System.out.println("Veuillez saisir une manipulation valide !");
								break;

							}
						}

						break;

						// Inventaire Calendrier
					case 2 :
						
						while (choixGestion != -1) {
							System.out.println("*************************************************");
							System.out.println("Comment manipuler le stock Calendrier ? ");
							System.out.println("(1) Incrémenter le stock");
							System.out.println("(2) Décrémenter le stock");
							choixGestion = LectureClavier.lireEntier("Manipulation choisie : ");							

							String modele = null;
							int quantite = 0;

							switch (choixGestion) {

							case 1 :
								System.out.println("Quel modèle incrémenter ?");
								modele = LectureClavier.lireChaine();
								quantite = LectureClavier.lireEntier("Quantité à ajouter ? : ");	

								Contrainte.incrementerStockCalendrier(modele, quantite);
								break;

							case 2 :
								System.out.println("Quel modèle décrémenter ?");
								modele = LectureClavier.lireChaine();
								quantite = LectureClavier.lireEntier("Quantité à retirer ? : ");	
								Connection connection = ConnectionJDBC.getConnection();
								try {
									connection.setAutoCommit(false);
									connection.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
								} catch (SQLException e) {
									e.printStackTrace();
								}
								Contrainte.decrementerStockCalendrier(modele, quantite, connection);
								connection.commit();
								connection.close();
								break;

							case -1 :
								// Sortie de gestion Calendrier
								break;
								
							default :
								System.out.println("Veuillez saisir une manipulation valide !");
								break;

							}
						}
						
						break;

						// Inventaire Papier
					case 3 :
						
						while (choixGestion != -1) {
							System.out.println("*************************************************");
							System.out.println("Comment manipuler le stock Papier ? ");
							System.out.println("(1) Incrémenter le stock");
							System.out.println("(2) Décrémenter le stock");
							choixGestion = LectureClavier.lireEntier("Manipulation choisie : ");							

							String formatPapier = null;
							String qualite = null;
							int quantite = 0;

							switch (choixGestion) {

							case 1 :
								System.out.println("Quel format de Papier incrémenter ?");
								formatPapier = LectureClavier.lireChaine();
								System.out.println("Quel qualite incrémenter ?");
								qualite = LectureClavier.lireChaine();
								quantite = LectureClavier.lireEntier("Quantité à ajouter ? : ");	

								Contrainte.incrementerStockPapier(qualite, formatPapier, quantite);
								break;

							case 2 :
								System.out.println("Quel format de Papier décrémenter ?");
								formatPapier = LectureClavier.lireChaine();
								System.out.println("Quel qualite décrémenter ?");
								qualite = LectureClavier.lireChaine();
								quantite = LectureClavier.lireEntier("Quantité à retirer ? : ");	
								Connection connection = ConnectionJDBC.getConnection();
								try {
									connection.setAutoCommit(false);
									connection.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
								} catch (SQLException e) {
									e.printStackTrace();
								}
								Contrainte.decrementerStockPapier(qualite, formatPapier, quantite, connection);
								connection.commit();
								connection.close();
								break;

							case -1 :
								// Sortie de gestion Calendrier
								break;
								
							default :
								System.out.println("Veuillez saisir une manipulation valide !");
								break;

							}
						}
						
						break;

						// Inventaire Cadre
					case 4 :
						
						while (choixGestion != -1) {
							System.out.println("*************************************************");
							System.out.println("Comment manipuler le stock Agenda ? ");
							System.out.println("(1) Incrémenter le stock");
							System.out.println("(2) Décrémenter le stock");
							choixGestion = LectureClavier.lireEntier("Manipulation choisie : ");							

							String modele = null;
							String taille = null;
							int quantite = 0;

							switch (choixGestion) {

							case 1 :
								System.out.println("Quel modèle incrémenter ?");
								modele = LectureClavier.lireChaine();
								System.out.println("Quel taille incrémenter ?");
								taille = LectureClavier.lireChaine();
								quantite = LectureClavier.lireEntier("Quantité à ajouter ? : ");	

								Contrainte.incrementerStockCadre(modele, taille, quantite);
								break;

							case 2 :
								System.out.println("Quel modèle décrémenter ?");
								modele = LectureClavier.lireChaine();
								System.out.println("Quel taille décrémenter ?");
								taille = LectureClavier.lireChaine();
								quantite = LectureClavier.lireEntier("Quantité à retirer ? : ");	
								Connection connection = ConnectionJDBC.getConnection();
								try {
									connection.setAutoCommit(false);
									connection.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
								} catch (SQLException e) {
									e.printStackTrace();
								}
								Contrainte.decrementerStockCadre(modele, taille, quantite, connection);
								connection.commit();
								connection.close();
								break;

							default :
								System.out.println("Veuillez saisir une manipulation valide !");
								break;

							}
						}
						
						break;

						// Sortie
					case -1 :
						//Là, on s'en va, vers le menu principal
						break;

					default :
						System.out.println("Veuillez saisir une option valide !");
						break;

					}


				}

				break;

				/*----------------- FIN Mise à Jour des Stocks -----------------*/



				/*----------------- DEBUT Envoie des commandes -----------------*/

			case 4 :

				TirageService tCmd = new TirageService();

				Commande cmd = new Commande();
				cmd.setStatut(statutCommande.Paye);
				System.out.println(cmd);
				List<Commande> resultCommand = tCmd.findByExample(cmd);

				if(resultCommand.size() > 0) {
					//Affichage de toutes les commandes non envoyées
					for(Commande c : resultCommand) {
						System.out.println(c);
					}

					System.out.println("Quelle commande voulez-vous envoyer (veuillez saisir son id) ? ");

					int id_commande = 0;
					Commande c = new Commande();

					while(id_commande >= 0) {

						id_commande = LectureClavier.lireEntier("id_commande (-1 pour sortir) : ");
						c.setId_commande(id_commande);
						c.setStatut(statutCommande.Envoye);
						//Envoie de la commande
						UpdateService.update(c, Connection.TRANSACTION_READ_COMMITTED);
					}
				} else {
					System.out.println("Aucune commande trouvée");
				}
				
				break;

				/*----------------- FIN Envoie des commandes -----------------*/



				/*----------------- DEBUT Statistiques -----------------*/

			case 5 :
				TirageService t = new TirageService();
				
				double nbImpr = t.getNbObjectForStats(Impression.class);
				double nbCadre = t.getNbObjectForStats(Cadre.class);
				double nbTirage = t.getNbObjectForStats(Tirage.class);
				double nbCalen = t.getNbObjectForStats(Calendrier.class);
				double nbAlbum = t.getNbObjectForStats(Album.class);
				double nbAgenda = t.getNbObjectForStats(Agenda.class);
				double codePromo = t.getNbObjectForStats(CodePromo.class);
				double codePromoUsed = t.getNbObjectForStats(CodePromo.class, "nombre_utilisation > 0");
				double nbCmd = t.getNbObjectForStats(Commande.class);
				double nbCmdEncours = t.getNbObjectForStats(Commande.class, "statut like '"+statutCommande.EnCours+"'");
				double nbCmdPaye = t.getNbObjectForStats(Commande.class, "statut like '"+statutCommande.Paye+"'");
				double nbCmdEnvoye = t.getNbObjectForStats(Commande.class, "statut like '"+statutCommande.Envoye+"'");
				double nbCmdAF = t.getNbObjectForStats(Commande.class, "statut like '"+statutCommande.EnAttenteFournisseur+"'");
				double nbCmdAnn = t.getNbObjectForStats(Commande.class, "statut like '"+statutCommande.Annule+"'");
				double nbCmdAeR = t.getNbObjectForStats(Commande.class, "statut like '"+statutCommande.AnnuleEtRembourse+"'");
				
				String csvTmp = "nbImpr;"+nbImpr+"\n";
				csvTmp += "nbCadre;"+nbCadre+"\n";
				csvTmp += "nbTirage;"+nbTirage+"\n";
				csvTmp += "nbCalendrier;"+nbCalen+"\n";
				csvTmp += "nbAlbum;"+nbAlbum+"\n";
				csvTmp += "nbAgenda;"+nbAgenda+"\n";
				csvTmp += "nbCodePromo;"+codePromo+"\n";
				csvTmp += "nbCodePromoUtilisé;"+codePromoUsed+"\n";
				csvTmp += "nbCmd;"+nbCmd+"\n";
				csvTmp += "nbCmdEncours;"+nbCmdEncours+"\n";
				csvTmp += "nbCmdPaye;"+nbCmdPaye+"\n";
				csvTmp += "nbCmdEnvoye;"+nbCmdEnvoye+"\n";
				csvTmp += "nbCmdAF;"+nbCmdAF+"\n";
				
				System.out.println("****************************************");
				System.out.println("Statistiques :");
				System.out.println(" Proportion d'impressions par type :");
				// Nombre d'Impression total
				System.out.println(" Nombre d'Impressions : " + nbImpr );
				// Cadre / Impressions
				double cadrImpr = (nbCadre/nbImpr)*100;
				System.out.println(" Cadre      :   "+ cadrImpr +"%");
				// Tirage / Impressions
				double tirageImpr = (nbTirage/nbImpr)*100;
				System.out.println(" Tirage     :   "+ tirageImpr +"%");
				// Calendrier / Impressions
				double calenImpr = (nbCalen/nbImpr)*100;
				System.out.println(" Calendrier :   "+ calenImpr +"%");
				// Album / Impressions
				double albumImpr = (nbAlbum/nbImpr)*100;
				System.out.println(" Album      :   "+ albumImpr +"%");
				// Agenda / Impressions
				double agendaImpr = (nbAgenda/nbImpr)*100;
				System.out.println(" Agenda     :   "+ agendaImpr +"%");
				
				System.out.println("Impressions moyennes par commandes :  "+ (nbImpr/nbCmd));
				
				System.out.println(" Proportion de commandes par statut :");
				// Nombre total de commande
				System.out.println(" Nombre de Commandes : " + nbCmd);
				System.out.println(" Attente Fournisseur :   " + (nbCmdAF/nbCmd) + "%");
				System.out.println(" En Cours 			:   " + (nbCmdEncours/nbCmd) + "%");
				System.out.println(" Envoye 			:   " + (nbCmdEnvoye/nbCmd) + "%");
				System.out.println(" Paye 				:   " + (nbCmdPaye/nbCmd) + "%");
				System.out.println(" Annule 			:  " + (nbCmdAnn/nbCmd) + "%");
				System.out.println(" Annulé & Rembourse :   " + (nbCmdAeR/nbCmd) + "%");

				System.out.println(" Proportion de commandes par statut :");
				// Nombre total de commande
				System.out.println(" Nombre de Code promo généré : " + codePromo);
				System.out.println(" Nombre de Code promo utilisé :   " + codePromoUsed);
				System.out.println(" Rapport entre les deux : " + (codePromoUsed/codePromo) + "%\n\n");
				

				System.out.println("Voulez-vous exporter un CSV avec ces valeurs ?");
				System.out.println("(1) Non");
				System.out.println("(2) Oui");
				choixCsv = LectureClavier.lireEntier("Export CSV ? : ");							

				while (choixCsv != 1 && choixCsv != 2) {

					System.out.println("Valeur incorrecte !\n");
					System.out.println("Voulez-vous exporter un CSV avec ces valeurs ?");
					System.out.println("(1) Non");
					System.out.println("(2) Oui");
					choixCsv = LectureClavier.lireEntier("Export CSV ? : ");							

				}
				
				if(choixCsv == 2) {
					genererCSV(csvTmp);
				}
				
				System.out.println("");

				System.out.println("Fin Statistiques");
				System.out.println("****************************************\n\n");
				break;

				/*----------------- FIN Statistiques -----------------*/


			case 6:
				TirageService ts = new TirageService();
				FichierImage tmp = new FichierImage();
				tmp.setEtat("actif");
				List<FichierImage> resultFichier = ts.findByExample(tmp, Connection.TRANSACTION_READ_COMMITTED);
				
				if(resultFichier.size() > 0) {
					//Affichage de tous les clients
					for(FichierImage f : resultFichier) {
						System.out.println(f.myToString());
					}
					System.out.println("Quel fichier voulez-vous supprimer (veuillez l'id du fichier) ? ");
					int idfic = LectureClavier.lireEntier("");
					//Suppression client
					FichierImage f = new FichierImage();
					f.setId_fic(idfic);
					ts.deleteObject(f, Connection.TRANSACTION_READ_COMMITTED);
				} else {
					System.out.println("Aucun fichier trouvé");
				}
				break;
				
				
				
				/*----------------- DEBUT Sortie -----------------*/

			case -1 :
				// La, c'est la sortie
				break;

				/*----------------- FIN Sortie -----------------*/



				/*----------------- DEBUT DEFAULT -----------------*/

			default :
				System.out.println("Veuillez saisir un chiffre valide !");
				break;

				/*----------------- FIN DEFAULT -----------------*/

			}
		}
	}
	
	/**
	 * Methode qui genere un csv
	 * @param csv
	 */
	public static void genererCSV(String csv) {
		
		try {
			String t = Long.toString(System.currentTimeMillis());
			String fileName = "Stats-"+t+".csv";
			FileWriter fileWriter = new FileWriter(fileName);
			fileWriter.append(csv);
			fileWriter.close();
			System.out.println("Votre fichier de statistique "+fileName+" a été généré !");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}