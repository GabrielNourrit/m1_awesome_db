package client;
	
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import models.Calendrier;
import models.Client;
import models.Commande;
import models.Lk_Agenda_Photo;
import models.Lk_Album_Photo;
import models.Lk_Cadre_Photo;
import models.Lk_Calendrier_Photo;
import models.Lk_Tirage_Photo;
import models.Photo;
import services.TirageService;
import services.UpdateService;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;


public class Main extends Application {
	 private static String addPhotoNavOrigin;
	 private static Client clientConnecte;
	 private static Commande commandeEnCours;
	 
	 
	 // Selection de photo
	 private static List<Photo> photoSelectionne = new ArrayList<Photo>();
	 
	 private static Calendrier calendrierCreer = new Calendrier();
	 private static List<?> listeLienImpressionPhoto = new ArrayList<>();
	 private static Commande cmdClick = new Commande();
	
	@Override
	public void start(Stage primaryStage) {
		try {
			Parent root = FXMLLoader.load(getClass().getResource("accueil.fxml")); 
		    Scene scene = new Scene(root); 
			primaryStage.setScene(scene);
			primaryStage.show();
			primaryStage.setOnCloseRequest(e -> {
				if (clientConnecte != null) {
					clientConnecte.setEst_connecte(0);
					UpdateService.update(clientConnecte, Connection.TRANSACTION_READ_COMMITTED);	
				}
				Platform.exit();
			});
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	 public static String getAddPhotoNavOrigin() {
	        return addPhotoNavOrigin;
	    }

	    public static void setAddPhotoNavOrigin(String addPhotosNavOrigin) {
	        Main.addPhotoNavOrigin = addPhotosNavOrigin;
	    }
	
	public static void main(String[] args) {
		launch(args);
	}

	public static Client getClientConnecte() {
		return clientConnecte;
	}

	public static void setClientConnecte(Client clientConnecte) {
		Main.clientConnecte = clientConnecte;
	}

	public static Commande getCommandeEnCours() {
		return commandeEnCours;
	}

	public static void setCommandeEnCours(Commande commandeEnCours) {
		Main.commandeEnCours = commandeEnCours;
	}

	public static List<Photo> getPhotoSelectionne() {
		return photoSelectionne;
	}

	public static void setPhotoSelectionne(List<Photo> photoSelectionne) {
		Main.photoSelectionne = photoSelectionne;
	}


	public static Lk_Agenda_Photo getLienAgendaByPhoto(Photo p) {
		for(Lk_Agenda_Photo lk : (ArrayList<Lk_Agenda_Photo>)listeLienImpressionPhoto) {
			if(lk.getId_photo().getId_photo()==p.getId_photo())
				return lk;
		}
		return null;
	}
	public static Lk_Album_Photo getLienAlbumByPhoto(Photo p) {
		for(Lk_Album_Photo lk : (ArrayList<Lk_Album_Photo>)listeLienImpressionPhoto) {
			if(lk.getId_photo().getId_photo()==p.getId_photo())
				return lk;
		}
		return null;
	}
	public static Lk_Cadre_Photo getLienCadreByPhoto(Photo p) {
		for(Lk_Cadre_Photo lk : (ArrayList<Lk_Cadre_Photo>)listeLienImpressionPhoto) {
			if(lk.getId_photo().getId_photo()==p.getId_photo())
				return lk;
		}
		return null;
	}
	public static Lk_Calendrier_Photo getLienCalendrierByPhoto(Photo p) {
		for(Lk_Calendrier_Photo lk : (ArrayList<Lk_Calendrier_Photo>)listeLienImpressionPhoto) {
			if(lk.getId_photo().getId_photo()==p.getId_photo())
				return lk;
		}
		return null;
	}
	public static Lk_Tirage_Photo getLienTirageByPhoto(Photo p) {
		for(Lk_Tirage_Photo lk : (ArrayList<Lk_Tirage_Photo>)listeLienImpressionPhoto) {
			if(lk.getId_photo().getId_photo()==p.getId_photo())
				return lk;
		}
		return null;
	}
	
	public static void setListeLienImpressionPhoto(List<?> listeLienCalendrierPhoto) {
		listeLienImpressionPhoto = listeLienCalendrierPhoto;
	}
	public static List<?> getListeLienImpressionPhoto() {
		return listeLienImpressionPhoto;
	}

	public static Calendrier getCalendrierCreer() {
		return calendrierCreer;
	}

	public static void setCalendrierCreer(Calendrier calendrierCreer) {
		Main.calendrierCreer = calendrierCreer;
	}

	public static Commande getCmdClick() {
		return cmdClick;
	}

	public static void setCmdClick(Commande cmdClick) {
		Main.cmdClick = cmdClick;
	}


}
