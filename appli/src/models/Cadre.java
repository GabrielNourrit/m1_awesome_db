package models;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import services.IdentityPrimary;
import services.IgnoreQueryProperty;
import services.TirageService;

public class Cadre extends Impression {
	
	@IdentityPrimary("")
	private Number id_cadre;
	private String taille;
	private String modele;
	private String mise_en_page;
	@IgnoreQueryProperty
	private LK lk_cadre_photo;
	/**
	 * @return the id_cadre
	 */
	public Number getId_cadre() {
		return id_cadre;
	}
	
	/**
	 * @param id_cadre the id_cadre to set
	 */
	public void setId_cadre(Number id_cadre) {
		this.id_cadre = id_cadre;
	}
	/**
	 * @return the taille
	 */
	public String getTaille() {
		return taille;
	}
	/**
	 * @param taille the taille to set
	 */
	public void setTaille(String taille) {
		this.taille = taille;
	}
	/**
	 * @return the modele
	 */
	public String getModele() {
		return modele;
	}
	/**
	 * @param modele the modele to set
	 */
	public void setModele(String modele) {
		this.modele = modele;
	}
	public String getMise_en_page() {
		return mise_en_page;
	}
	public void setMise_en_page(String mise_en_page) {
		this.mise_en_page = mise_en_page;
	}

	public List<Photo> getAllPhoto() {
		List<Lk_Cadre_Photo> l = lk_cadre_photo.get_Lk_Cadre_Photo_byCadre(this);
		List<Photo> ret = new ArrayList<Photo>();
		for (Lk_Cadre_Photo lk : l) ret.add(lk.getId_photo());
		return ret;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Cadre [id_cadre=" + id_cadre + ", taille=" + taille + ", modele=" + modele
				+ ", mise_en_page=" + mise_en_page + "]";
	}
	
	public String customToString() {
		return "Cadre [id_cadre=" + id_cadre + ", taille=" + taille + ", modele=" + modele
				+ ", mise_en_page=" + mise_en_page + "]";
	}
	
	public boolean equals(Object o) {
		if (id_cadre == ((Cadre) o).getId_cadre()) return true;
		return false;
	}
	
	public String toStringDetails() {
		TirageService t = new TirageService();
		List<Photo> lp = new ArrayList<Photo>();
		Lk_Cadre_Photo cp = new Lk_Cadre_Photo();
		List<Lk_Cadre_Photo> lks = t.findLKById(this.getClass(), this.id_cadre, Lk_Cadre_Photo.class, Connection.TRANSACTION_READ_COMMITTED);
		for (Lk_Cadre_Photo lk : lks) lp.add(lk.getId_photo());
		return toString() + "\n" + lp;
	}
}
