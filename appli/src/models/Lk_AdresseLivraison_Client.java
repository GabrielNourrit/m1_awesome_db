package models;

import java.sql.SQLException;

import services.TirageService;

public class Lk_AdresseLivraison_Client {

	private AdresseLivraison id_adresse_livraison;
	private Client mail_client;
	
	public Lk_AdresseLivraison_Client(AdresseLivraison a, Client c) {
		id_adresse_livraison = a;
		mail_client = c;
	}
	public Lk_AdresseLivraison_Client() {
	}
	
	public AdresseLivraison getId_adresse_livraison() {
		return id_adresse_livraison;
	}
	public void setId_adresse_livraison(Number id) throws SQLException {
		TirageService t = new TirageService();
		this.id_adresse_livraison = (AdresseLivraison) t.findById(AdresseLivraison.class, id);
		//this.adresse_livraison = adresse_livraison;
	}
	
	
	public Client getMail_client() {
		return mail_client;
	}
	public void setMail_client(String id) throws SQLException {
		TirageService t = new TirageService();
		this.mail_client = (Client) t.findById(Client.class, id);
		//this.client = client;
	}
	
	public void setMail_client(Client id) {
		this.mail_client = id;
		//this.client = client;
	}
	
	public boolean equals(Object o) {
		if (o instanceof Lk_AdresseLivraison_Client) {
			Lk_AdresseLivraison_Client lk = (Lk_AdresseLivraison_Client) o;
			if (id_adresse_livraison.equals(lk.getId_adresse_livraison()) && mail_client.equals(lk.getMail_client())) return true;
		}
		return false;
	}
}
