package models;

import java.sql.Timestamp;
import java.util.List;

import javafx.beans.property.SimpleStringProperty;
import services.ActiveState;
import services.IdentityPrimary;
import services.IgnoreQueryProperty;
import services.TirageService;
import type.etatFichierImage;

public class FichierImage {
	
	@IdentityPrimary("fichierImage_id")
	private Number id_fic;
	private String libelle;
	private String chemin;
	private String infos_vue;
	private String resolution;
	@ActiveState
	private etatFichierImage etat;
	private Number est_actif;
	private Timestamp date_ajout;
	private Number est_partage;
	private Client mail_client;
	// Liste des fichiers photos créé à partir de ce fichier image
	@IgnoreQueryProperty
	private List<Photo> photos_associe;
	
	
	@IgnoreQueryProperty
	private SimpleStringProperty libelleColumn;
	@IgnoreQueryProperty
    private SimpleStringProperty pathColumn;
	@IgnoreQueryProperty
    private SimpleStringProperty infosColumn;
	@IgnoreQueryProperty
    private SimpleStringProperty resolutionColumn;
	@IgnoreQueryProperty
    private SimpleStringProperty dateAjoutColumn;
	@IgnoreQueryProperty
    private SimpleStringProperty partageColumn;
	
	
	/**
	 * @return the id_fic
	 */
	public Number getId_fic() {
		return id_fic;
	}
	/**
	 * @param id_fic the id_fic to set
	 */
	public void setId_fic(Number id_fic) {
		this.id_fic = id_fic;
	}
	
	public List<Photo> getPhotoAssocie() {
		TirageService t = new TirageService();
		// On recupere la liste des articles
		Photo example = new Photo();
		example.setId_fic(id_fic);
		return this.photos_associe = t.findByExample(example);
	}
	/**
	 * @return the libelle
	 */
	public String getLibelle() {
		return libelle;
	}
	/**
	 * @param libelle the libelle to set
	 */
	public void setLibelle(String libelle) {
		this.libelle = libelle;
		this.libelleColumn = new SimpleStringProperty(libelle);
	}
	/**
	 * @return the chemin
	 */
	public String getChemin() {
		return chemin;
	}
	/**
	 * @param chemin the chemin to set
	 */
	public void setChemin(String chemin) {
		this.chemin = chemin;
		this.pathColumn = new SimpleStringProperty(chemin);
	}
	/**
	 * @return the infos_vue
	 */
	public String getInfos_vue() {
		return infos_vue;
	}
	/**
	 * @param infos_vue the infos_vue to set
	 */
	public void setInfos_vue(String infos_vue) {
		this.infos_vue = infos_vue;
		this.infosColumn = new SimpleStringProperty(infos_vue);
	}
	/**
	 * @return the resolution
	 */
	public String getResolution() {
		return resolution;
	}
	/**
	 * @param resolution the resolution to set
	 */
	public void setResolution(String resolution) {
		this.resolution = resolution;
		this.resolutionColumn = new SimpleStringProperty(resolution);
	}
	/**
	 * @return the etat
	 */
	public etatFichierImage getEtat() {
		return etat;
	}
	/**
	 * @param etat the etat to set
	 */
	public void setEtat(String etat) {
		this.etat = etatFichierImage.valueOf(etat);
	}
	/**
	 * @return the est_actif
	 */
	public boolean isEst_actif() {
		return est_actif.intValue()==1?true:false;
	}
	/**
	 * @param est_actif the est_actif to set
	 */
	public void setEst_actif(Number est_actif) {
		this.est_actif = est_actif;
	}
	/**
	 * @return the date_ajout
	 */
	public Timestamp getDate_ajout() {
		return date_ajout;
	}
	/**
	 * @param date_ajout the date_ajout to set
	 */
	public void setDate_ajout(Timestamp date_ajout) {
		this.date_ajout = date_ajout;
		this.dateAjoutColumn = new SimpleStringProperty(date_ajout+"");
	}
	/**
	 * @return the est_partage
	 */
	public boolean isEst_partage() {
		return est_partage.intValue()==1?true:false;
	}
	
	/**
	 * @return the est_partage
	 */
	public Number getEst_partage() {
		return est_partage;
	}
	/**
	 * @return the est_partage
	 */
	public Number getEst_actif() {
		return est_actif;
	}
	/**
	 * @param est_partage the est_partage to set
	 */
	public void setEst_partage(Number est_partage) {
		this.est_partage = est_partage;
		this.partageColumn = new SimpleStringProperty(this.est_partage+"");
	}
	public List<Photo> getPhotos_associe() {
		return photos_associe;
	}
	public void setPhotos_associe(List<Photo> photos_associe) {
		this.photos_associe = photos_associe;
	}
	/**
	 * @return the mail_client
	 */
	public Client getMail_client() {
		return mail_client;
	}
	/**
	 * @param mail_client the mail_client to set
	 */
	public void setMail_client(Client mail_client) {
		this.mail_client = mail_client;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return libelle + ", " + infos_vue ;
	}
	
	public boolean equals(Object o) {
		if(o == null) return false;
		if(this.id_fic == null) return false;
		if (id_fic == ((FichierImage) o).getId_fic()) return true;
		return false;
	}
	
	public void switchPartage() {
		if (est_partage.intValue() == 1) {
			est_partage = 0;
		}
		else est_partage = 1;
		this.partageColumn.setValue(est_partage+"");
	}
	
	
	public String getLibelleColumn() {
		return libelleColumn.get();
	}
	public void setLibelleColumn(SimpleStringProperty libelleColumn) {
		this.libelleColumn = libelleColumn;
	}
	public String getPathColumn() {
		return pathColumn.get();
	}
	public void setPathColumn(SimpleStringProperty pathColumn) {
		this.pathColumn = pathColumn;
	}
	public String getInfosColumn() {
		return infosColumn.get();
	}
	public void setInfosColumn(SimpleStringProperty infosColumn) {
		this.infosColumn = infosColumn;
	}
	public String getResolutionColumn() {
		return resolutionColumn.get();
	}
	public void setResolutionColumn(SimpleStringProperty resolutionColumn) {
		this.resolutionColumn = resolutionColumn;
	}
	public String getDateAjoutColumn() {
		return dateAjoutColumn.get();
	}
	public void setDateAjoutColumn(SimpleStringProperty dateAjoutColumn) {
		this.dateAjoutColumn = dateAjoutColumn;
	}
	public String getPartageColumn() {
		return partageColumn.get();
	}
	public void setPartageColumn(SimpleStringProperty partageColumn) {
		this.partageColumn = partageColumn;
	}
	
	
	public String myToString() {
		return "FichierImage [id_fic =" + id_fic + ",libelle=" + libelle + "]";
	}
}
