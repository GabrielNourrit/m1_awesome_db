package models;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javafx.beans.property.SimpleStringProperty;
import services.IdentityPrimary;
import services.IgnoreQueryProperty;
import services.TirageService;
import util.ObjectPanier;

public class Article {
	
	@IdentityPrimary("article_id")
	private Number id_article;
	private Number quantite;
	private Number id_commande;
	
	/**
	 * @return the id_article
	 */
	public Number getId_article() {
		return id_article;
	}
	/**
	 * @param id_article the id_article to set
	 */
	public void setId_article(Number id_article) {
		this.id_article = id_article;
	}
	/**
	 * @return the quantite
	 */
	public Number getQuantite() {
		return quantite;
	}
	/**
	 * @param quantite the quantite to set
	 */
	public void setQuantite(Number quantite) {
		this.quantite = quantite;
	}
	public Number getId_commande() {
		return id_commande;
	}
	public void setId_commande(Number id_commande) {
		this.id_commande = id_commande;
	}
	@Override
	public String toString() {
		return "Article [id_article=" + id_article + ", quantite=" + quantite + ", id_commande=" + id_commande + "]";
	}
	
	public boolean equals(Object o) {
		if (id_article == ((Article) o).getId_article()) return true;
		return false;
	}
	
	
	public ObjectPanier getObjectPanier() {
		ObjectPanier op = new ObjectPanier();
		List<Tirage> lt;
		List<Calendrier> lcal;
		List<Cadre> lc;
		List<Agenda> lag;
		TirageService t = new TirageService();
		Album a = new Album();
		a.setId_album(id_article);
		List<Album> la = t.findByExample(a);
		
		if (la.size()==0) {
			Agenda ag = new Agenda();
			ag.setId_agenda(id_article);
			lag = t.findByExample(ag);
			
			if(lag.size()==0) {
				Cadre c = new Cadre();
				c.setId_cadre(id_article);
				lc = t.findByExample(c);
				
				if(lc.size()==0) {
					Calendrier cal = new Calendrier();
					cal.setId_calendrier(id_article);
					lcal = t.findByExample(cal);
					
					if(lcal.size()==0) {
						Tirage ti = new Tirage();
						ti.setId_tirage(id_article);
						lt = t.findByExample(ti);
						op.setType(new SimpleStringProperty("Tirage"));
					} else op.setType(new SimpleStringProperty("Calendrier"));
				} else op.setType(new SimpleStringProperty("Cadre"));
			} else op.setType(new SimpleStringProperty("Agenda"));
		} else op.setType(new SimpleStringProperty("Album"));
		
		Impression i = null;
		try {
			i = (Impression) t.findById(Impression.class, id_article, Connection.TRANSACTION_READ_COMMITTED);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		op.setId_impression(new SimpleStringProperty(id_article+""));
		op.setQuantite(new SimpleStringProperty(quantite+""));
		op.setPU(new SimpleStringProperty(i.getPu()+""));
		op.setReduction(new SimpleStringProperty(i.getPrix_promo()+""));
		return op;
	}
	
	
	
	public Article getTypeArticle() {
		Impression op = new Impression();
		List<Tirage> lt;
		List<Calendrier> lcal;
		List<Cadre> lc;
		List<Agenda> lag;
		TirageService t = new TirageService();
		Album a = new Album();
		a.setId_album(id_article);
		List<Album> la = t.findByExample(a);
		
		
		if (la.size()==0) {
			Agenda ag = new Agenda();
			ag.setId_agenda(id_article);
			lag = t.findByExample(ag);
			
			if(lag.size()==0) {
				Cadre c = new Cadre();
				c.setId_cadre(id_article);
				lc = t.findByExample(c);
				
				if(lc.size()==0) {
					Calendrier cal = new Calendrier();
					cal.setId_calendrier(id_article);
					lcal = t.findByExample(cal);
					
					if(lcal.size()==0) {
						Tirage ti = new Tirage();
						ti.setId_tirage(id_article);
						lt = t.findByExample(ti);
						op = lt.get(0);
					} else op = lcal.get(0);
				} else op = lc.get(0);
			} else op = lag.get(0);
		} else op = la.get(0);
		Impression i = null;
		try {
			i = (Impression) t.findById(Impression.class, id_article, Connection.TRANSACTION_READ_COMMITTED);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		op.setId_impression(id_article);
		op.setPu(i.getPu());
		op.setQuantite(quantite);
		
		return op;
	}
}
