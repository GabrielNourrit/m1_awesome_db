package models;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import services.IdentityPrimary;
import services.IgnoreQueryProperty;
import services.TirageService;

public class Album extends Impression{
	
	@IdentityPrimary("")
	private Number id_album;
	private String titre;
	private String infos_miseenpage;
	private String format;
	private String qualite;
	private Photo id_photo;
	@IgnoreQueryProperty
	private LK lk_album_photo;
	/**
	 * @return the id_album
	 */
	public Number getId_album() {
		return id_album;
	}
	/**
	 * @param id_album the id_album to set
	 */
	public void setId_album(Number id_album) {
		this.id_album = id_album;
	}
	/**
	 * @return the titre
	 */
	public String getTitre() {
		return titre;
	}
	/**
	 * @param titre the titre to set
	 */
	public void setTitre(String titre) {
		this.titre = titre;
	}


	/**
	 * @return the format
	 */
	public String getFormat() {
		return format;
	}
	/**
	 * @param format the format to set
	 */
	public void setFormat(String format) {
		this.format = format;
	}
	/**
	 * @return the qualite
	 */
	public String getQualite() {
		return qualite;
	}
	/**
	 * @param qualite the qualite to set
	 */
	public void setQualite(String qualite) {
		this.qualite = qualite;
	}
	public Photo getId_photo() {
		return id_photo;
	}
	public void setId_photo(Photo photo_titre) {
		this.id_photo = photo_titre;
	}
	public String getInfos_miseenpage() {
		return infos_miseenpage;
	}
	public void setInfos_miseenpage(String infos_Miseenpage) {
		this.infos_miseenpage = infos_Miseenpage;
	}
	
	
	public List<Photo> getAllPhoto() {
		List<Lk_Album_Photo> l = lk_album_photo.get_Lk_Album_Photo_byAlbum(this);
		List<Photo> ret = new ArrayList<Photo>();
		for (Lk_Album_Photo lk : l) ret.add(lk.getId_photo());
		return ret;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Album [id_album=" + id_album + ", titre=" + titre + ", infos_miseenpage=" + infos_miseenpage
				+ ", format=" + format + ", qualite=" + qualite + ", photo_titre=" + id_photo + "]";
	}
	
	public boolean equals(Object o) {
		if (id_album == ((Album) o).getId_album()) return true;
		return false;
	}
	
	public String customToString() {
		return "Album [id_album=" + id_album + ", titre=" + titre + ", infos_miseenpage=" + infos_miseenpage
				+ ", format=" + format + ", qualite=" + qualite + ", photo_titre=" + id_photo + "]";
	}
	
	public String toStringDetails() {
		TirageService t = new TirageService();
		List<Photo> lp = new ArrayList<Photo>();
		Lk_Album_Photo cp = new Lk_Album_Photo();
		List<Lk_Album_Photo> lks = t.findLKById(this.getClass(), this.id_album, Lk_Album_Photo.class, Connection.TRANSACTION_READ_COMMITTED);
		for (Lk_Album_Photo lk : lks) lp.add(lk.getId_photo());
		return toString() + "\n" + lp;
	}
}
