package models;

public class Models {
	
	/*------------- DEBUT CLASSE AdresseLivraison -------------*/
	public static String[] getAdresseLivraisonAttributSimple() {
		String[] tab = {"id_adresse_livraison","numero","rue","nom","prenom","ville","code_postal"};
		return tab;
	}
	public static String[] getAdresseLivraisonAttributFK() {
		String[] tab = {};
		return tab;
	}
	public static Class<?>[] getAdresseLivraisonAttributLK() {
		Class<?>[] tab = {Lk_AdresseLivraison_Client.class};
		return tab;
	}
	/*------------- FIN CLASSE AdresseLivraison -------------*/
	
	
	
	/*------------- DEBUT CLASSE Agenda -------------*/
	public static String[] getAgendaAttributSimple() {
		String[] tab = {"id_agenda","modele","type"};
		return tab;
	}
	public static String[] getAgendaAttributFK() {
		String[] tab = {};
		return tab;
	}
	public static Class<?>[] getAgendaAttributLK() {
		Class<?>[] tab = {Lk_Agenda_Photo.class};
		return tab;
	}
	/*------------- FIN CLASSE Agenda -------------*/
	
	
	/*------------- DEBUT CLASSE Album -------------*/
	public static String[] getAlbumAttributSimple() {
		String[] tab = {"id_album","titre","infos_MiseEnPage","format","qualite"};
		return tab;
	}
	public static String[] getAlbumAttributFK() {
		String[] tab = {"id_photo"};
		return tab;
	}
	public static Class<?>[] getAlbumAttributLK() {
		Class<?>[] tab = {Lk_Album_Photo.class};
		return tab;
	}
	/*------------- FIN CLASSE Album -------------*/
	
	
	/*------------- DEBUT CLASSE Article -------------*/
	public static String[] getArticleAttributSimple() {
		String[] tab = {"id_article","quantite","id_commande"};
		return tab;
	}
	public static String[] getArticleAttributFK() {
		String[] tab = {};
		return tab;
	}
	public static Class<?>[] getArticleAttributLK() {
		Class<?>[] tab = {};
		return tab;
	}
	/*------------- FIN CLASSE Article -------------*/
	
	
	/*------------- DEBUT CLASSE Cadre -------------*/
	public static String[] getCadreAttributSimple() {
		String[] tab = {"id_cadre","taille","modele","mise_en_page"};
		return tab;
	}
	public static String[] getCadreAttributFK() {
		String[] tab = {};
		return tab;
	}
	public static Class<?>[] getCadreAttributLK() {
		Class<?>[] tab = {Lk_Cadre_Photo.class};
		return tab;
	}
	/*------------- FIN CLASSE Cadre -------------*/
	
	
	/*------------- DEBUT CLASSE Calendrier -------------*/
	public static String[] getCalendrierAttributSimple() {
		String[] tab = {"id_calendrier","modele"};
		return tab;
	}
	public static String[] getCalendrierAttributFK() {
		String[] tab = {};
		return tab;
	}
	public static Class<?>[] getCalendrierAttributLK() {
		Class<?>[] tab = {Lk_Calendrier_Photo.class};
		return tab;
	}
	/*------------- FIN CLASSE Calendrier -------------*/
	
	
	/*------------- DEBUT CLASSE Client -------------*/
	public static String[] getClientAttributSimple() {
		String[] tab = {"mail_client","nom","prenom","adresse","etat","password"};
		return tab;
	}
	public static String[] getClientAttributFK() {
		String[] tab = {};
		return tab;
	}
	public static Class<?>[] getClientAttributLK() {
		Class<?>[] tab = {Lk_AdresseLivraison_Client.class};
		return tab;
	}
	/*------------- FIN CLASSE Client -------------*/
	
	
	
	/*------------- DEBUT CLASSE CodePromo -------------*/
	public static String[] getCodePromoAttributSimple() {
		String[] tab = {"code_promo","remise","nombre_utilisation","est_public","date_fin_code"};
		return tab;
	}
	public static String[] getCodePromoAttributFK() {
		String[] tab = {"id_commande"};
		return tab;
	}
	public static Class<?>[] getCodePromoAttributLK() {
		Class<?>[] tab = {Lk_CodePromo_Commande.class};
		return tab;
	}
	/*------------- FIN CLASSE CodePromo -------------*/
	
	
	/*------------- DEBUT CLASSE Commande -------------*/
	public static String[] getCommandeAttributSimple() {
		String[] tab = {"id_commande","date_commande","mode_livraison","code_point_relais","prix_tt","prix_tt_remise","statut","id_adresse_livraison"};
		return tab;
	}
	public static String[] getCommandeAttributFK() {
		String[] tab = {"mail_client","id_adresse_livraison"};
		return tab;
	}
	public static Class<?>[] getCommandeAttributLK() {
		Class<?>[] tab = {Lk_CodePromo_Commande.class};
		return tab;
	}
	/*------------- FIN CLASSE Commande -------------*/
	
	
	/*------------- DEBUT CLASSE FichierImage -------------*/
	public static String[] getFichierImageAttributSimple() {
		String[] tab = {"id_fic","libelle","chemin","infos_vue","resolution","etat","date_ajout","est_actif","est_partage"};
		return tab;
	}
	public static String[] getFichierImageAttributFK() {
		String[] tab = {"mail_client"};
		return tab;
	}
	public static Class<?>[] getFichierImageAttributLK() {
		Class<?>[] tab = {};
		return tab;
	}
	/*------------- FIN CLASSE FichierImage -------------*/


	/*------------- DEBUT CLASSE Impression -------------*/
	public static String[] getImpressionAttributSimple() {
		String[] tab = {"id_impression","pu","estPromo","prix_promo","id_article"};
		return tab;
	}
	public static String[] getImpressionAttributFK() {
		String[] tab = {};
		return tab;
	}
	public static Class<?>[] getImpressionAttributLK() {
		Class<?>[] tab = {};
		return tab;
	}
	/*------------- FIN CLASSE Impression -------------*/

	
	/*------------- DEBUT CLASSE Photo -------------*/
	public static String[] getPhotoAttributSimple() {
		String[] tab = {"id_photo","retouches", "mail_client","etat"};
		return tab;
	}
	public static String[] getPhotoAttributFK() {
		String[] tab = {"id_fic"};
		return tab;
	}
	public static Class<?>[] getPhotoAttributLK() {
		Class<?>[] tab = {Lk_Album_Photo.class,Lk_Agenda_Photo.class,Lk_Cadre_Photo.class,Lk_Calendrier_Photo.class,Lk_Tirage_Photo.class};
		return tab;
	}
	/*------------- FIN CLASSE Photo -------------*/
	
	
	/*------------- DEBUT CLASSE Tirage -------------*/
	public static String[] getTirageAttributSimple() {
		String[] tab = {"id_tirage","qualite","format"};
		return tab;
	}
	public static String[] getTirageAttributFK() {
		String[] tab = {};
		return tab;
	}
	public static Class<?>[] getTirageAttributLK() {
		Class<?>[] tab = {Lk_Tirage_Photo.class};
		return tab;
	}
	/*------------- FIN CLASSE Tirage -------------*/
	
	
	
	/*------------- DEBUT CLASSE Lk_AdresseLivraison_Client -------------*/
	public static String[] getLk_AdresseLivraison_ClientAttributSimple() {
		String[] tab = {};
		return tab;
	}
	public static String[] getLk_AdresseLivraison_ClientAttributFK() {
		String[] tab = {"id_adresse_livraison","id_client"};
		return tab;
	}
	public static Class<?>[] getLk_AdresseLivraison_ClientAttributLK() {
		Class<?>[] tab = {};
		return tab;
	}
	/*------------- FIN CLASSE Lk_AdresseLivraison_Client -------------*/
	
	
	/*------------- DEBUT CLASSE Lk_Agenda_Photo -------------*/
	public static String[] getLk_Agenda_PhotoAttributSimple() {
		String[] tab = {"no_page"};
		return tab;
	}
	public static String[] getLk_Agenda_PhotoAttributFK() {
		String[] tab = {"id_agenda","id_photo"};
		return tab;
	}
	public static Class<?>[] getLk_Agenda_PhotoAttributLK() {
		Class<?>[] tab = {};
		return tab;
	}
	/*------------- FIN CLASSE Lk_Agenda_Photo -------------*/
	
	
	/*------------- DEBUT CLASSE Lk_Album_Photo -------------*/
	public static String[] getLk_Album_PhotoAttributSimple() {
		String[] tab = {"description","ordre","page"};
		return tab;
	}
	public static String[] getLk_Album_PhotoAttributFK() {
		String[] tab = {"id_album","id_photo"};
		return tab;
	}
	public static Class<?>[] getLk_Album_PhotoAttributLK() {
		Class<?>[] tab = {};
		return tab;
	}
	/*------------- FIN CLASSE Lk_Album_Photo -------------*/
	
	
	/*------------- DEBUT CLASSE Lk_Cadre_Photo -------------*/
	public static String[] getLk_Cadre_PhotoAttributSimple() {
		String[] tab = {"page"};
		return tab;
	}
	public static String[] getLk_Cadre_PhotoAttributFK() {
		String[] tab = {"id_cadre","id_photo"};
		return tab;
	}
	public static Class<?>[] getLk_Cadre_PhotoAttributLK() {
		Class<?>[] tab = {};
		return tab;
	}
	/*------------- FIN CLASSE Lk_Cadre_Photo -------------*/
	
	
	/*------------- DEBUT CLASSE Lk_Calendrier_Photo -------------*/
	public static String[] getLk_Calendrier_PhotoAttributSimple() {
		String[] tab = {"page","ordre"};
		return tab;
	}
	public static String[] getLk_Calendrier_PhotoAttributFK() {
		String[] tab = {"id_calendrier","id_photo"};
		return tab;
	}
	public static Class<?>[] getLk_Calendrier_PhotoAttributLK() {
		Class<?>[] tab = {};
		return tab;
	}
	/*------------- FIN CLASSE Lk_Calendrier_Photo -------------*/
	
	/*------------- DEBUT CLASSE Lk_CodePromo_Commande -------------*/
	public static String[] getLk_CodePromo_CommandeAttributSimple() {
		String[] tab = {};
		return tab;
	}
	public static String[] getLk_CodePromo_CommandeAttributFK() {
		String[] tab = {"code_promo","id_commande"};
		return tab;
	}
	public static Class<?>[] getLk_CodePromo_CommandeAttributLK() {
		Class<?>[] tab = {};
		return tab;
	}
	/*------------- FIN CLASSE Lk_CodePromo_Commande -------------*/
	
	/*------------- DEBUT CLASSE Lk_Tirage_Photo -------------*/
	public static String[] getLk_Tirage_PhotoAttributSimple() {
		String[] tab = {"quantite"};
		return tab;
	}
	public static String[] getLk_Tirage_PhotoAttributFK() {
		String[] tab = {"id_tirage","id_photo"};
		return tab;
	}
	public static Class<?>[] getLk_Tirage_PhotoAttributLK() {
		Class<?>[] tab = {};
		return tab;
	}
	/*------------- FIN CLASSE Lk_Tirage_Photo -------------*/
}
