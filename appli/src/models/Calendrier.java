package models;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import services.IdentityPrimary;
import services.IgnoreQueryProperty;
import services.TirageService;
import type.calendrierModel;

public class Calendrier extends Impression {
	
	@IdentityPrimary("")
	private Number id_calendrier;
	private calendrierModel modele;
	@IgnoreQueryProperty
	private LK lk_calendrier_photo;
	/**
	 * @return the id_calendrier
	 */
	public Number getId_calendrier() {
		return id_calendrier;
	}
	/**
	 * @param id_calendrier the id_calendrier to set
	 */
	public void setId_calendrier(Number id_calendrier) {
		this.id_calendrier = id_calendrier;
	}
	/**
	 * @return the modele
	 */
	public calendrierModel getModele() {
		return modele;
	}
	/**
	 * @param modele the modele to set
	 */
	public void setModele(String modele) {
		this.modele = calendrierModel.valueOf(modele);
	}
	
	
	public List<Photo> getAllPhoto() {
		List<Lk_Calendrier_Photo> l = lk_calendrier_photo.get_Lk_Calendrier_Photo_byCalendrier(this);
		List<Photo> ret = new ArrayList<Photo>();
		for (Lk_Calendrier_Photo lk : l) ret.add(lk.getId_photo());
		return ret;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Calendrier [id_calendrier=" + id_calendrier + ", modele=" + modele + "]";
	}
	
	public String customToString() {
		return "Calendrier [id_calendrier=" + id_calendrier + ", modele=" + modele + "]";
	}
	
	public boolean equals(Object o) {
		if (id_calendrier == ((Calendrier) o).getId_calendrier()) return true;
		return false;
	}
	
	public String toStringDetails() {
		TirageService t = new TirageService();
		List<Photo> lp = new ArrayList<Photo>();
		Lk_Calendrier_Photo cp = new Lk_Calendrier_Photo();
		cp.setId_calendrier(this);
		List<Lk_Calendrier_Photo> lks = t.findLKById(this.getClass(), this.id_calendrier, Lk_Calendrier_Photo.class, Connection.TRANSACTION_READ_COMMITTED);
		for (Lk_Calendrier_Photo lk : lks) lp.add(lk.getId_photo());
		return toString() + "\n" + lp;
	}
}
