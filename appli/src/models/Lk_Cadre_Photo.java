package models;

import java.sql.SQLException;

import services.TirageService;

public class Lk_Cadre_Photo implements LkImpressionnable {

	private Cadre id_cadre;
	private Photo id_photo;
	private Number page;


	public Lk_Cadre_Photo(Cadre cadre, Photo photo, int page) {
		this.id_cadre = cadre;
		this.id_photo = photo;
		this.page = page;
	}
	public Lk_Cadre_Photo() {
	}


	public Cadre getId_cadre() {
		return id_cadre;
	}
	public void setId_cadre(Number id) throws SQLException {
		TirageService t = new TirageService();
		this.id_cadre = (Cadre) t.findById(Cadre.class, id);
	}
	public void setId_cadre(Cadre id) {
		this.id_cadre = id;
	}


	public Photo getId_photo() {
		return id_photo;
	}
	public void setId_photo(Number id) throws SQLException {
		TirageService t = new TirageService();
		this.id_photo = (Photo) t.findById(Photo.class, id);
	}
	public void setId_photo(Photo id) {
		this.id_photo = id;
	}


	public Number getPage() {
		return page;
	}
	public void setPage(Number page) {
		this.page = page;
	}

	public boolean equals(Object o) {
		if (o instanceof Lk_Cadre_Photo) {
			Lk_Cadre_Photo lk = (Lk_Cadre_Photo) o;
			if (id_cadre.equals(lk.getId_cadre()) && id_photo.equals(lk.getId_photo())) return true;
		}
		return false;
	}

	@Override
	public void setId(Object o) {
		this.setId_cadre((Cadre) o);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Lk_Cadre_Photo [Cadre="+this.id_cadre.getId_cadre()+",photo="+this.id_photo.getId_photo()+",page="+this.page+"]";
	}
}
