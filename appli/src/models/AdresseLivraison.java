package models;

import java.util.List;

import services.IdentityPrimary;
import services.IgnoreQueryProperty;
import services.TirageService;

public class AdresseLivraison {
	
	@IdentityPrimary("")
	private Number id_adresse_livraison;
	private String numero;
	private String rue;
	private String nom;
	private String prenom;
	private String ville;
	private String code_postal;
	// Liste des commandes pass�es � ces adresses
	@IgnoreQueryProperty
	private List<Commande> commandes_associe;
	@IgnoreQueryProperty
	private List<LK> lk_adresse_client;
	
	/**
	 * @return the id_adresse_livraison
	 */
	public Number getId_adresse_livraison() {
		return id_adresse_livraison;
	}
	/**
	 * @param id_adresse_livraison the id_adresse_livraison to set
	 */
	public void setId_adresse_livraison(Number id_adresse_livraison) {
		this.id_adresse_livraison = id_adresse_livraison;
	}

	public List<Commande> getCommandeAssocie() {
		TirageService t = new TirageService();
		// On recupere la liste des codes promo
		Commande exampleCode = new Commande();
		exampleCode.setId_commande(this.id_adresse_livraison);
		return this.commandes_associe = t.findByExample(exampleCode);	
	}
	/**
	 * @return the numero
	 */
	public String getNumero() {
		return numero;
	}
	/**
	 * @param numero the numero to set
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}
	/**
	 * @return the code_postal
	 */
	public String getCode_postal() {
		return code_postal;
	}
	/**
	 * @param code_postal the code_postal to set
	 */
	public void setCode_postal(String code_postal) {
		this.code_postal = code_postal;
	}
	/**
	 * @return the rue
	 */
	public String getRue() {
		return rue;
	}
	/**
	 * @param rue the rue to set
	 */
	public void setRue(String rue) {
		this.rue = rue;
	}
	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return the prenom
	 */
	public String getPrenom() {
		return prenom;
	}
	/**
	 * @param prenom the prenom to set
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	/**
	 * @return the ville
	 */
	public String getVille() {
		return ville;
	}
	/**
	 * @param ville the ville to set
	 */
	public void setVille(String ville) {
		this.ville = ville;
	}
	/**
	 * @return the code_Postal
	 */
	public String getCode_Postal() {
		return code_postal;
	}
	/**
	 * @param code_Postal the code_Postal to set
	 */
	public void setCode_Postal(String code_Postal) {
		this.code_postal = code_Postal;
	}
	public List<Commande> getCommandes_associe() {
		return commandes_associe;
	}
	public void setCommandes_associe(List<Commande> commandes_associe) {
		this.commandes_associe = commandes_associe;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AdresseLivraison [id_adresse_livraison=" + id_adresse_livraison + ", numero=" + numero + ", rue=" + rue
				+ ", nom=" + nom + ", prenom=" + prenom + ", ville=" + ville + ", code_postal=" + code_postal
				+ ", commandes_associe=" + commandes_associe + "]";
	}
	
	public boolean equals(Object o) {
		if(o == null) return false;
		if(this.id_adresse_livraison == null) return false;
		if (id_adresse_livraison == ((AdresseLivraison) o).getId_adresse_livraison()) return true;
		return false;
	}
	
	
}
