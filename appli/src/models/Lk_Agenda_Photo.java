package models;

import java.sql.SQLException;

import services.TirageService;

public class Lk_Agenda_Photo implements LkImpressionnable {

	private Agenda id_agenda;
	private Photo id_photo;
	private Number no_page;


	public Lk_Agenda_Photo(Agenda agenda, Photo photo, int no_page) {
		this.id_agenda = agenda;
		this.id_photo = photo;
		this.no_page = no_page;
	}
	public Lk_Agenda_Photo() {
	}

	public Agenda getId_agenda() {
		return id_agenda;
	}
	public void setId_agenda(Number id) throws SQLException {
		TirageService t = new TirageService();
		this.id_agenda = (Agenda) t.findById(Agenda.class, id);
	}
	public void setId_agenda(Agenda id) {
		this.id_agenda = id;
	}

	public Photo getId_photo() {
		return id_photo;
	}
	public void setId_photo(Number id) throws SQLException {
		TirageService t = new TirageService();
		this.id_photo = (Photo) t.findById(Photo.class, id);
	}
	public void setId_photo(Photo id) {
		this.id_photo = id;
	}


	public Number getNo_page() {
		return no_page;
	}
	public void setNo_page(Number no_page) {
		this.no_page = no_page;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Lk_Agenda_Photo [Agenda="+this.id_agenda.getId_agenda()+",photo="+this.id_photo.getId_photo()+",no_page="+this.no_page+"]";
	}

	public boolean equals(Object o) {
		if (o instanceof Lk_Agenda_Photo) {
			Lk_Agenda_Photo lk = (Lk_Agenda_Photo) o;
			if (id_agenda.equals(lk.getId_agenda()) && id_photo.equals(lk.getId_photo())) return true;
		}
		return false;
	}
	@Override
	public void setId(Object o) {
		this.setId_agenda((Agenda) o);
	}
}
