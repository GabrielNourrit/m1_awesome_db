package models;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import services.IdentityPrimary;
import services.IgnoreQueryProperty;
import services.TirageService;
import type.agendaModel;

public class Agenda extends Impression {
	
	@IdentityPrimary("")
	private Number id_agenda;
	private agendaModel modele;
	private String type;
	@IgnoreQueryProperty
	private LK lk_agenda_photo;
	/**
	 * @return the id_agenda
	 */
	public Number getId_agenda() {
		return id_agenda;
	}
	/**
	 * @param id_agenda the id_agenda to set
	 */
	public void setId_agenda(Number id_agenda) {
		this.id_agenda = id_agenda;
	}
	/**
	 * @return the modele
	 */
	public agendaModel getModele() {
		return modele;
	}
	/**
	 * @param modele the modele to set
	 */
	public void setModele(String modele) {
		this.modele = agendaModel.valueOf(modele);
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	public List<Photo> getAllPhoto() {
		List<Lk_Agenda_Photo> l = lk_agenda_photo.get_Lk_Agenda_Photo_byAgenda(this);
		List<Photo> ret = new ArrayList<Photo>();
		for (Lk_Agenda_Photo lk : l) ret.add(lk.getId_photo());
		return ret;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Agenda [id_agenda=" + id_agenda + ", modele=" + modele + ", type=" + type + "]";
	}
	
	public String customToString() {
		return "Agenda [id_agenda=" + id_agenda + ", modele=" + modele + ", type=" + type + "]";
	}
	
	public boolean equals(Object o) {
		if (id_agenda == ((Agenda) o).getId_agenda()) return true;
		return false;
	}
	
	public String toStringDetails() {
		TirageService t = new TirageService();
		List<Photo> lp = new ArrayList<Photo>();
		Lk_Agenda_Photo cp = new Lk_Agenda_Photo();
		List<Lk_Agenda_Photo> lks = t.findLKById(this.getClass(), this.id_agenda, Lk_Agenda_Photo.class, Connection.TRANSACTION_READ_COMMITTED);
		for (Lk_Agenda_Photo lk : lks) lp.add(lk.getId_photo());
		return toString() + "\n" + lp;
		
	}
}
