package models;

import java.util.ArrayList;
import java.util.List;

public class LK {

	private List<Lk_AdresseLivraison_Client> lk_adresseLivraison_client = new ArrayList<Lk_AdresseLivraison_Client>();
	private List<Lk_Agenda_Photo> lk_agenda_photo = new ArrayList<Lk_Agenda_Photo>();
	private List<Lk_Album_Photo> lk_album_photo = new ArrayList<Lk_Album_Photo>();
	private List<Lk_Cadre_Photo> lk_cadre_photo = new ArrayList<Lk_Cadre_Photo>();
	private List<Lk_Calendrier_Photo> lk_calendrier_photo = new ArrayList<Lk_Calendrier_Photo>();
	private List<Lk_Tirage_Photo> lk_tirage_photo = new ArrayList<Lk_Tirage_Photo>();
	private List<Lk_CodePromo_Commande> lk_codePromo_commande = new ArrayList<Lk_CodePromo_Commande>();
	
	private static final LK instance = new LK();
	
	private LK() {}
	
	public static LK getInstance() {
		return instance;
	}
	
	
	
	
	/*------------------------ DEBUT Lk_AdresseLivraison_Client ------------------------*/
	
	public void ajouter_Lk_AdresseLivraison_Client(AdresseLivraison a, Client c) {
		if (!lk_adresseLivraison_client.contains(new Lk_AdresseLivraison_Client(a,c))) lk_adresseLivraison_client.add(new Lk_AdresseLivraison_Client(a,c));
	}
	public void ajouter_Lk_AdresseLivraison_Client(Lk_AdresseLivraison_Client lk) {
		if (!lk_adresseLivraison_client.contains(lk)) lk_adresseLivraison_client.add(lk);
	}
	public List<Lk_AdresseLivraison_Client> get_Lk_AdresseLivraison_Client_byClient(Client c) {
		List<Lk_AdresseLivraison_Client> l = new ArrayList<Lk_AdresseLivraison_Client>();
		for (Lk_AdresseLivraison_Client lk : lk_adresseLivraison_client) {
			if (lk.getMail_client().equals(c)) l.add(lk);
		}
		return l;
	}
	public List<Lk_AdresseLivraison_Client> get_Lk_AdresseLivraison_Client_byAdresseLivraison(AdresseLivraison a) {
		List<Lk_AdresseLivraison_Client> l = new ArrayList<Lk_AdresseLivraison_Client>();
		for (Lk_AdresseLivraison_Client lk : lk_adresseLivraison_client) {
			if (lk.getId_adresse_livraison().equals(a)) l.add(lk);
		}
		return l;
	}
	public List<Lk_AdresseLivraison_Client> getLk_adresseLivraison_client() {
		return this.lk_adresseLivraison_client;
	}
	
	/*------------------------ FIN Lk_AdresseLivraison_Client ------------------------*/
	
	
	
	
	/*------------------------ DEBUT Lk_Agenda_Photo ------------------------*/
	
	public void ajouter_Lk_Agenda_Photo(Agenda a, Photo p, int no_page) {
		if (!lk_agenda_photo.contains(new Lk_Agenda_Photo(a,p,no_page))) lk_agenda_photo.add(new Lk_Agenda_Photo(a,p,no_page));
	}
	public void ajouter_Lk_Agenda_Photo(Lk_Agenda_Photo lk) {
		if (!lk_agenda_photo.contains(lk)) lk_agenda_photo.add(lk);
	}
	public List<Lk_Agenda_Photo> get_Lk_Agenda_Photo_byAgenda(Agenda a) {
		List<Lk_Agenda_Photo> l = new ArrayList<Lk_Agenda_Photo>();
		for (Lk_Agenda_Photo lk : lk_agenda_photo) {
			if (lk.getId_agenda().equals(a)) l.add(lk);
		}
		return l;
	}
	public List<Lk_Agenda_Photo> get_Lk_Agenda_Photo_byPhoto(Photo p) {
		List<Lk_Agenda_Photo> l = new ArrayList<Lk_Agenda_Photo>();
		for (Lk_Agenda_Photo lk : lk_agenda_photo) {
			if (lk.getId_photo().equals(p)) l.add(lk);
		}
		return l;
	}
	public List<Lk_Agenda_Photo> getLk_agenda_photo() {
		return this.lk_agenda_photo;
	}
	
	/*------------------------ FIN Lk_Agenda_Photo ------------------------*/
	
	
	
	/*------------------------ DEBUT Lk_Album_Photo ------------------------*/
	
	public void ajouter_Lk_Album_Photo(Album a, Photo p, String description, int ordre, int page) {
		if (!lk_album_photo.contains(new Lk_Album_Photo(a,p,description,ordre,page))) lk_album_photo.add(new Lk_Album_Photo(a,p,description,ordre,page));
	}
	public void ajouter_Lk_Album_Photo(Lk_Album_Photo lk) {
		if (!lk_album_photo.contains(lk)) lk_album_photo.add(lk);
	}
	public List<Lk_Album_Photo> get_Lk_Album_Photo_byAlbum(Album a) {
		List<Lk_Album_Photo> l = new ArrayList<Lk_Album_Photo>();
		for (Lk_Album_Photo lk : lk_album_photo) {
			if (lk.getId_album().equals(a)) l.add(lk);
		}
		return l;
	}
	public List<Lk_Album_Photo> get_Lk_Album_Photo_byPhoto(Photo p) {
		List<Lk_Album_Photo> l = new ArrayList<Lk_Album_Photo>();
		for (Lk_Album_Photo lk : lk_album_photo) {
			if (lk.getId_photo().equals(p)) l.add(lk);
		}
		return l;
	}
	public List<Lk_Album_Photo> getLk_album_photo() {
		return this.lk_album_photo;
	}
	
	/*------------------------ DEBUT Lk_Album_Photo ------------------------*/
	
	
	
	
	/*------------------------ DEBUT Lk_Cadre_Photo ------------------------*/
	
	public void ajouter_Lk_Cadre_Photo(Cadre c, Photo p, int page) {
		if (!lk_cadre_photo.contains(new Lk_Cadre_Photo(c,p,page))) lk_cadre_photo.add(new Lk_Cadre_Photo(c,p,page));
	}
	public void ajouter_Lk_Cadre_Photo(Lk_Cadre_Photo lk) {
		if (!lk_cadre_photo.contains(lk)) lk_cadre_photo.add(lk);
	}
	public List<Lk_Cadre_Photo> get_Lk_Cadre_Photo_byCadre(Cadre c) {
		List<Lk_Cadre_Photo> l = new ArrayList<Lk_Cadre_Photo>();
		for (Lk_Cadre_Photo lk : lk_cadre_photo) {
			if (lk.getId_cadre().equals(c)) l.add(lk);
		}
		return l;
	}
	public List<Lk_Cadre_Photo> get_Lk_Cadre_Photo_byPhoto(Photo p) {
		List<Lk_Cadre_Photo> l = new ArrayList<Lk_Cadre_Photo>();
		for (Lk_Cadre_Photo lk : lk_cadre_photo) {
			if (lk.getId_photo().equals(p)) l.add(lk);
		}
		return l;
	}
	public List<Lk_Cadre_Photo> getLk_cadre_photo() {
		return this.lk_cadre_photo;
	}
	
	/*------------------------ Fin Lk_Cadre_Photo ------------------------*/
	
	
	
	
	/*------------------------ DEBUT Lk_Calendrier_Photo ------------------------*/
	
	public void ajouter_Lk_Calendrier_Photo(Calendrier c, Photo p, int ordre, int page) {
		if (!lk_calendrier_photo.contains(new Lk_Calendrier_Photo(c,p,ordre,page))) lk_calendrier_photo.add(new Lk_Calendrier_Photo(c,p,ordre,page));
	}
	public void ajouter_Lk_Calendrier_Photo(Lk_Calendrier_Photo lk) {
		if (!lk_calendrier_photo.contains(lk)) lk_calendrier_photo.add(lk);
	}
	public List<Lk_Calendrier_Photo> get_Lk_Calendrier_Photo_byCalendrier(Calendrier c) {
		List<Lk_Calendrier_Photo> l = new ArrayList<Lk_Calendrier_Photo>();
		for (Lk_Calendrier_Photo lk : lk_calendrier_photo) {
			if (lk.getId_calendrier().equals(c)) l.add(lk);
		}
		return l;
	}
	public List<Lk_Calendrier_Photo> get_Lk_Calendrier_Photo_byPhoto(Photo p) {
		List<Lk_Calendrier_Photo> l = new ArrayList<Lk_Calendrier_Photo>();
		for (Lk_Calendrier_Photo lk : lk_calendrier_photo) {
			if (lk.getId_photo().equals(p)) l.add(lk);
		}
		return l;
	}
	public List<Lk_Calendrier_Photo> getLk_calendrier_photo() {
		return this.lk_calendrier_photo;
	}
	
	/*------------------------ FIN Lk_Calendrier_Photo ------------------------*/
	
	
	
	
	/*------------------------ DEBUT Lk_Tirage_Photo ------------------------*/
	
	public void ajouter_Lk_Tirage_Photo(Tirage t, Photo p, int quantite) {
		if (!lk_tirage_photo.contains(new Lk_Tirage_Photo(t,p,quantite))) lk_tirage_photo.add(new Lk_Tirage_Photo(t,p,quantite));
	}
	public void ajouter_Lk_Tirage_Photo(Lk_Tirage_Photo lk) {
		if (!lk_tirage_photo.contains(lk)) lk_tirage_photo.add(lk);
	}
	public List<Lk_Tirage_Photo> get_Lk_Tirage_Photo_byTirage(Tirage t) {
		List<Lk_Tirage_Photo> l = new ArrayList<Lk_Tirage_Photo>();
		for (Lk_Tirage_Photo lk : lk_tirage_photo) {
			if (lk.getId_tirage().equals(t)) l.add(lk);
		}
		return l;
	}
	public List<Lk_Tirage_Photo> get_Lk_Tirage_Photo_byPhoto(Photo p) {
		List<Lk_Tirage_Photo> l = new ArrayList<Lk_Tirage_Photo>();
		for (Lk_Tirage_Photo lk : lk_tirage_photo) {
			if (lk.getId_photo().equals(p)) l.add(lk);
		}
		return l;
	}
	public List<Lk_Tirage_Photo> getLk_tirage_photo() {
		return this.lk_tirage_photo;
	}
	
	/*------------------------ FIN Lk_Tirage_Photo ------------------------*/
	
	
	
	
	/*------------------------ DEBUT Lk_CodePromo_Commande ------------------------*/
	
	public void ajouter_Lk_CodePromo_Commande(CodePromo cp, Commande cmd) {
		if (!lk_codePromo_commande.contains(new Lk_CodePromo_Commande(cp,cmd))) lk_codePromo_commande.add(new Lk_CodePromo_Commande(cp,cmd));
	}
	public void ajouter_Lk_CodePromo_Commande(Lk_CodePromo_Commande lk) {
		if (!lk_codePromo_commande.contains(lk)) lk_codePromo_commande.add(lk);
	}
	public List<Lk_CodePromo_Commande> get_Lk_CodePromo_Commande_byCodePromo(CodePromo cp) {
		List<Lk_CodePromo_Commande> l = new ArrayList<Lk_CodePromo_Commande>();
		for (Lk_CodePromo_Commande lk : lk_codePromo_commande) {
			if (lk.getCode_promo().equals(cp)) l.add(lk);
		}
		return l;
	}
	public List<Lk_CodePromo_Commande> get_Lk_CodePromo_Commande_byCommande(Commande cmd) {
		List<Lk_CodePromo_Commande> l = new ArrayList<Lk_CodePromo_Commande>();
		for (Lk_CodePromo_Commande lk : lk_codePromo_commande) {
			if (lk.getId_commande().equals(cmd)) l.add(lk);
		}
		return l;
	}
	public List<Lk_CodePromo_Commande> getLk_CodePromo_Commande() {
		return this.lk_codePromo_commande;
	}
	
	/*------------------------ FIN Lk_CodePromo_Commande ------------------------*/
}