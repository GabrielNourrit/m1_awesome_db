package models;

import java.sql.SQLException;

import services.TirageService;

public class Lk_CodePromo_Commande {
	
	private CodePromo code_promo;
	private Commande id_commande;
	
	
	public Lk_CodePromo_Commande() {
	}
	
	public Lk_CodePromo_Commande(CodePromo code_promo, Commande commande) {
		this.code_promo = code_promo;
		this.id_commande = commande;
	}
	
	public CodePromo getCode_promo() {
		return code_promo;
	}
	
	public void setCode_promo(String code) throws SQLException {
		TirageService t = new TirageService();
		this.code_promo = (CodePromo) t.findById(CodePromo.class, code);
		//this.code_promo = code_promo;
	}
	
	
	public Commande getId_commande() {
		return id_commande;
	}
	public void setId_commande(Number id) throws SQLException {
		TirageService t = new TirageService();
		this.id_commande = (Commande) t.findById(Commande.class, id);
		//this.commande = commande;
	}
	public void setId_commande(Commande cmd) throws SQLException {
		this.id_commande = cmd;
	}
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Lk_CodePromo_Commande [codePromo="+this.code_promo.getCode_promo()+",commande="+this.id_commande.getId_commande()+"]";
	}
	
	public boolean equals(Object o) {
		if (o instanceof Lk_CodePromo_Commande) {
			Lk_CodePromo_Commande lk = (Lk_CodePromo_Commande) o;
			if (code_promo.equals(lk.getCode_promo()) && id_commande.equals(lk.getId_commande())) return true;
		}
		return false;
	}
}
