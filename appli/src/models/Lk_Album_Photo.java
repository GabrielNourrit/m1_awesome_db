package models;

import java.sql.SQLException;

import services.TirageService;

public class Lk_Album_Photo implements LkImpressionnable {
	private Album id_album;
	private Photo id_photo;
	private String description;
	private Number ordre;
	private Number page;


	public Lk_Album_Photo(Album album, Photo photo, String description, int ordre, int page) {
		this.id_album = album;
		this.id_photo = photo;
		this.description = description;
		this.ordre = ordre;
		this.page = page;
	}
	public Lk_Album_Photo() {
	}


	public Album getId_album() {
		return id_album;
	}

	public void setId_album(Number id) throws SQLException {
		TirageService t = new TirageService();
		this.id_album = (Album) t.findById(Album.class, id);
	}
	public void setId_album(Album id) {
		this.id_album = id;
	}

	public Photo getId_photo() {
		return id_photo;
	}
	public void setId_photo(Number id) throws SQLException {
		TirageService t = new TirageService();
		this.id_photo = (Photo) t.findById(Photo.class, id);
	}
	public void setId_photo(Photo id) {
		this.id_photo = id;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}


	public Number getOrdre() {
		return ordre;
	}
	public void setOrdre(Number ordre) {
		this.ordre = ordre;
	}


	public Number getPage() {
		return page;
	}
	public void setPage(Number page) {
		this.page = page;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Lk_Album_Photo [Album="+this.id_album.getId_album()+",photo="+this.id_photo.getId_photo()+",description="+this.description+",ordre="+this.ordre+",page="+this.page+"]";
	}

	public boolean equals(Object o) {
		if (o instanceof Lk_Album_Photo) {
			Lk_Album_Photo lk = (Lk_Album_Photo) o;
			if (id_album.equals(lk.getId_album()) && id_photo.equals(lk.getId_photo())) return true;
		}
		return false;
	}

	@Override
	public void setId(Object o) {
		this.setId_album((Album) o);
	}
}
