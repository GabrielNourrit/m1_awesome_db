package models;

import java.sql.SQLException;

import services.TirageService;

public class Lk_Tirage_Photo implements LkImpressionnable {
	private Tirage id_tirage;
	private Photo id_photo;
	private Number quantite;


	public Lk_Tirage_Photo(Tirage tirage, Photo photo, int quantite) {
		this.id_tirage = tirage;
		this.id_photo = photo;
		this.quantite = quantite;
	}
	public Lk_Tirage_Photo() {
	}

	public Tirage getId_tirage() {
		return id_tirage;
	}
	public void setId_tirage(Number id) throws SQLException {
		TirageService t = new TirageService();
		this.id_tirage = (Tirage) t.findById(Tirage.class, id);
	}
	public void setId_tirage(Tirage id) {
		this.id_tirage = id;
	}


	public Photo getId_photo() {
		return id_photo;
	}
	public void setId_photo(Number id) throws SQLException {
		TirageService t = new TirageService();
		this.id_photo = (Photo) t.findById(Photo.class, id);
	}
	public void setId_photo(Photo id) {
		this.id_photo = id;
	}


	public Number getQuantite() {
		return quantite;
	}
	public void setQuantite(Number quantite) {
		this.quantite = quantite;
	}

	public boolean equals(Object o) {
		if (o instanceof Lk_Tirage_Photo) {
			Lk_Tirage_Photo lk = (Lk_Tirage_Photo) o;
			if (id_tirage.equals(lk.getId_tirage()) && id_photo.equals(lk.getId_photo())) return true;
		}
		return false;
	}

	@Override
	public void setId(Object o) {
		this.setId_tirage((Tirage) o);
	}
}
