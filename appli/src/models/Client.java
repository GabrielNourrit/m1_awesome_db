package models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import services.ActiveState;
import services.ConnectionJDBC;
import services.IdentityPrimary;
import services.IgnoreQueryProperty;
import services.TirageService;
import type.clientEtat;

public class Client {
	
	@IdentityPrimary("")
	private String mail_client;
	private String nom;
	private String prenom;
	private String adresse;
	@ActiveState
	private clientEtat etat;
	private String password;
	private int est_connecte;
	@IgnoreQueryProperty
	//private List<AdresseLivraison> adresses_livraison;
	//TODO :: link table
	private LK lk_adresseLivraison_client;
	@IgnoreQueryProperty
	private List<FichierImage> fichiersImage;
	@IgnoreQueryProperty
	// Liste des photos proprietaire du client
	private List<Photo> photos_proprietaire;
	@IgnoreQueryProperty
	// Liste des commandes pass�es par le client
	private List<Commande> commandes_associees;
	@IgnoreQueryProperty
	// Liste des codes promo du client
	private List<CodePromo> codePromo_associes;
	@IgnoreQueryProperty
	private List<AdresseLivraison> listAdresseLivraison;
	
	/**
	 * Ajoute une adresse de livraison
	 * @param adresse
	 */
	public void ajouteAdresseLivraison(AdresseLivraison a) {
		this.lk_adresseLivraison_client.ajouter_Lk_AdresseLivraison_Client(a, this);;
	}
	/**
	 * @return the adresses_livraison
	 */
	public List<AdresseLivraison> getAdresses_livraison() {
		List<AdresseLivraison> l = new ArrayList<AdresseLivraison>();
		List<Lk_AdresseLivraison_Client> ll = lk_adresseLivraison_client.get_Lk_AdresseLivraison_Client_byClient(this);
		for (Lk_AdresseLivraison_Client lk : ll) l.add(lk.getId_adresse_livraison());
		return l;
	}

	/**
	 * @return the mail_client
	 */
	public String getMail_client() {
		return mail_client;
	}
	/**
	 * @param mail_client the mail_client to set
	 */
	public void setMail_client(String mail_client) {
		this.mail_client = mail_client;

	}
	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return the prenom
	 */
	public String getPrenom() {
		return prenom;
	}
	/**
	 * @param prenom the prenom to set
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	/**
	 * @return the adresse
	 */
	public String getAdresse() {
		return adresse;
	}
	/**
	 * @param adresse the adresse to set
	 */
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	public List<FichierImage> getFichiersImage() {
		return fichiersImage;
	}
	public void setFichiersImage(List<FichierImage> fichiersImage) {
		this.fichiersImage = fichiersImage;
	}
	public List<Photo> getPhotos_proprietaire() {
		return photos_proprietaire;
	}
	public void setPhotos_proprietaire(List<Photo> photos_proprietaire) {
		this.photos_proprietaire = photos_proprietaire;
	}
	/*public List<Commande> getCommandes_associe() {
		TirageService t = new TirageService();
		// On recupere la liste des photo
		Commande exampleCommande = new Commande();
		try {
			exampleCommande.setMail_client(mail_client);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		this.commandes_associe = t.findByExample(exampleCommande);
		return commandes_associe;
	}
	public void setCommandes_associe(List<Commande> commandes_associe) {
		this.commandes_associe = commandes_associe;
	}*/
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Client [mail_client=" + mail_client + ", nom=" + nom + ", prenom=" + prenom + ", adresse=" + adresse
				+ ", password=" + password + "]";
	}
	public clientEtat getEtat() {
		return etat;
	}
	public void setEtat(clientEtat etat) {
		this.etat = etat;
	}
	public void setEtat(String etat) {
		if (etat.toUpperCase().equals("ACTIF")) this.etat = clientEtat.Actif;
		if (etat.toUpperCase().equals("SUPPRIME")) this.etat = clientEtat.Supprime;
	}
	public boolean equals(Object o) {
		if (mail_client == ((Client) o).getMail_client()) return true;
		return false;
	}
	
	public List<CodePromo> getCodePromoClient() {
		return codePromo_associes;
	}
	
	public void loadCodePromo() {
		TirageService t = new TirageService();
		List<Object> l = new ArrayList<Object>();
		List<CodePromo> ret = new ArrayList<CodePromo>();
		Connection connection = ConnectionJDBC.getConnection();
		try {
			connection.setAutoCommit(false);
			connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		PreparedStatement stmt = null;
		try {
			stmt = connection.prepareStatement("select * from codepromo where nombre_utilisation=0 and id_commande in (select id_commande from commande where mail_client='jules@jules.com')");
			ResultSet rs = stmt.executeQuery();
			l = t.resultsetToListObject(rs, CodePromo.class);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		for (Object o : l) if (o instanceof CodePromo) ret.add((CodePromo) o);
		codePromo_associes = ret;
	}
	
	public List<Commande> getCommande() {
		return this.commandes_associees;
	}
	
	
	/**
	 * Methode qui recupere les fichiers du client
	 * @return
	 */
	public List<FichierImage> loadFichiers() {
		TirageService t = new TirageService();
		FichierImage f = new FichierImage();
		Client c = new Client();
		c.setMail_client(this.mail_client);
		f.setMail_client(c);
		f.setEst_actif(1);
		List<FichierImage> liste =  t.findByExample(f);
		this.fichiersImage = liste;
		return this.fichiersImage;
	}
	
	/**
	 * Methode qui recupere les photos du client
	 * @return
	 */
	public List<Photo> loadPhoto() {
		TirageService t = new TirageService();
		Photo f = new Photo();
		f.setMail_client(this.mail_client);
		f.setEtat("actif");
		List<Photo> liste =  t.findByExample(f);
		this.photos_proprietaire = liste;
		return this.photos_proprietaire;
	}
	
	
	public void loadCommande() {
		TirageService t = new TirageService();
		Commande cmd = new Commande();
		cmd.setMail_client(this);
		this.commandes_associees = t.findByExample(cmd, Connection.TRANSACTION_READ_COMMITTED);
		System.out.println(this.commandes_associees);
	}
	
	public int getEst_connecte() {
		return est_connecte;
	}
	public void setEst_connecte(Number estConnecte) {
		this.est_connecte = estConnecte.intValue();
	}
	
	public void loadAllAdresse() {
		TirageService t = new TirageService();
		List<AdresseLivraison> ret = new ArrayList<AdresseLivraison>();
		Lk_AdresseLivraison_Client tmp = new Lk_AdresseLivraison_Client();
		tmp.setMail_client(this);
		List<Lk_AdresseLivraison_Client> lks = t.findByExample(tmp, Connection.TRANSACTION_READ_COMMITTED);
		for (Lk_AdresseLivraison_Client lk : lks) ret.add(lk.getId_adresse_livraison());
		listAdresseLivraison = ret;
	}
	
	public List<AdresseLivraison> getAllAdresse() {
		return listAdresseLivraison;
	}
}
