package models;

import java.sql.SQLException;

import services.TirageService;

public class Lk_Calendrier_Photo implements LkImpressionnable {

	private Calendrier id_calendrier;
	private Photo id_photo;
	private Number ordre;
	private Number page;
	
	public Lk_Calendrier_Photo(Calendrier calendrier, Photo photo, int ordre, int page) {
		this.id_calendrier = calendrier;
		this.id_photo = photo;
		this.ordre = ordre;
		this.page = page;
	}
	public Lk_Calendrier_Photo() {
	}
	
	public Calendrier getId_calendrier() {
		return id_calendrier;
	}
	public void setId_calendrier(Number id) throws SQLException {
		TirageService t = new TirageService();
		this.id_calendrier = (Calendrier) t.findById(Calendrier.class, id);
	}
	public void setId_calendrier(Calendrier id) {
		this.id_calendrier = id;
	}
	
	public Photo getId_photo() {
		return id_photo;
	}
	public void setId_photo(Number id) throws SQLException {
		TirageService t = new TirageService();
		this.id_photo = (Photo) t.findById(Photo.class, id);
	}
	public void setId_photo(Photo id) {
		this.id_photo = id;
	}
	
	public Number getOrdre() {
		return ordre;
	}
	public void setOrdre(Number ordre) {
		this.ordre = ordre;
	}
	
	
	public Number getPage() {
		return page;
	}
	public void setPage(Number page) {
		this.page = page;
	}
	
	public boolean equals(Object o) {
		if (o instanceof Lk_Calendrier_Photo) {
			Lk_Calendrier_Photo lk = (Lk_Calendrier_Photo) o;
			if (id_calendrier.equals(lk.getId_calendrier()) && id_photo.equals(lk.getId_photo())) return true;
		}
		return false;
	}
	@Override
	public String toString() {
		return "Lk_Calendrier_Photo [id_calendrier=" + id_calendrier + ", id_photo=" + id_photo + ", ordre=" + ordre
				+ ", page=" + page + "]";
	}
	
	@Override
	public void setId(Object o) {
		this.setId_calendrier((Calendrier) o);
	}
	
}
