package models;

import java.util.ArrayList;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import client.Main;
import javafx.beans.property.SimpleStringProperty;
import services.ActiveState;
import services.IdentityPrimary;
import services.IgnoreQueryProperty;
import services.TirageService;
import type.statutCommande;
import util.ObjectPanier;
import type.modeLivraison;

public class Commande {
	
	@IdentityPrimary("commande_id")
	private Number id_commande;
	private Client mail_client;
	private Timestamp date_commande;
	private modeLivraison mode_livraison;
	private String code_point_relais;
	private Number prix_tt;
	private Number prix_tt_remise;
	@ActiveState
	private statutCommande statut;
	@IgnoreQueryProperty
	private List<ObjectPanier> obj;
	@IgnoreQueryProperty
	private List<Article> lart = new ArrayList<Article>();
	/**
	 * Les codes promos utilis� par la commande
	 */
	@IgnoreQueryProperty
	private LK lk_codePromo_commande = LK.getInstance();
	
	@IgnoreQueryProperty
	private List<Article> articles;
	
	private AdresseLivraison id_adresse_livraison;
	
	
	@IgnoreQueryProperty
	private SimpleStringProperty date;
	@IgnoreQueryProperty
    private SimpleStringProperty mode;
	@IgnoreQueryProperty
    private SimpleStringProperty prix;
	@IgnoreQueryProperty
    private SimpleStringProperty statutColumn;

	
	
	
	public Commande() {
		this.articles = new ArrayList<Article>();
		this.date = new SimpleStringProperty();
		this.mode = new SimpleStringProperty();
		this.prix = new SimpleStringProperty();
		this.statutColumn = new SimpleStringProperty();
	}
	
	/**
	 * Ajoute un article � la commande
	 * @param article
	 */
	public void ajoutArticle(Article article) {
		this.lart.add(article);
	}
	

	/**
	 * Ajoute un code promo � la commande
	 * @param code
	 */
	public void ajoutCodePromo(CodePromo code) {
		//this.code_promo_utilises.add(code);
		this.lk_codePromo_commande.ajouter_Lk_CodePromo_Commande(code, this);
	}

	
	/**
	 * @return the id_adresse_livraison
	 */
	public AdresseLivraison getId_adresse_livraison() {
		return id_adresse_livraison;
	}

	/**
	 * @param mode_livraison the mode_livraison to set
	 */
	public void setMode_livraison(modeLivraison mode_livraison) {
		this.mode_livraison = mode_livraison;
	}

	/**
	 * @param statut the statut to set
	 */
	public void setStatut(statutCommande statut) {
		this.statut = statut;
	}

	/**
	 * @return the id_commande
	 */
	public Number getId_commande() {
		return id_commande;
	}

	/**
	 * @param id_commande the id_commande to set
	 */
	public void setId_commande(Number id_commande) {
		this.id_commande = id_commande;	
	}


	/**
	 * @param client the client to set
	 */
	public void setMail_client(Client client) {
		this.mail_client = client;
	}

	/**
	 * @return the date_commande
	 */
	public Timestamp getDate_commande() {
		return date_commande;
	}

	/**
	 * @param date_commande the date_commande to set
	 */
	public void setDate_commande(Timestamp date_commande) {
		this.date_commande = date_commande;
		this.date.setValue(this.date_commande.toString());
	}

	/**
	 * @return the mode_livraison
	 */
	public modeLivraison getMode_livraison() {
		return mode_livraison;
	}

	/**
	 * @param mode_livraison the mode_livraison to set
	 */
	public void setMode_livraison(String mode_livraison) {
		this.mode_livraison = modeLivraison.valueOf(mode_livraison);
		this.mode.setValue(this.mode_livraison.name());
	}

	/**
	 * @return the code_point_relais
	 */
	public String getCode_point_relais() {
		return code_point_relais;
	}

	/**
	 * @param code_point_relais the code_point_relais to set
	 */
	public void setCode_point_relais(String code_point_relais) {
		this.code_point_relais = code_point_relais;
	}

	/**
	 * @return the prix_tt
	 */
	public Number getPrix_tt() {
		return prix_tt;
	}

	/**
	 * @param prix_tt the prix_tt to set
	 */
	public void setPrix_tt(Number prix_tt) {
		this.prix_tt = prix_tt;
		this.prix.setValue(this.prix_tt.toString());
	}

	/**
	 * @return the prix_tt_remise
	 */
	public Number getPrix_tt_remise() {
		return prix_tt_remise;
	}

	/**
	 * @param prix_tt_remise the prix_tt_remise to set
	 */
	public void setPrix_tt_remise(Number prix_tt_remise) {
		this.prix_tt_remise = prix_tt_remise;
	}

	/**
	 * @return the statut
	 */
	public statutCommande getStatut() {
		return statut;
	}

	/**
	 * @param statut the statut to set
	 */
	public void setStatut(String statut) {
		this.statut =  statutCommande.valueOf(statut);
		this.statutColumn.setValue(this.statut.name());
	}


	/**
	 * @return the articles
	 */
	public List<Article> getArticles() {
		return articles;
	}

	/**
	 * @param articles the articles to set
	 */
	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}

	/**
	 * @return the adresse_livraison
	 */
	public AdresseLivraison getAdresse_livraison() {
		return id_adresse_livraison;
	}

	/**
	 * @param adresse_livraison the adresse_livraison to set
	 */
	public void setAdresse_livraison(AdresseLivraison adresse_livraison) {
		this.id_adresse_livraison = adresse_livraison;
	}
	
	/**
	 * @param adresse_livraison the adresse_livraison to set
	 */
	public void setId_adresse_livraison(AdresseLivraison adresse_livraison) {
		this.id_adresse_livraison = adresse_livraison;
	}

	public Client getMail_client() {
		return mail_client;
	}
	
	public void setMail_client(String mail) throws SQLException {
		TirageService service = new TirageService();
		this.mail_client = (Client) service.findById(Client.class, mail);
	}
	

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Commande [\n        id_commande=" + id_commande + ", \n        client=" + mail_client + ", \n        date_commande=" + date_commande
				+ ", \n        mode_livraison=" + mode_livraison + ", \n        code_point_relais=" + code_point_relais + ", \n        prix_tt="
				+ prix_tt + ", \n        prix_tt_remise=" + prix_tt_remise + ", \n        statut=" + statut + ", \n        articles= " + lart + ", \n        adresse_livraison=" + id_adresse_livraison + "]";
	}
	
	
	public boolean equals(Object o) {
		if (id_commande == ((Commande) o).getId_commande()) return true;
		return false;
	}
	
	public List<CodePromo> getAllCodePromo() {
		List<Lk_CodePromo_Commande> l = lk_codePromo_commande.get_Lk_CodePromo_Commande_byCommande(this);
		List<CodePromo> ret = new ArrayList<CodePromo>();
		for (Lk_CodePromo_Commande lk : l) ret.add(lk.getCode_promo());
		return ret;
	}
	
	public LK getLk_codePromo_commande() {
		return lk_codePromo_commande;
	}

	public void setLk_codePromo_commande(LK lk_codePromo_commande) {
		this.lk_codePromo_commande = lk_codePromo_commande;
	}

	public String getDate() {
		return date.get();
	}

	public void setDate(SimpleStringProperty date) {
		this.date = date;
	}

	public String getPrix() {
		return prix.get();
	}

	public void setPrix(SimpleStringProperty prix) {
		this.prix = prix;
	}

	public String getMode() {
		return mode.get();
	}

	public void setMode(SimpleStringProperty mode) {
		this.mode = mode;
	}

	public String getStatutColumn() {
		return statutColumn.get();
	}

	public void setStatutColumn(SimpleStringProperty statutColumn) {
		this.statutColumn = statutColumn;
	}
	
	
	
	
	
	public List<ObjectPanier> getObjectPanier() {
		if (obj == null) return new ArrayList<ObjectPanier>();
		return obj;
	}
	
	public void loadObjectPanier() {
		List<ObjectPanier> l = new ArrayList<ObjectPanier>();
		TirageService t = new TirageService();
		Article aa = new Article();
		if (Main.getCommandeEnCours().getId_commande() != null) {
			aa.setId_commande(Main.getCommandeEnCours().getId_commande());
			List<Article> la = t.findByExample(aa, Connection.TRANSACTION_READ_COMMITTED);
			for (Article art : la) {
				ObjectPanier i = art.getObjectPanier();
				l.add(i);
			}
			obj=l;
		}
		
	}
	
	public void loadArticle() {
		TirageService t = new TirageService();
		Article aa = new Article();
		if (this.getId_commande() != null) {
			aa.setId_commande(this.getId_commande());
			List<Article> la = t.findByExample(aa, Connection.TRANSACTION_READ_COMMITTED);
			for (Article art : la) {
				Article i = art.getTypeArticle();
				lart.add(i);
			}
		}
	}
	

	
	public List<Article> getArticle() {
		return lart;
	}
	
	
	private String lartToString() {
		String ret = "";
		int i = 0;
		int size = lart.size();
		for (Article a : lart) {
			if(i==size) ret+="                 "+a.toString(); 
			else ret+=a.toString() + "\n";
			i++;
		}
		return ret;
	}
	
	public String toStringDetails() {
		String s = "";
		for (Article a : lart) {
			if (a instanceof Album) s+=((Album) a).toStringDetails()+"\n";
			else if (a instanceof Agenda) s+=((Agenda) a).toStringDetails()+"\n";
			else if (a instanceof Cadre) s+=((Cadre) a).toStringDetails()+"\n";
			else if (a instanceof Calendrier) s+=((Calendrier) a).toStringDetails()+"\n";
			else if (a instanceof Tirage) s+=((Tirage) a).toStringDetails()+"\n";
		}
		
		return s;
	}
}
