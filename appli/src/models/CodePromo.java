package models;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import services.IdentityPrimary;
import services.IgnoreQueryProperty;

public class CodePromo {
	
	@IdentityPrimary("")
	private String code_promo;
	private Number remise;
	private Number nombre_utilisation;
	private Number est_public;
	private Timestamp date_fin_code;
	private Commande id_commande;
	// La commande qui a gener� le code promo
	@IgnoreQueryProperty
	private Commande commande_creatrice_du_codePromo;

	@IgnoreQueryProperty
	//private List<Commande> historique_commande_codePromo;
	private LK lk_codePromo_commande;
	
	
	public CodePromo createCodePromo() {
		return new CodePromo();
	}
	
	/**
	 * @return the code_promo
	 */
	public String getCode_promo() {
		return code_promo;
	}
	/**
	 * @param code_promo the code_promo to set
	 */
	public void setCode_promo(String code_promo) {
		this.code_promo = code_promo;
	}
	
	/**
	 * @return the remise
	 */
	public Number getRemise() {
		return remise;
	}
	/**
	 * @param remise the remise to set
	 */
	public void setRemise(Number remise) {
		this.remise = remise;
	}
	/**
	 * @return the nombre_utilisation
	 */
	public Number getNombre_utilisation() {
		return nombre_utilisation;
	}
	/**
	 * @param nombre_utilisation the nombre_utilisation to set
	 */
	public void setNombre_utilisation(Number nombre_utilisation) {
		this.nombre_utilisation = nombre_utilisation;
	}
	/**
	 * @return the est_public
	 */
	public boolean isEst_public() {
		return est_public.intValue()==1?true:false;
	}
	
	/**
	 * @return the est_public
	 */
	public Number getEst_public() {
		return est_public;
	}	
	
	public void setEst_public(Number est_public) {
		this.est_public = est_public;
	}
	/**
	 * @return the date_fin_code
	 */
	public Timestamp getDate_fin_code() {
		return date_fin_code;
	}
	/**
	 * @param date_fin_code the date_fin_code to set
	 */
	public void setDate_fin_code(Timestamp date_fin_code) {
		this.date_fin_code = date_fin_code;
	}
	public Commande getCommande_creatrice_du_codePromo() {
		return commande_creatrice_du_codePromo;
	}
	public void setCommande_creatrice_du_codePromo(Commande commande_creatrice_du_codePromo) {
		this.id_commande = commande_creatrice_du_codePromo;
		//this.commande_creatrice_du_codePromo = commande_creatrice_du_codePromo;
	}
	/*public List<Commande> getHistorique_commande_codePromo() {
		return historique_commande_codePromo;
	}
	public void setHistorique_commande_codePromo(List<Commande> historique_commande_codePromo) {
		this.historique_commande_codePromo = historique_commande_codePromo;
	}*/
	
	
	public Commande getId_commande() {
		return id_commande;
	}
	public void setId_commande(Commande id_commande) {
		this.id_commande = id_commande;
	}
	
	
	public LK getLk_codePromo_commande() {
		return lk_codePromo_commande;
	}
	public void setLk_codePromo_commande(LK lk_codePromo_commande) {
		this.lk_codePromo_commande = lk_codePromo_commande;
	}
	
	public boolean equals(Object o) {
		if (code_promo == ((CodePromo) o).getCode_promo()) return true;
		return false;
	}
	
	public List<Commande> getAllCommande() {
		List<Lk_CodePromo_Commande> l = lk_codePromo_commande.get_Lk_CodePromo_Commande_byCodePromo(this);
		List<Commande> ret = new ArrayList<Commande>();
		for (Lk_CodePromo_Commande lk : l) ret.add(lk.getId_commande());
		return ret;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CodePromo [code_promo=" + code_promo + ", remise=" + remise + ", commande=" + id_commande + "]";
	}
	
}
