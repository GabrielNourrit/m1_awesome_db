package models;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import services.IdentityPrimary;
import services.IgnoreQueryProperty;
import services.TirageService;

public class Tirage extends Impression{
	
	@IdentityPrimary("")
	private Number id_tirage;
	private String qualite;
	private String format;
	@IgnoreQueryProperty
	private LK lk_tirage_photo;
	/**
	 * @return the id_tirage
	 */
	public Number getId_tirage() {
		return id_tirage;
	}
	/**
	 * @param id_tirage the id_tirage to set
	 */
	public void setId_tirage(Number id_tirage) {
		this.id_tirage = id_tirage;
	}
	/**
	 * @return the qualite
	 */
	public String getQualite() {
		return qualite;
	}
	/**
	 * @param qualite the qualite to set
	 */
	public void setQualite(String qualite) {
		this.qualite = qualite;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}

	
	public List<Photo> getAllPhoto() {
		List<Lk_Tirage_Photo> l = lk_tirage_photo.get_Lk_Tirage_Photo_byTirage(this);
		List<Photo> ret = new ArrayList<Photo>();
		for (Lk_Tirage_Photo lk : l) ret.add(lk.getId_photo());
		return ret;
	}
	
	@Override
	public boolean equals(Object o) {
		if (id_tirage == ((Tirage) o).getId_tirage()) return true;
		return false;
	}
	
	@Override
	public String toString() {
		return "Tirage [id_tirage=" + id_tirage + ", qualite=" + qualite + ", format=" + format + "]";
	}
	
	public String customToString() {
		return "Tirage [id_tirage=" + id_tirage + ", qualite=" + qualite + ", format=" + format + "]";
	}
	
	public String toStringDetails() {
		TirageService t = new TirageService();
		List<Photo> lp = new ArrayList<Photo>();
		Lk_Tirage_Photo cp = new Lk_Tirage_Photo();
		List<Lk_Tirage_Photo> lks = t.findLKById(this.getClass(), this.id_tirage, Lk_Tirage_Photo.class, Connection.TRANSACTION_READ_COMMITTED);
		for (Lk_Tirage_Photo lk : lks) lp.add(lk.getId_photo());
		return toString() + "\n" + lp;
	}
}
