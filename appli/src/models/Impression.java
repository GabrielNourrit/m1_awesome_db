package models;

import services.IdentityPrimary;

public class Impression extends Article {
	
	@IdentityPrimary("impression_id")
	private Number id_impression;
	private Number pu;
	private boolean estPromo;
	private Number prix_promo;
	
	
	public Number getEstPromo() {
		if (estPromo) return 1;
		else return 0;
	}
	
	
	
	/**
	 * @return the id_impression
	 */
	public Number getId_impression() {
		return id_impression;
	}
	/**
	 * @param id_impression the id_impression to set
	 */
	public void setId_impression(Number id_impression) {
		this.id_impression = id_impression;
	}
	/**
	 * @return the pU
	 */
	public Number getPu() {
		return pu;
	}
	/**
	 * @param pU the pU to set
	 */
	public void setPu(Number pU) {
		pu = pU;
	}
	/**
	 * @return the estPromo
	 */
	public boolean isEstPromo() {
		return estPromo;
	}
	/**
	 * @param estPromo the estPromo to set
	 */
	public void setEstpromo(Number estPromo) {
		this.estPromo = estPromo.intValue()==1?true:false;
	}
	/**
	 * @return the prixPromo
	 */
	public Number getPrix_promo() {
		return prix_promo;
	}
	/**
	 * @param prixPromo the prixPromo to set
	 */
	public void setPrix_promo(Number prixPromo) {
		prix_promo = prixPromo;
	}	
	
	public boolean equals(Object o) {
		if (id_impression == ((Impression) o).getId_impression()) return true;;
		return false;
	}
}
