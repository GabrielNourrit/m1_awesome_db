package models;

import java.util.ArrayList;
import java.util.List;

import services.IdentityPrimary;
import services.IgnoreQueryProperty;

public class Photo {
	
	@IdentityPrimary("photo_id")
	private Number id_photo;
	
	private String retouches;
	
	private FichierImage id_fic;
	
	private String mail_client;
	
	private String etat;
	
	@IgnoreQueryProperty
	private LK lk;

	@IgnoreQueryProperty
	private List<Lk_Album_Photo> lk_album_photo;

	@IgnoreQueryProperty
	private List<Lk_Cadre_Photo> lk_cadre_photo;

	@IgnoreQueryProperty
	private List<Lk_Calendrier_Photo> lk_calendrier_photo;

	@IgnoreQueryProperty
	private List<Lk_Tirage_Photo> lk_tirage_photo;
	
	
	/**
	 * @return the id_photo
	 */
	public Number getId_photo() {
		return id_photo;
	}
	/**
	 * @param id_photo the id_photo to set
	 */
	public void setId_photo(Number id_photo) {
		this.id_photo = id_photo;
	}
	/**
	 * @return the retouches
	 */
	public String getRetouches() {
		return retouches;
	}
	/**
	 * @param retouches the retouches to set
	 */
	public void setRetouches(String retouches) {
		this.retouches = retouches;
	}
	public FichierImage getId_fic() {
		return id_fic;
	}
	
	public void setId_fic(FichierImage fichierImage) {
		this.id_fic = fichierImage;
	}
	
	public void setId_fic(Number id) {
		this.id_fic = new FichierImage();
		this.id_fic.setId_fic(id);
	}
	/**
	 * @return the mail_client
	 */
	public String getMail_client() {
		return mail_client;
	}
	/**
	 * @param mail_client the mail_client to set
	 */
	public void setMail_client(String mail_client) {
		this.mail_client = mail_client;
	}
	
	public boolean equals(Object o) {
		if(o == null) return false;
		if(this.id_photo == null) return false;
		if (id_photo == ((Photo) o).getId_photo()) return true;
		return false;
	}
	
	
	public List<Agenda> getAllAgenda() {
		List<Lk_Agenda_Photo> l = lk.get_Lk_Agenda_Photo_byPhoto(this);
		List<Agenda> ret = new ArrayList<Agenda>();
		for (Lk_Agenda_Photo lk : l) ret.add(lk.getId_agenda());
		return ret;
	}
	
	public List<Album> getAllAlbum() {
		List<Lk_Album_Photo> l = lk.get_Lk_Album_Photo_byPhoto(this);
		List<Album> ret = new ArrayList<Album>();
		for (Lk_Album_Photo lk : l) ret.add(lk.getId_album());
		return ret;
	}
	
	public List<Album> getAllCadre() {
		List<Lk_Album_Photo> l = lk.get_Lk_Album_Photo_byPhoto(this);
		List<Album> ret = new ArrayList<Album>();
		for (Lk_Album_Photo lk : l) ret.add(lk.getId_album());
		return ret;
	}
	
	public List<Calendrier> getAllCalendrier() {
		List<Lk_Calendrier_Photo> l = lk.get_Lk_Calendrier_Photo_byPhoto(this);
		List<Calendrier> ret = new ArrayList<Calendrier>();
		for (Lk_Calendrier_Photo lk : l) ret.add(lk.getId_calendrier());
		return ret;
	}
	
	public List<Tirage> getAllTirage() {
		List<Lk_Tirage_Photo> l = lk.get_Lk_Tirage_Photo_byPhoto(this);
		List<Tirage> ret = new ArrayList<Tirage>();
		for (Lk_Tirage_Photo lk : l) ret.add(lk.getId_tirage());
		return ret;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Photo " + id_photo + ", retouches "+retouches;
	}
	public String getEtat() {
		return etat;
	}
	public void setEtat(String etat) {
		this.etat = etat;
	}
}
