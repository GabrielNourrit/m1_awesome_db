package services;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import models.Article;
import models.Client;
import models.CodePromo;
import models.Commande;
import models.FichierImage;
import models.LK;
import models.Models;
import type.agendaModel;
import type.calendrierModel;
import type.clientEtat;
import type.etatFichierImage;
import type.modeLivraison;
import type.statutCommande;
import util.Contrainte;


public class TirageService {

	/**
	 * Recupere la liste de tous les objets d'une classe
	 * @param c
	 * @param type
	 * @return
	 * @throws SQLException
	 */
	public <T> List<T> getAll(Class<T> c, int... type) throws SQLException {
		int connectionLevelIsolation = Connection.TRANSACTION_SERIALIZABLE;
		if(type.length != 0) connectionLevelIsolation = type[0];
		Connection connection = ConnectionJDBC.getConnection();
		try {
			connection.setAutoCommit(false);
			connection.setTransactionIsolation(connectionLevelIsolation);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		Statement conn = connection.createStatement();
		String query = "SELECT * FROM "+c.getSimpleName();
		List<T> list = null;
		try {
			ResultSet rs = conn.executeQuery(query);
			list = (List<T>) resultsetToListObject(rs, c);
			for (T o : list) getLK(o, connectionLevelIsolation);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		connection.commit();
		conn.close();
		connection.close();
		return list;
	}

	/**
	 * Methode permettant de récupérer toutes les tables LK associées à un objet
	 * @param obj
	 * @throws SQLException
	 */
	public <T> void getLK(Object obj, int... type) {
		int connectionLevelIsolation = Connection.TRANSACTION_SERIALIZABLE;
		if(type.length != 0) connectionLevelIsolation = type[0];
		String methodName, p1, p2;
		Object attributLK = null;
		/*--- DEBUT : récupération des tables de lien à remplir ---*/
		methodName = "get" + obj.getClass().getSimpleName() + "AttributLK";
		try {
			attributLK = Models.class.getMethod(methodName).invoke(null);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}
		Class<?>[] alk = (Class<?>[]) attributLK;
		/*--- FIN : récupération des tables de lien à remplir ---*/
		LK l = LK.getInstance();
		//Pour chaque table de lien
		for (Class<?> tableAssociate : alk) {	
			Object o1 = null;
			try {	
				/*--- DEBUT : récupération de l'id de la classe ---*/
				String id = this.getAttributeIdentityPrimary(obj.getClass());
				p1 = id.toString().substring(0, 1).toUpperCase();
				p2 = id.toString().substring(1);			   
				methodName = "get"+p1+p2;
				try {
					o1 = obj.getClass().getMethod(methodName).invoke(obj);
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
						| NoSuchMethodException | SecurityException e) {
					e.printStackTrace();
				}
				/*--- FIN : récupération de l'id de la classe ---*/
				if (o1 != null) {
					/*--- DEBUT : ajout des tuples de lk dans la classe LK ---*/
					List<Object> lk = (List<Object>) this.findLKById(obj.getClass(), o1, tableAssociate, connectionLevelIsolation);			   
					methodName = "ajouter_"+tableAssociate.getSimpleName();
					try {
						for (Object lkk : lk) {
							java.lang.reflect.Method method = l.getClass().getMethod(methodName, lkk.getClass());
							method.invoke(l, lkk);
						}
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
							| SecurityException e) {
						e.printStackTrace();
					}
					/*--- FIN : ajout des tuples de lk dans la classe LK ---*/
				}
			} catch (IllegalArgumentException | NoSuchMethodException | SecurityException e) {
				e.printStackTrace();
			}
		}
	}



	/**
	 * Recupere le nom des foreing key d'une classe
	 * @param c
	 * @return
	 */
	public List<String> getFKFields(Class<?> c) {
		String methodName, p1, p2;
		Object attributFK = null;
		//Class<?> tableAssociate = null;
		/*--- DEBUT : r�cup�ration des tables de lien � remplir ---*/
		methodName = "get" + c.getSimpleName() + "AttributFK";
		try {
			attributFK = Models.class.getMethod(methodName).invoke(null);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}
		String[] afk = (String[]) attributFK;
		return Arrays.asList(afk);
	}




	/**
	 * Converti un result set en List d'objet java
	 * @param rs
	 * @param c
	 * @return
	 */
	public List<Object> resultsetToListObject(ResultSet rs, Class<?> c) {
		List<Object> list = new ArrayList<Object>();
		ResultSetMetaData resMeta = null;
		int numberOfColumn=0;
		List<String> tabFK;
		//Si on est dans le cas d'une table LK -> on ignore
		if (c.getSimpleName().toUpperCase().contains("LK_")) tabFK = new ArrayList<String>();
		else tabFK = getFKFields(c);
		try {resMeta = rs.getMetaData();} catch (SQLException e2) {e2.printStackTrace();}
		try {
			numberOfColumn = resMeta.getColumnCount();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			Object o = null;
			try {
				o = c.newInstance();
			} catch (InstantiationException | IllegalAccessException e2) {
				e2.printStackTrace();
			}
			while( rs.next() ) {
				for(int i=1; i<=numberOfColumn;i++) {

					String field = resMeta.getColumnName(i).toLowerCase();
					String primaryAttribute = this.getAttributeIdentityPrimary(c);

					// Recupere le setter du field
					String p1 = field.substring(0, 1).toUpperCase();
					String p2 = field.substring(1);			   
					String methodName = "set"+p1+p2;
					java.lang.reflect.Method method = null;
					try {
						Object uu = rs.getObject(i);
						if (tabFK.contains(field)) {
							Class<?> classType = getClassAttribute(c, field);
							method = c.getMethod(methodName, classType);

							if (uu == null) method.invoke(o, classType.newInstance());
							else method.invoke(o, findById(classType, uu));
						}
						else {
							Class<?> classType = oracleTypeToJavaType(resMeta.getColumnTypeName(i));
							method = c.getMethod(methodName, classType);

							method.invoke(o, uu);
						}
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException | InstantiationException e) {
						e.printStackTrace();
					}
				}
				list.add(o);
				try {
					o = c.newInstance();
				} catch (InstantiationException | IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * Recupere un objet en BDD via son identifiant
	 * @param c
	 * @param id
	 * @param type
	 * @return
	 * @throws SQLException
	 */
	public <T> Object findById(Class<?> c, T id, int... type) throws SQLException {
		int connectionLevelIsolation = Connection.TRANSACTION_SERIALIZABLE;
		if(type.length != 0) connectionLevelIsolation = type[0];
		String primaryAttribute = this.getAttributeIdentityPrimary(c);
		String query = "SELECT * FROM "+c.getSimpleName()+" WHERE "+primaryAttribute+"='"+id+"'";
		Connection connection = ConnectionJDBC.getConnection();
		try {
			connection.setAutoCommit(false);
			connection.setTransactionIsolation(connectionLevelIsolation);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		Statement conn = connection.createStatement();
		ResultSet rs = null;
		try {
			rs = conn.executeQuery(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		List<T> list = (List<T>) this.resultsetToListObject(rs, c);
		if(list.size()==0) {
			conn.close();
			return null;
		}

		conn.close();
		//connection.close();
		return list.get(0);
	}
	
	/**
	 * Recupere un objet en BDD via son identifiant
	 * @param c
	 * @param id
	 * @param type
	 * @return
	 * @throws SQLException
	 */
	public <T> Object findById(Class<?> c, T id, Connection connection) throws SQLException {
		String primaryAttribute = this.getAttributeIdentityPrimary(c);
		String query = "SELECT * FROM "+c.getSimpleName()+" WHERE "+primaryAttribute+"='"+id+"'";
		Statement conn = connection.createStatement();
		ResultSet rs = null;
		try {
			rs = conn.executeQuery(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		List<T> list = (List<T>) this.resultsetToListObject(rs, c);
		if(list.size()==0) {
			conn.close();
			return null;
		}

		conn.close();
		return list.get(0);
	}


	/**
	 * Methode permettant de récupérer un tuple d'une table en fonction d'une des foreign key
	 * exemple : SELECT * FROM Lk_Agenda_Photo where id_photo=1 
	 * @param c la table de recherche (ici Photo -> id_photo)
	 * @param id l'id a rechercher (ici 1)
	 * @param lk la table de lien (ici Lk_Agenda_Photo)
	 * @return une liste de tuple du type de lk
	 * @throws SQLException
	 */
	public <T> List<T> findLKById(Class<?> c, Object id, Class<T> lk, int... type)  {
		int connectionLevelIsolation = Connection.TRANSACTION_SERIALIZABLE;
		if(type.length != 0) connectionLevelIsolation = type[0];
		String primaryAttribute = this.getAttributeIdentityPrimary(c);
		String query = "SELECT * FROM "+ lk.getSimpleName() +" WHERE "+ primaryAttribute+"='"+id+"'";
		Connection connection = ConnectionJDBC.getConnection();
		try {
			connection.setAutoCommit(false);
			connection.setTransactionIsolation(connectionLevelIsolation);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		Statement conn = null;
		try {
			conn = connection.createStatement();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		ResultSet rs = null;
		try {
			rs = conn.executeQuery(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		List<T> list = (List<T>) this.resultsetToListObject(rs, lk);
		if(list.size()==0) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return null;
		}

		try {
			conn.close();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}


	/**
	 * Converti un type ORACLE en type Java correspondant
	 * @param typeName
	 * @return
	 */
	public Class<?> oracleTypeToJavaType(String typeName) {
		Class<?> returnedType = String.class;
		switch(typeName) {
		case "NUMBER":
			returnedType = Number.class;
			break;
		case "VARCHAR2": 
			returnedType = String.class;
			break;
		case "DATE":
			returnedType = java.sql.Timestamp.class;
			break;
		case "INT":
			returnedType = int.class;
			break;
		}
		return returnedType;
	}

	/**
	 * Recupere la liste des attributs d'un objet
	 * @param c
	 * @return
	 */
	public <T> List<Field> getAllFields(Class<T> c) {
		Class<?> class1 = c;
		List<Field> liste = new ArrayList<Field>();
		for(Field field: class1.getDeclaredFields()) {
			liste.add(field);
		}
		return liste;
	}

	/**
	 * Recupere la liste des nom des attributs d'un objet
	 * @param c
	 * @return
	 */
	public <T> List<String> getAllFieldsName(Class<T> c) {
		List<Field> fields = this.getAllFields(c);
		List<String> fieldsName = new ArrayList<String>();
		for(Field field: fields) {
			fieldsName.add(field.getName());
		}
		return fieldsName;
	}

	/**
	 * Retourne le nom de l'attribut primaire
	 * @param c
	 * @return
	 */
	public <T> String getAttributeIdentityPrimary(Class<T> c) {
		for(Field field: c.getDeclaredFields()) {
			try {
				Annotation[] annotations = field.getDeclaredAnnotations();
				if(annotations != null && annotations.length>0) {
					if(annotations[0].annotationType().equals(IdentityPrimary.class))
						return field.getName();
				}
			} catch (SecurityException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * Retourne le nom de l'attribut primaire
	 * @param c
	 * @return
	 */
	public <T> String getAttributeSequenceId(Class<T> c, String name) {
		IdentityPrimary a = null;
		try {
			Field field = c.getDeclaredField(name);
			a = field.getAnnotation(IdentityPrimary.class);
		} catch (SecurityException | NoSuchFieldException e) {
			e.printStackTrace();
		}
		return a.value();
	}


	/**
	 * 
	 * @param c
	 * @param example
	 * @return
	 * @throws SQLException 
	 */
	public <T> List<T> findByExample(Object example, int... type) {
		int connectionLevelIsolation = Connection.TRANSACTION_SERIALIZABLE;
		if(type.length != 0) connectionLevelIsolation = type[0];
		Connection connection = ConnectionJDBC.getConnection();
		try {
			connection.setAutoCommit(false);
			connection.setTransactionIsolation(connectionLevelIsolation);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		Statement conn = null;
		try {
			conn = connection.createStatement();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		Class<?> c = example.getClass();
		String query = "SELECT * FROM "+c.getSimpleName()+" WHERE "+this.example(example);
		System.out.println(query);
		ResultSet rs = null;
		try {
			rs = conn.executeQuery(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		List<T> list = (List<T>) resultsetToListObject(rs, c);
		return list;		
	}

	public String example(Object o) {
		Class<?> clazz = o.getClass();
		int nb_field = 0;
		String req = "";

		for(Field field : clazz.getDeclaredFields()) {
			//you can also use .toGenericString() instead of .getName(). This will
			//give you the type information as well.

			try {
				List<String> attributeToIgnore = this.getAttributeIgnoredQueryProperty(clazz);
				java.lang.reflect.Method method;
				if(attributeToIgnore.contains(field.getName())) {

				} else {
					String p1 = field.getName().substring(0, 1).toUpperCase();
					String p2 = field.getName().substring(1);

					method = o.getClass().getMethod("get"+p1+p2);

					Object ret = method.invoke(o);

					if (ret != null) {

						if(nb_field == 0) {
							if (ret instanceof Number || ret instanceof Integer)
								req += field.getName() + " = " + ret + " ";
							else if (ret instanceof Timestamp) {
								Date d = new Date(((Timestamp) ret).getTime());
								String date = new java.text.SimpleDateFormat("dd-MMM-yy").format(d);
								req += field.getName() + " = '" + date + "' ";
							} else if (ret instanceof String) {
								req += field.getName() + " =  '" + ret + "' ";
							} else if (ret instanceof etatFichierImage) {
								req += field.getName() + " =  '" + ret.toString() + "' ";
							} else {
								String id = this.getAttributeIdentityPrimary(ret.getClass());
								p1 = id.substring(0, 1).toUpperCase();
								p2 = id.substring(1);
								Object tmp = null;
								method = ret.getClass().getMethod("get"+p1+p2);
								tmp = method.invoke(ret);
								if (tmp instanceof String) req += field.getName() + " = '" + tmp + "' ";
								else req += field.getName() + " = " + tmp;
							}


						} else {
							if (ret instanceof Number || ret instanceof Integer )	{
								req += "AND " + field.getName() + " = " + ret + " ";

							} else if (ret instanceof Timestamp) {

								Date d = new Date(((Timestamp) ret).getTime());
								String date = new java.text.SimpleDateFormat("dd-MMM-yy").format(d);
								req += "AND " + field.getName() + " = '" + date + "' ";

							} else if (ret instanceof String) {
								req += "AND " + field.getName() + " =  '" + ret + "' ";
							} else if (ret instanceof agendaModel || ret instanceof calendrierModel || ret instanceof clientEtat || ret instanceof etatFichierImage || ret instanceof modeLivraison || ret instanceof statutCommande) {
								req += "AND " + field.getName() + " = '" + ret.toString() + "' ";
							}
							else {
								String id = this.getAttributeIdentityPrimary(ret.getClass());
								p1 = id.substring(0, 1).toUpperCase();
								p2 = id.substring(1);
								Object tmp = null;
								method = ret.getClass().getMethod("get"+p1+p2);
								tmp = method.invoke(ret);
								if (tmp instanceof String) req += "AND " + field.getName() + " = '" + tmp + "' ";
								else req += "AND " + field.getName() + " = " + tmp;
							}

						}

						nb_field++;
					}
				}

			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}

		}

		return req;

	}


	/**
	 * Retourne le nom des attributs à ignorer pour constituer la query
	 * @param c
	 * @return
	 */
	public <T> List<String> getAttributeIgnoredQueryProperty(Class<T> c) {
		List<String> returnedList = new ArrayList<String>();
		for(Field field: c.getDeclaredFields()) {
			try {
				Annotation[] annotations = field.getDeclaredAnnotations();
				if(annotations != null && annotations.length>0) {
					if(annotations[0].toString().equals("@services.IgnoreQueryProperty()"))
						returnedList.add(field.getName());
				}
			} catch (SecurityException e) {
				e.printStackTrace();
			}
		}
		return returnedList;
	}


	private Class<?> getClassAttribute(Class<?> c, String s) {
		try {
			return c.getDeclaredField(s).getType();
		} catch (NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		}
		return null;
	}



	/**
	 * Retourne le nom de l'attribut primaire
	 * @param c
	 * @return
	 */
	public <T> String getAttributeActiveState(Class<T> c) {
		for(Field field: c.getDeclaredFields()) {
			try {
				Annotation[] annotations = field.getDeclaredAnnotations();
				if(annotations != null && annotations.length>0) {
					if(annotations[0].toString().equals("@services.ActiveState()"))
						return field.getName();  
				}
			} catch (SecurityException e) {
				e.printStackTrace();
			}
		}
		return null;
	}



	/**
	 * Fonction qui prend un objet o et qui construit la requête de suppression
	 * @param o
	 * @return
	 */
	public boolean deleteObject(Object o, int... type) {
		int connectionLevelIsolation = Connection.TRANSACTION_SERIALIZABLE;
		if(type.length != 0) connectionLevelIsolation = type[0];
		Connection connection = ConnectionJDBC.getConnection();
		try {
			connection.setAutoCommit(false);
			connection.setTransactionIsolation(connectionLevelIsolation);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		Statement conn = null;
		try {
			conn = connection.createStatement();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		Object pK = null;

		try {
			java.lang.reflect.Method method;

			String p1 = getAttributeIdentityPrimary(o.getClass()).substring(0, 1).toUpperCase();
			String p2 = getAttributeIdentityPrimary(o.getClass()).substring(1);

			method = o.getClass().getMethod("get"+p1+p2);

			pK = method.invoke(o);

		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}

		Object state = getAttributeActiveState(o.getClass());

		String req = "";

		if (state != null) {
			req = "UPDATE " + o.getClass().getSimpleName();
			if (o.getClass() == FichierImage.class) req+= " SET " + state + " = 'problemeJuridique'";
			else req+= " SET " + state + " = 'supprime'";
			if (pK instanceof String) req+= " WHERE " + getAttributeIdentityPrimary(o.getClass()) + " = '" + pK.toString() + "'";
			else req+= " WHERE " + getAttributeIdentityPrimary(o.getClass()) + " = " + pK.toString();
		} else {
			req = "DELETE FROM " + o.getClass().getSimpleName();
			if (pK instanceof String) req+=" WHERE " + getAttributeIdentityPrimary(o.getClass()) + " = '" + pK.toString() + "'";
			else req += " WHERE " + getAttributeIdentityPrimary(o.getClass()) + " = " + pK.toString();
			
			String methodName = "get" + o.getClass().getSimpleName() + "AttributLK";
			Object tmp = null;
			try {
				tmp = Models.class.getMethod(methodName).invoke(null);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
					| NoSuchMethodException | SecurityException e) {
				e.printStackTrace();
			}
			Class<?>[] alk = (Class<?> []) tmp;

			//Pour tous les tables LK
			for (Class<?> a : alk) {
				String requete = "DELETE FROM " + a.getSimpleName() + " WHERE " + getAttributeIdentityPrimary(o.getClass()) + " = " + pK.toString();
				Statement stmt;
				try {
					stmt = connection.createStatement();
					int r = stmt.executeUpdate(requete);
					System.out.println("Nombre de lignes supprimées dans " + a.getSimpleName() + " : " + r);
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}	   

		try {
			int result = conn.executeUpdate(req);
			System.out.println("Nombre de lignes supprimées dans " + o.getClass().getSimpleName() + " : " + result);
			conn.close();
			connection.commit();
			connection.close();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			conn.close();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println("Suppression appelée, aucun résultat");
		return false;
	}

	
	
	/**
	 * Fonction qui prend un objet o et qui construit la requête de suppression
	 * @param o
	 * @return
	 */
	public boolean deleteObject(Object o, Connection connection) {
		Statement conn = null;
		try {
			conn = connection.createStatement();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		Object pK = null;

		try {
			java.lang.reflect.Method method;

			String p1 = getAttributeIdentityPrimary(o.getClass()).substring(0, 1).toUpperCase();
			String p2 = getAttributeIdentityPrimary(o.getClass()).substring(1);

			method = o.getClass().getMethod("get"+p1+p2);

			pK = method.invoke(o);

		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}

		Object state = getAttributeActiveState(o.getClass());

		String req = "";

		if (state != null) {
			req = "UPDATE " + o.getClass().getSimpleName();
			req+= " SET " + state + " = 'supprime'";
			if (pK instanceof String) req+= " WHERE " + getAttributeIdentityPrimary(o.getClass()) + " = '" + pK.toString() + "'";
			else req+= " WHERE " + getAttributeIdentityPrimary(o.getClass()) + " = " + pK.toString();
		} else {
			req = "DELETE FROM " + o.getClass().getSimpleName();
			if (pK instanceof String) req+=" WHERE " + getAttributeIdentityPrimary(o.getClass()) + " = '" + pK.toString() + "'";
			else req += " WHERE " + getAttributeIdentityPrimary(o.getClass()) + " = " + pK.toString();
			
			String methodName = "get" + o.getClass().getSimpleName() + "AttributLK";
			Object tmp = null;
			try {
				tmp = Models.class.getMethod(methodName).invoke(null);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
					| NoSuchMethodException | SecurityException e) {
				e.printStackTrace();
			}
			Class<?>[] alk = (Class<?> []) tmp;

			//Pour tous les tables LK
			for (Class<?> a : alk) {
				String requete = "DELETE FROM " + a.getSimpleName() + " WHERE " + getAttributeIdentityPrimary(o.getClass()) + " = " + pK.toString();
				Statement stmt;
				try {
					stmt = connection.createStatement();
					int r = stmt.executeUpdate(requete);
					System.out.println("Nombre de lignes supprimées dans " + a.getSimpleName() + " : " + r);
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}	   

		try {
			int result = conn.executeUpdate(req);
			System.out.println("Nombre de lignes supprimées dans " + o.getClass().getSimpleName() + " : " + result);
			conn.close();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println("Suppression appelée, aucun résultat");
		return false;
	}
	
	
	/**
	 * Methode permettant l'authentification du client
	 * @param email
	 * @param password
	 * @return
	 */
	public Client connect(String email, String password) {
		Client client = new Client();
		client.setPassword(password);
		client.setMail_client(email);
		List<Client> l = this.findByExample(client, Connection.TRANSACTION_READ_COMMITTED);
		if (l.size() > 0 ) return l.get(0);
		return null;
	}


	

	/**
	 * Simule l'envoie d'une commande (statut envoyé)
	 * @param commande
	 * @return
	 */
	public boolean simulerEnvoieCommande(Commande commande) {
		commande.setStatut(statutCommande.Envoye);
		try {
			UpdateService.update(commande);
			return true;
		} catch(Exception e) {
			return false;
		}
	}



	public List<Object> getStats() {
		List<Object> liste = new ArrayList<Object>();
		try {
			liste.add(this.getAll(Commande.class));
			liste.add(this.getAll(Article.class));
			liste.add(this.getAll(Client.class));
		} catch (SQLException e) {
			e.printStackTrace();
		}		
		return liste;


	}


	public boolean insererCommandeAvecCode(Commande commande) {
		Object id = null;
		try {
			id = InsertService.insert(commande);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Commande commandeInsere = null;
		try {
			commandeInsere = (Commande) this.findById(Commande.class, id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		boolean estEligibleCP = Contrainte.estEligibleCodePromo(commandeInsere);
		// Si la commande est eligible au code promo on crée le code
		if(estEligibleCP) {
			CodePromo code = new CodePromo();
			String codePromo = Contrainte.genererCodePromo();
			code.setCode_promo(codePromo);
			code.setId_commande(commandeInsere);
			code.setEst_public(0);
			code.setRemise(5);
			code.setNombre_utilisation(0);
			code.setDate_fin_code(null);
			try {
				InsertService.insert(code);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
		}
		// Met a jour les stocks
		boolean onALesStock = true;
		if(onALesStock) commandeInsere.setStatut(statutCommande.Paye);
		else commandeInsere.setStatut(statutCommande.EnAttenteFournisseur);
		UpdateService.update(commandeInsere);
		return true;
	}

	
	/**
	 * La methode recupere les fichiers Public
	 * @return
	 */
	public List<FichierImage> loadFichiersPublic() {
		TirageService t = new TirageService();
		FichierImage f = new FichierImage();
		f.setEst_partage(1);
		List<FichierImage> liste =  t.findByExample(f);
		return liste;
	}
	
	public <T> int getNbObjectForStats(Class<T> c, String... args) {
		int resulCount = 0;
		
		String req = "Select Count(*) FROM " + c.getSimpleName();
		if(args.length > 0) {
			req += " WHERE " + args[0];
		}

		Connection connection = ConnectionJDBC.getConnection();
		Statement conn = null;
		ResultSet rs = null;
		
		try {
			connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
			connection.setAutoCommit(false);
			conn = connection.createStatement();
			rs = conn.executeQuery(req);
		} catch (SQLException e2) {
			e2.printStackTrace();
		}
		
		try {
			while( rs.next() ) {
				resulCount = rs.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return resulCount;
	}
}
