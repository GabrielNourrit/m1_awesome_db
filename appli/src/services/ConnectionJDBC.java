package services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Singleton connection JDBC
 * @author Mickael
 *
 */
public class ConnectionJDBC {
	private static final String configurationFile ="src/BD.properties";	
	private final static DatabaseAccessProperties dap = new DatabaseAccessProperties(configurationFile);


	public static Connection getConnection() {
		Connection conn = null;
		try {
			// Load the database driver
			Class.forName(dap.getJdbcDriver()) ;
			String dbUrl= dap.getDatabaseUrl();
			String username= dap.getUsername();
			String password= dap.getPassword();
			// Get a connection to the database
			conn = DriverManager.getConnection(dbUrl, username, password);
		} catch( Exception e ) {
			System.err.println( "Exception: " + e.getMessage()) ;
			e.printStackTrace();
		}
		return conn;
	}



}
