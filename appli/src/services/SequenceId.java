package services;

public @interface SequenceId {

	String value();

}
