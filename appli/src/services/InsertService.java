package services;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import models.LK;
import models.Models;
import type.myEnum;

public class InsertService {

	/**
	 * Methode d'insertion des tables modeles
	 * @param <T>
	 * @param obj
	 * @param type
	 * @throws SQLException 
	 */
	public static Object insert(Object obj, int... type) throws SQLException {
		Object idValue = null;
		TirageService t = new TirageService();
		int connectionLevelIsolation = Connection.TRANSACTION_SERIALIZABLE;
		if(type.length != 0) connectionLevelIsolation = type[0];
		Object o = null;
		String id = "";
		String p1, p2, methodName, att = " (", val = " values (", requete = "insert into " + obj.getClass().getSimpleName();

		methodName = "get" + obj.getClass().getSimpleName() + "AttributSimple";
		try {
			o = Models.class.getMethod(methodName).invoke(null);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}
		String[] as = (String []) o;
		List<Object> value = new ArrayList<Object>();
		List<String> attribute = new ArrayList<String>();
		if (as.length!=0) {
			String primaryKey = t.getAttributeIdentityPrimary(obj.getClass());
			String sequence = t.getAttributeSequenceId(obj.getClass(), primaryKey);
			for (String a : as) {
				p1 = a.substring(0, 1).toUpperCase();
				p2 = a.substring(1);			   
				methodName = "get"+p1+p2;
				try {
					o = obj.getClass().getMethod(methodName).invoke(obj);
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
						| NoSuchMethodException | SecurityException e) {
					e.printStackTrace();
				}
				if (a.equals(primaryKey) && o==null) {
					value.add(sequence+".nextval");
					attribute.add(primaryKey);
				}
				else if (a.equals(primaryKey) && o != null) {
					idValue = o;
					value.add(o);
					attribute.add(a.toString());
				}
				else if (o != null) {
					value.add(o);
					attribute.add(a.toString());
				}

			} 
		}
		

		methodName = "get" + obj.getClass().getSimpleName() + "AttributFK";
		try {
			o = Models.class.getMethod(methodName).invoke(null);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}
		String[] afk = (String []) o;
		for (String a : afk) {
			p1 = a.substring(0, 1).toUpperCase();
			p2 = a.substring(1);			   
			methodName = "get"+p1+p2;
			try {
				o = obj.getClass().getMethod(methodName).invoke(obj);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
					| NoSuchMethodException | SecurityException e) {
				e.printStackTrace();
			}
			if (o != null) {
				id = t.getAttributeIdentityPrimary(o.getClass());
				p1 = id.toString().substring(0, 1).toUpperCase();
				p2 = id.toString().substring(1);			   
				methodName = "get"+p1+p2;
				try {
					Object o1 = o.getClass().getMethod(methodName).invoke(o);
					if (o1 != null) {
						value.add(o1);
						attribute.add(id);
					}
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
						| NoSuchMethodException | SecurityException e) {
					e.printStackTrace();
				}
			}

		}
		int i = attribute.size()-1;
		for (String a : attribute) {
			if (i == 0) att += a + ") ";
			else att += a + ", ";
			i--;
		}
		i = value.size()-1;
		for (Object v : value) {
			if (v instanceof String) {
				if (((String) v).contains(".nextval")) val += v;
				else val += "'" + v + "'";
			}
			else if (v instanceof Timestamp) {
				Date d = new Date(((Timestamp) v).getTime());
				String date = new java.text.SimpleDateFormat("dd-MMM-yy").format(d);
				val += "'" + date + "'";
			}
			else if(v instanceof myEnum) {
				val += "'" + v + "'";				
			}
			else val += v;
			if (i == 0) val += ")";
			else val += ", ";
			i--;
		}
		requete += att + val;
		Connection connection = ConnectionJDBC.getConnection();
		try {
			connection.setAutoCommit(false);
			connection.setTransactionIsolation(connectionLevelIsolation);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		PreparedStatement stmt = null;
		stmt = connection.prepareStatement(requete);
		stmt.executeQuery();
		System.out.println(requete);

		/*-- DEBUT On recupère l'id de la table courante --*/
		id = t.getAttributeIdentityPrimary(obj.getClass());
		/*-- FIN On recupère l'id de la table associée --*/
		//Si on est dans le cas d'un client on utilise pas séquence pour l'id
		if (!obj.getClass().getSimpleName().contains("Lk_")) {
			if (obj.getClass().getSimpleName().equals("Client") || obj.getClass().getSimpleName().equals("CodePromo")) {
				/*-- DEBUT On recupère la valeur l'id de la table courante --*/
				Object o1 = null;
				p1 = id.substring(0, 1).toUpperCase();
				p2 = id.substring(1);			   
				methodName = "get"+p1+p2;
				try {
					o1 = obj.getClass().getMethod(methodName).invoke(obj);
					if (o1 != null) idValue = o1.toString();
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
						| NoSuchMethodException | SecurityException e) {
					e.printStackTrace();
				}	
				/*-- FIN On recupère la valeur l'id de la table courante --*/
			}
			else {
				/*-- DEBUT On recupère la valeur de l'id insere precedemment --*/
				if (idValue == null) idValue = getCurrvalOfSequence(connection, t.getAttributeSequenceId(obj.getClass(), id));
				/*-- FIN On recupàre la valeur de l'id insere precedemment --*/
			}

			//On insere dans les tables de LK associees a la table courante
			insertLK(connection, obj, idValue, id, true);
		}
		
		return idValue;
	}

	/**
	 * Methode d'insertion des tables modeles
	 * @param <T>
	 * @param obj
	 * @param type
	 * @throws SQLException 
	 */
	public static Object insert(Object obj, Connection connection) throws SQLException {
		Object idValue = null;
		TirageService t = new TirageService();
		Object o = null;
		String id = "";
		String p1, p2, methodName, att = " (", val = " values (", requete = "insert into " + obj.getClass().getSimpleName();

		methodName = "get" + obj.getClass().getSimpleName() + "AttributSimple";
		try {
			o = Models.class.getMethod(methodName).invoke(null);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}
		String[] as = (String []) o;
		List<Object> value = new ArrayList<Object>();
		List<String> attribute = new ArrayList<String>();
		if (as.length!=0) {
			String primaryKey = t.getAttributeIdentityPrimary(obj.getClass());
			String sequence = t.getAttributeSequenceId(obj.getClass(), primaryKey);
			for (String a : as) {
				p1 = a.substring(0, 1).toUpperCase();
				p2 = a.substring(1);			   
				methodName = "get"+p1+p2;
				try {
					o = obj.getClass().getMethod(methodName).invoke(obj);
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
						| NoSuchMethodException | SecurityException e) {
					e.printStackTrace();
				}
				if (a.equals(primaryKey) && o==null) {
					value.add(sequence+".nextval");
					attribute.add(primaryKey);
				}
				else if (a.equals(primaryKey) && o != null) {
					idValue = o;
					value.add(o);
					attribute.add(a.toString());
				}
				else if (o != null) {
					value.add(o);
					attribute.add(a.toString());
				}

			} 
		}
		

		methodName = "get" + obj.getClass().getSimpleName() + "AttributFK";
		try {
			o = Models.class.getMethod(methodName).invoke(null);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}
		String[] afk = (String []) o;
		for (String a : afk) {
			p1 = a.substring(0, 1).toUpperCase();
			p2 = a.substring(1);			   
			methodName = "get"+p1+p2;
			try {
				o = obj.getClass().getMethod(methodName).invoke(obj);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
					| NoSuchMethodException | SecurityException e) {
				e.printStackTrace();
			}
			if (o != null) {
				id = t.getAttributeIdentityPrimary(o.getClass());
				p1 = id.toString().substring(0, 1).toUpperCase();
				p2 = id.toString().substring(1);			   
				methodName = "get"+p1+p2;
				try {
					Object o1 = o.getClass().getMethod(methodName).invoke(o);
					if (o1 != null) {
						value.add(o1);
						attribute.add(id);
					}
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
						| NoSuchMethodException | SecurityException e) {
					e.printStackTrace();
				}
			}

		}
		int i = attribute.size()-1;
		for (String a : attribute) {
			if (i == 0) att += a + ") ";
			else att += a + ", ";
			i--;
		}
		i = value.size()-1;
		for (Object v : value) {
			if (v instanceof String) {
				if (((String) v).contains(".nextval")) val += v;
				else val += "'" + v + "'";
			}
			else if (v instanceof Timestamp) {
				Date d = new Date(((Timestamp) v).getTime());
				String date = new java.text.SimpleDateFormat("dd-MMM-yy").format(d);
				val += "'" + date + "'";
			}
			else if(v instanceof myEnum) {
				val += "'" + v + "'";				
			}
			else val += v;
			if (i == 0) val += ")";
			else val += ", ";
			i--;
		}
		requete += att + val;
		PreparedStatement stmt = null;
		stmt = connection.prepareStatement(requete);
		stmt.executeQuery();
		System.out.println(requete);

		/*-- DEBUT On recupère l'id de la table courante --*/
		id = t.getAttributeIdentityPrimary(obj.getClass());
		/*-- FIN On recupère l'id de la table associée --*/
		//Si on est dans le cas d'un client on utilise pas séquence pour l'id
		if (!obj.getClass().getSimpleName().contains("Lk_")) {
			if (obj.getClass().getSimpleName().equals("Client") || obj.getClass().getSimpleName().equals("CodePromo")) {
				/*-- DEBUT On recupère la valeur l'id de la table courante --*/
				Object o1 = null;
				p1 = id.substring(0, 1).toUpperCase();
				p2 = id.substring(1);			   
				methodName = "get"+p1+p2;
				try {
					o1 = obj.getClass().getMethod(methodName).invoke(obj);
					if (o1 != null) idValue = o1.toString();
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
						| NoSuchMethodException | SecurityException e) {
					e.printStackTrace();
				}	
				/*-- FIN On recupère la valeur l'id de la table courante --*/
			}
			else {
				/*-- DEBUT On recupère la valeur de l'id insere precedemment --*/
				if (idValue == null) idValue = getCurrvalOfSequence(connection, t.getAttributeSequenceId(obj.getClass(), id));
				/*-- FIN On recupàre la valeur de l'id insere precedemment --*/
			}

			//On insere dans les tables de LK associees a la table courante
			insertLK(connection, obj, idValue, id, false);
		}
		
		return idValue;
	}

	
	

	/**
	 * Methode d'insertion des tables modeles
	 * @param <T>
	 * @param obj
	 * @param type
	 * @throws SQLException 
	 */
	public static Object insertMyLk(Object obj, Connection connection) throws SQLException {
		Object idValue = null;
		TirageService t = new TirageService();
		Object o = null;
		String id = "";
		String p1, p2, methodName, att = " (", val = " values (", requete = "insert into " + obj.getClass().getSimpleName();

		methodName = "get" + obj.getClass().getSimpleName() + "AttributSimple";
		try {
			o = Models.class.getMethod(methodName).invoke(null);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}
		String[] as = (String []) o;
		List<Object> value = new ArrayList<Object>();
		List<String> attribute = new ArrayList<String>();
		if (as.length!=0) {
			String primaryKey = t.getAttributeIdentityPrimary(obj.getClass());
			String sequence = t.getAttributeSequenceId(obj.getClass(), primaryKey);
			for (String a : as) {
				p1 = a.substring(0, 1).toUpperCase();
				p2 = a.substring(1);			   
				methodName = "get"+p1+p2;
				try {
					o = obj.getClass().getMethod(methodName).invoke(obj);
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
						| NoSuchMethodException | SecurityException e) {
					e.printStackTrace();
				}
				if (a.equals(primaryKey) && o==null) {
					value.add(sequence+".nextval");
					attribute.add(primaryKey);
				}
				else if (a.equals(primaryKey) && o != null) {
					idValue = o;
					value.add(o);
					attribute.add(a.toString());
				}
				else if (o != null) {
					value.add(o);
					attribute.add(a.toString());
				}

			} 
		}
		

		methodName = "get" + obj.getClass().getSimpleName() + "AttributFK";
		try {
			o = Models.class.getMethod(methodName).invoke(null);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}
		String[] afk = (String []) o;
		for (String a : afk) {
			p1 = a.substring(0, 1).toUpperCase();
			p2 = a.substring(1);			   
			methodName = "get"+p1+p2;
			try {
				o = obj.getClass().getMethod(methodName).invoke(obj);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
					| NoSuchMethodException | SecurityException e) {
				e.printStackTrace();
			}
			if (o != null) {
				id = t.getAttributeIdentityPrimary(o.getClass());
				p1 = id.toString().substring(0, 1).toUpperCase();
				p2 = id.toString().substring(1);			   
				methodName = "get"+p1+p2;
				try {
					Object o1 = o.getClass().getMethod(methodName).invoke(o);
					if (o1 != null) {
						value.add(o1);
						attribute.add(id);
					}
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
						| NoSuchMethodException | SecurityException e) {
					e.printStackTrace();
				}
			}

		}
		int i = attribute.size()-1;
		for (String a : attribute) {
			if (i == 0) att += a + ") ";
			else att += a + ", ";
			i--;
		}
		i = value.size()-1;
		for (Object v : value) {
			if (v instanceof String) {
				if (((String) v).contains(".nextval")) val += v;
				else val += "'" + v + "'";
			}
			else if (v instanceof Timestamp) {
				Date d = new Date(((Timestamp) v).getTime());
				String date = new java.text.SimpleDateFormat("dd-MMM-yy").format(d);
				val += "'" + date + "'";
			}
			else if(v instanceof myEnum) {
				val += "'" + v + "'";				
			}
			else val += v;
			if (i == 0) val += ")";
			else val += ", ";
			i--;
		}
		requete += att + val;
		PreparedStatement stmt = null;
		stmt = connection.prepareStatement(requete);
		stmt.executeQuery();
		System.out.println(requete);

		/*-- DEBUT On recupère l'id de la table courante --*/
		id = t.getAttributeIdentityPrimary(obj.getClass());
		/*-- FIN On recupère l'id de la table associée --*/
		//Si on est dans le cas d'un client on utilise pas séquence pour l'id
		if (!obj.getClass().getSimpleName().contains("Lk_")) {
			if (obj.getClass().getSimpleName().equals("Client") || obj.getClass().getSimpleName().equals("CodePromo")) {
				/*-- DEBUT On recupère la valeur l'id de la table courante --*/
				Object o1 = null;
				p1 = id.substring(0, 1).toUpperCase();
				p2 = id.substring(1);			   
				methodName = "get"+p1+p2;
				try {
					o1 = obj.getClass().getMethod(methodName).invoke(obj);
					if (o1 != null) idValue = o1.toString();
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
						| NoSuchMethodException | SecurityException e) {
					e.printStackTrace();
				}	
				/*-- FIN On recupère la valeur l'id de la table courante --*/
			}
			else {
				/*-- DEBUT On recupère la valeur de l'id insere precedemment --*/
				if (idValue == null) idValue = getCurrvalOfSequence(connection, t.getAttributeSequenceId(obj.getClass(), id));
				/*-- FIN On recupàre la valeur de l'id insere precedemment --*/
			}

			//On insere dans les tables de LK associees a la table courante
		}
		
		return idValue;
	}
	

	/**
	 * Methode d'insertion pour les tables liens associ�es � un objet
	 * @param obj
	 * @param idInsere
	 * @param nomAttributInsere
	 * @param type
	 */
	private static void insertLK(Connection connection, Object obj, Object idInsere, String nomAttributInsere, boolean commit) {
		PreparedStatement stmt = null;
		Object o=null, table_lien = null, attribut = null, id = null;
		String requete = "", p1, p2, methodName;
		LK lkk = LK.getInstance();

		methodName = "get" + obj.getClass().getSimpleName() + "AttributLK";
		try {
			o = Models.class.getMethod(methodName).invoke(null);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}
		Class<?>[] alk = (Class<?> []) o;

		//Pour tous les tables LK
		for (Class<?> a : alk) {
			/*-- DEBUT On recupère LK --*/
			p1 = a.getSimpleName().substring(0, 1).toUpperCase();
			p2 = a.getSimpleName().substring(1);			   
			methodName = "get_"+p1+p2+"_by"+obj.getClass().getSimpleName();
			//methodName = "get"+p1+p2;
			java.lang.reflect.Method method = null;
			try {
				method = lkk.getClass().getMethod(methodName, obj.getClass());
				table_lien = method.invoke(lkk, obj);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
					| NoSuchMethodException | SecurityException e) {
				e.printStackTrace();
			}
			/*-- FIN On recupère LK --*/

			// On cast en liste car on recupère une liste de tuple
			List<?> l = (List<?>) table_lien;
			TirageService t = new TirageService();
			List<String> fields;
			//Pour chaque tuple
			for (Object tuple : l) {
				requete = "insert into " + tuple.getClass().getSimpleName() + " values (";
				//On recupère la liste des attributs
				fields = t.getAllFieldsName(tuple.getClass());
				int taille = fields.size();
				int i = 0;
				//Pour chaque attribut
				for (String s : fields) {
					//On recupère sa valeur
					p1 = s.substring(0, 1).toUpperCase();
					p2 = s.substring(1);			   
					methodName = "get"+p1+p2;
					try {
						attribut = tuple.getClass().getMethod(methodName).invoke(tuple);
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
							| NoSuchMethodException | SecurityException e) {
						e.printStackTrace();
					}
					if (attribut != null) {
						//On recupère son attribut id
						String attributId = t.getAttributeIdentityPrimary(attribut.getClass());
						if (attributId != null) {
							//On recupère la valeur de son attribut id
							p1 = attributId.substring(0, 1).toUpperCase();
							p2 = attributId.substring(1);			   
							methodName = "get"+p1+p2;
							try {
								if (attributId.equals(nomAttributInsere)) id = idInsere;
								else id = attribut.getClass().getMethod(methodName).invoke(attribut);
								if (id instanceof String) id = "'" + id + "'";
								if (i==taille-1) requete += id + ")";
								else requete += id + ",";
							} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
									| NoSuchMethodException | SecurityException e) {
								e.printStackTrace();
							}
						}
						else {
							if (attribut instanceof String) attribut = "'" + attribut + "'";
							if (i==taille-1) requete += attribut + ")";
							else requete += attribut + ",";
						}

					}
					i++;
				}
				System.out.println(requete);
				try {
					stmt = connection.prepareStatement(requete);
					stmt.executeQuery();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
		try {
			if (commit)	{
				connection.commit();
				if (stmt!=null) stmt.close();
				connection.close();
			}
			else {
				if (stmt!=null) stmt.close();
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}


	public static int getCurrvalOfSequence(Connection connection, String sequence) {
		PreparedStatement stmt = null;
		int currval = 0;
		try {
			System.out.println("select " + sequence + ".currval from dual");
			stmt = connection.prepareStatement("select " + sequence + ".currval from dual");
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) currval = rs.getInt(1);
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return currval;
	}


}
