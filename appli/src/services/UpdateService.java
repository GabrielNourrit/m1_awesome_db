package services;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.List;

import type.agendaModel;
import type.calendrierModel;
import type.clientEtat;
import type.etatFichierImage;
import type.modeLivraison;
import type.statutCommande;

public class UpdateService {

	public static void update(Object o, int... type) {
		int connectionLevelIsolation = Connection.TRANSACTION_SERIALIZABLE;
		if(type.length != 0) connectionLevelIsolation = type[0];		
		TirageService t = new TirageService();
		String req = "UPDATE " + o.getClass().getSimpleName() + " SET " + UpdateService.getSetFieldAndValue(o) + " WHERE ";
		String id = t.getAttributeIdentityPrimary(o.getClass());
		String p1 = id.substring(0, 1).toUpperCase();
		String p2 = id.substring(1);
		Object tmp = null;
		java.lang.reflect.Method method;
		try {
			method = o.getClass().getMethod("get"+p1+p2);
			tmp = method.invoke(o);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
		if (tmp instanceof String) req += id + " = '" + tmp + "' ";
		else req += id + " = " + tmp;

		Connection connection = ConnectionJDBC.getConnection();
		try {
			connection.setAutoCommit(false);
			connection.setTransactionIsolation(connectionLevelIsolation);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		PreparedStatement stmt = null;
		try {
			stmt = connection.prepareStatement(req);
			stmt.executeQuery();

			connection.commit();
			stmt.close();
			connection.close();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}


		System.out.println(req);

	}

	public static void update(Object o, Connection connection) throws SQLException  {		
		TirageService t = new TirageService();
		String req = "UPDATE " + o.getClass().getSimpleName() + " SET " + UpdateService.getSetFieldAndValue(o) + " WHERE ";
		String id = t.getAttributeIdentityPrimary(o.getClass());
		String p1 = id.substring(0, 1).toUpperCase();
		String p2 = id.substring(1);
		Object tmp = null;
		java.lang.reflect.Method method;
		try {
			method = o.getClass().getMethod("get"+p1+p2);
			tmp = method.invoke(o);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
		if (tmp instanceof String) req += id + " = '" + tmp + "' ";
		else req += id + " = " + tmp;
		PreparedStatement stmt = null;
		stmt = connection.prepareStatement(req);
		stmt.executeQuery();
		stmt.close();


		System.out.println(req);

	}



	private static String getSetFieldAndValue(Object o) {
		TirageService t = new TirageService();
		Class<?> clazz = o.getClass();
		int nb_field = 0;
		String req = "";

		for(Field field : clazz.getDeclaredFields()) {
			//you can also use .toGenericString() instead of .getName(). This will
			//give you the type information as well.

			try {
				List<String> attributeToIgnore = t.getAttributeIgnoredQueryProperty(clazz);
				java.lang.reflect.Method method;
				if(attributeToIgnore.contains(field.getName())) {

				} else {
					String p1 = field.getName().substring(0, 1).toUpperCase();
					String p2 = field.getName().substring(1);

					method = o.getClass().getMethod("get"+p1+p2);

					Object ret = method.invoke(o);

					if (ret != null) {
						if(nb_field == 0) {
							if (ret instanceof Number || ret instanceof Integer)
								req += field.getName() + " = " + ret + " ";
							else if (ret instanceof Timestamp) {
								Date d = new Date(((Timestamp) ret).getTime());
								String date = new java.text.SimpleDateFormat("dd-MMM-yy").format(d);
								req += field.getName() + " = '" + date + "' ";
							} else if (ret instanceof String) {
								req += field.getName() + " =  '" + ret + "' ";
							} else {
								String id = t.getAttributeIdentityPrimary(ret.getClass());
								p1 = id.substring(0, 1).toUpperCase();
								p2 = id.substring(1);
								Object tmp = null;
								method = ret.getClass().getMethod("get"+p1+p2);
								tmp = method.invoke(ret);
								if (tmp instanceof String) req += field.getName() + " = '" + tmp + "' ";
								else req += field.getName() + " = " + tmp;
							}

						} else {
							if (ret instanceof Number || ret instanceof Integer )	{
								req += ", " + field.getName() + " = " + ret + " ";

							} else if (ret instanceof Timestamp) {

								Date d = new Date(((Timestamp) ret).getTime());
								String date = new java.text.SimpleDateFormat("dd-MMM-yy").format(d);
								req += ", " + field.getName() + " = '" + date + "' ";

							} else if (ret instanceof String) {
								req += ", " + field.getName() + " =  '" + ret + "' ";
							} else if (ret instanceof agendaModel || ret instanceof calendrierModel || ret instanceof clientEtat || ret instanceof etatFichierImage || ret instanceof modeLivraison || ret instanceof statutCommande) {
								req += ", " + field.getName() + " = '" + ret.toString() + "' ";
							} else {
								String id = t.getAttributeIdentityPrimary(ret.getClass());
								p1 = id.substring(0, 1).toUpperCase();
								p2 = id.substring(1);
								Object tmp = null;
								method = ret.getClass().getMethod("get"+p1+p2);
								tmp = method.invoke(ret);
								if (tmp instanceof String) req += ", " + field.getName() + " = '" + tmp + "' ";
								else req += ", " + field.getName() + " = " + tmp;
							}
						}

						nb_field++;
					}
				}

			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}

		}

		return req;

	}


}
